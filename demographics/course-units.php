<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_data_table.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_scatter_plot.php');

  $db = get_database();
  
  $labelAlias = 'CourseUnit';
  $valueAlias = 'NumCorrect';
  $suffix     = 'Rank';

  // get data
  $res = $db->query("
    SELECT  courseUnits AS CourseUnits,
            COUNT(*) AS NumParticipants
    FROM    vw_participants 
    WHERE   isValid = 1
    GROUP   BY courseUnits");
  $data = [ 'rows' => get_rows_as_array($res, $db) ];
  Layout::EmitTop('Course Unit Distribution', true);


  scatter_plot($data['rows'],
    'CourseUnits',
    'NumParticipants',
    'scatter_plot',
    false);
  data_table($data);

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}