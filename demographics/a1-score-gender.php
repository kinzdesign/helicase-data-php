<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_anova_one_way.php');

  $db = get_database();
  
  $question     = 87;
  $labelCol     = 'Gender';
  $identCol     = 'Participant';
  $treatCol     = 'Gender';
  $valueCol     = 'NumCorrect';

  // get data
  $res = $db->query("
    SELECT  p.participant AS $identCol, 
            COALESCE(MIN(a.answerGroup), MIN(a.answerText)) AS $treatCol,
            p.score AS $valueCol
    FROM    responses AS r
            INNER JOIN vw_participants AS p ON r.participant = p.participant
            LEFT  JOIN responses_multi AS m ON r.question = m.question AND r.participant = m.participant
            LEFT  JOIN answers AS a ON m.answer = a.answer
    WHERE   p.isValid = 1
            AND r.question = $question
    GROUP   BY p.participant
    ORDER   BY a.displayOrder");
  $data = array('rows' => get_rows_as_array($res, $db));

  render_anova_one_way($data, $question, $identCol, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}