<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('Demographic Data', true);
?>
      <h2>Summary Tables and Charts</h2>
      <ul>
        <li><a href="charts.php">View Question Charts</a></li>
        <!-- <li><a href="tables.php">View Tables</a></li> -->
      </ul>

      <h2>Detailed Information</h2>
      <div class='table-responsive'><table class="table table-striped">
        <thead>
          <tr>
            <td>
            </td>
            <th class="text-center">Score</th>
            <th class="text-center">Questions Correct</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>1: By Age</th>
            <td>
              <a href="a1-score-demographic.php?question=81&amp;label=Age">One-Way ANOVA</a>
            </td>
            <td>
              <a href="a1-correct-demographic.php?question=81&amp;label=Age">One-Way ANOVA</a>
            </td>
          </tr>
          <tr>
            <th>2: By Highest Degree</th>
            <td>
              <a href="a1-score-demographic.php?question=82&amp;label=Degree">One-Way ANOVA</a>
            </td>
            <td>
              <a href="a1-correct-demographic.php?question=82&amp;label=Degree">One-Way ANOVA</a>
            </td>
          </tr> 
          <tr>
            <th>3: By Courses</th>
            <td>
              <a href="c-courses-score.php">Correlation</a>
            </td>
            <td>
              <a href="c-courses-correct.php">Correlation</a>
            </td>
          </tr>
          <tr>
            <th>4: By Hispanic Ethnicity</th>
            <td>
              <a href="t2-score-demographic-multi.php?question=84&amp;header=Ethnicity">Two-Sample t-Test</a>
            </td>
            <td>
              <a href="t2-correct-demographic-multi.php?question=84&amp;header=Ethnicity">Two-Sample t-Test</a>
            </td>
          </tr>
          <tr>
            <th>5: By Race</th>
            <td>
              <a href="a1-score-demographic-multi.php?question=85&amp;header=Race">One-Way ANOVA</a>
            </td>
            <td>
              <a href="a1-correct-demographic-multi.php?question=85&amp;header=Race">One-Way ANOVA</a>
            </td>
          </tr>
          <tr>
            <th>6: Sex Assigned at Birth</th>
            <td>
              <a href="t2-score-demographic.php?question=86&amp;header=Sex">Two-Sample t-Test</a>
            </td>
            <td>
              <a href="t2-correct-demographic.php?question=86&amp;header=Sex">Two-Sample t-Test</a>
            </td>
          </tr>
          <tr>
            <th>7: Current Gender Identity</th>
            <td>
              <a href="a1-score-gender.php">One-Way ANOVA</a>
            </td>
            <td>
              <a href="a1-correct-gender.php">One-Way ANOVA</a>
            </td>
          </tr>
        </tbody>
      </table></div>


<?php
  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}