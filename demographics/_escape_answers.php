<?php
  function escape_answers($answers) {
    return 
      str_replace('|', ' & ', 
      str_replace('Middle School:', 'MS:', 
      str_replace('High School:', 'HS:', 
      str_replace('Undergraduate:', 'UG:', 
      str_replace('Graduate:', 'GR:', 
        $answers
      )))));
  }
?>