<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_box_plot.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_data_table.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_scatter_plot.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_stats.php');

  $db = get_database();
  
  // get data
  $res = $db->query("
    SELECT  p.participant, 
            SUM(CASE WHEN a.answerText LIKE 'Middle School%' THEN 1 ELSE 0 END) AS ms,
            SUM(CASE WHEN a.answerText LIKE 'High School%' THEN 1 ELSE 0 END) AS hs,
            SUM(CASE WHEN a.answerText LIKE 'Undergraduate%' THEN 1 ELSE 0 END) AS ug,
            SUM(CASE WHEN a.answerText LIKE 'Graduate%' THEN 1 ELSE 0 END) AS gr
    FROM    responses_multi AS r
            INNER JOIN vw_participants AS p ON r.participant = p.participant
            INNER JOIN answers AS a ON r.answer = a.answer
    WHERE   p.isValid = 1
            AND r.question = 83
    GROUP   BY p.participant");
  $data = [ 'rows' => get_rows_as_array($res, $db) ];

  $tmp = array(
    'Middle School' => array(),
    'High School' => array(),
    'Undergraduate' => array(),
    'Graduate' => array()
  );
  $labels = array_keys($tmp);
  foreach ($data['rows'] as $row) {
    $tmp['Middle School'][] = $row['ms'];
    $tmp['High School'][] = $row['hs'];
    $tmp['Undergraduate'][] = $row['ug'];
    $tmp['Graduate'][] = $row['gr'];
  }
  $results = array();
  foreach ($tmp as $key => $row) {
    $results[$key] = descriptive_stats($row);
  }

  Layout::EmitTop('Course Counts by Level', true);

  box_plot($results);

  echo "
    <div class='table-responsive'><table class='table table-striped table-hover table-stats table-condensed'>
      <tbody>
        <tr class='info'>
          <th></th>";
  foreach ($results as $label => $summary) {
    echo "
          <td>$label</td>";
  }
  echo "
        </tr>
        <tr>
          <th>Observations</th>";
  foreach ($results as $label => $summary) {
    echo "
          <td>{$summary['n']}</td>";
  }
  echo "
        </tr>
        <tr>
          <th>Mean</th>";
  foreach ($results as $label => $summary) {
    echo "
          <td>".format_number($summary['mean'],2)."</td>";
  }
  echo "
        </tr>
        <tr>
          <th>Range</th>";
  foreach ($results as $label => $summary) {
    echo "
          <td>".
            format_number($summary['quartiles']['0%'],0).'-'.
            format_number($summary['quartiles']['100%'],0).
          "</td>";
  }
  echo "
        </tr>
        <tr>
          <th>Variance</th>";
  foreach ($results as $label => $summary) {
    echo "
          <td>".format_number($summary['var.s'])."</td>";
  }
  echo "
        </tr>
        <tr>
          <th>Standard Deviation</th>";
  foreach ($results as $label => $summary) {
    echo "
          <td>".format_number($summary['stddev.s'])."</td>";
  }
  echo "
        </tr>
      </tbody>
    </table></div>";




  data_table($data);

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}