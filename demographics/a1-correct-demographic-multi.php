<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_anova_one_way.php');

  $db = get_database();
  
  $question     = Layout::GetQueryStringInt('question');
  $labelCol     = Layout::GetQueryString('header');
  $identCol     = 'Participant';
  $treatCol     = 'Cohort';
  $valueCol     = 'NumCorrect';

  // get data
  $res = $db->query("
    SELECT  DISTINCT p.participant AS $identCol,
            p.cohort AS $treatCol,
            COALESCE(a.answerGroup, a.answerText) AS $labelCol, 
            p.questionsRight AS $valueCol
    FROM    responses_multi AS r
            INNER JOIN answers AS a ON r.answer = a.answer
            INNER JOIN vw_participants AS p ON r.participant = p.participant
    WHERE   r.question = $question
            AND p.isValid = 1
    ORDER   BY a.displayOrder, p.participant");
  $data = array('rows' => get_rows_as_array($res, $db));

  render_anova_one_way($data, $question, $identCol, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}