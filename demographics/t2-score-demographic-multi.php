<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_two_sample.php');

  $db = get_database();
  
  $question = Layout::GetQueryStringInt('question');
  $labelCol = Layout::GetQueryString('header');
  $valueCol = 'Score';

  $res = $db->query("
        SELECT  DISTINCT p.participant AS Participant, 
                p.cohort AS Cohort,
                fn_get_answers_for_question(p.participant, r.question) AS Answered,
                COALESCE(a.answerGroup, a.answerText) AS $labelCol, 
                p.score AS $valueCol
        FROM    responses_multi AS r
                INNER JOIN answers AS a ON r.answer = a.answer
                INNER JOIN vw_participants AS p ON r.participant = p.participant
        WHERE   r.question = $question
                AND p.isValid = 1
                AND COALESCE(a.answerGroup, a.answerText) NOT LIKE '%Other%'
        ORDER   BY $labelCol, $valueCol DESC");
  check_for_db_error($res, $db);

  render_t_test_two_sample($res, $question, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}