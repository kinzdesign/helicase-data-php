<?php
  function print_demo_question_data($question, $data, $script = '', $expanded = true) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_chart.php');
    echo "
        <div class='panel panel-danger'>
          <div class='panel-heading' role='tab' id='h-z-$question'>
            <h3 class='panel-title'>
              <a role='button' data-toggle='collapse' data-parent='#a-quiz' href='#c-z-$question' aria-controls='c-z-$question'" . 
              ($expanded ? " aria-expanded='true'" : '') .
              ">
                {$question}: {$data['intro']}
              </a>
            </h3>
          </div>
          <div id='c-z-$question' class='panel-collapse collapse" . 
              ($expanded ? ' in' : '') .
              "' role='tabpanel' aria-labelledby='h-z-$question'>
            <div class='panel-body'>
              <div class='panel-group' id='a-z-$question' role='tablist' aria-multiselectable='true'>";
    if(isset($data['answer']) && isset($data['answers'])) {
      echo "
                <div class='row'>
                  <div class='col-sm-6'>
                    " . render_chart("c-d-b-$question") . "
                  </div>
                  <div class='col-sm-6'>
                    " . render_chart("c-d-p-$question") . "
                  </div>
                </div>";
    } elseif(isset($data['answer'])) {
      echo render_chart("c-d-b-$question");
    } elseif(isset($data['answers'])) {
      echo render_chart("c-d-w-$question");
    }
    echo '
              </div>
            </div>
          </div>
        </div>';
  }
?>