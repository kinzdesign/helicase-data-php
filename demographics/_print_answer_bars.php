<?php
  function print_answer_bars($question, $answers, $script = '') {
    echo "
                    <div id='c-d-b-$question' class='chart'></div>";
    // build javascript
    $script .= "
      window.chartData['c-d-b-$question'] = google.visualization.arrayToDataTable([
        ['Answer', 'Count']";
    // iterate rows to output data
    foreach ($answers as $answer => $n) {
      $script .= ",
        ['$answer', $n]";
    }
    $script .= "
      ]);";
    return $script;
  }
?>
