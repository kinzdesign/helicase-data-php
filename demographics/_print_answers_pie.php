<?php
  function print_answers_pie($question, $answers, $layout, $script = '') {
    echo "
                    <div id='c-d-$layout-$question' class='chart'></div>";
    // build javascript
    $script .= "
      window.chartData['c-d-$layout-$question'] = google.visualization.arrayToDataTable([
        ['Answer(s)', 'Count']";
    // iterate rows to output data
    foreach ($answers as $answer => $n) {
      $script .= ",
        ['$answer', $n]";
    }
    $script .= "
      ]);";
    return $script;
  }
?>