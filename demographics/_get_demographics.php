<?php
  function get_demographics() {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/demographics/_escape_answers.php');
    $db = get_database();

    // create buckets
    $output = [
      'questions' => [ 1=>[], 2=>[], 3=>[], 4=>[], 5=>[], 6=>[], 7=>[] ]
    ];

    // multiple choice (single)
    $res = $db->query("
      SELECT  question, intro, answer, COUNT(participant) AS n
      FROM    (        
                SELECT  p.participant, 
                        q.displayOrder AS question,
                        q.multiIntroHtml AS intro,
                        REPLACE(a.answerText, '''', '\\\\\\'') AS answer,
                        a.displayOrder
                FROM    responses AS r
                        INNER JOIN questions AS q ON r.question = q.question
                        INNER JOIN answers AS a ON r.answer = a.answer
                        INNER JOIN vw_participants AS p ON r.participant = p.participant
                WHERE   p.isValid = 1 AND
                        q.quiz = 'DEMO' AND
                        ((q.questionFormat & 16) = 0) -- !allowMultipleMulti
              ) AS a
      GROUP   BY question, intro, answer
      ORDER   BY question, displayOrder");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      if(!isset($output['questions'][$q['question']]['intro'])) {
        $output['questions'][$q['question']]['intro'] = $q['intro'];
      }
      if(!isset($output['questions'][$q['question']]['answers']))
        $output['questions'][$q['question']]['answers'] = array();
      $output['questions'][$q['question']]['answers'][escape_answers($q['answer'])] = $q['n'];
    }

    // multiple choice (multi): get counts per answer
    $res = $db->query("
      SELECT  question, intro, answer, COUNT(participant) AS n
      FROM    (        
                SELECT  p.participant, 
                        q.displayOrder AS question,
                        q.multiIntroHtml AS intro,
                        a.answerText AS answer,
                        a.displayOrder
                FROM    responses_multi AS r
                        INNER JOIN questions AS q ON r.question = q.question
                        INNER JOIN answers AS a ON r.answer = a.answer
                        INNER JOIN vw_participants AS p ON r.participant = p.participant
                WHERE   p.isValid = 1
              ) AS a
      GROUP   BY question, intro, answer
      ORDER   BY question, displayOrder");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      if(!isset($output['questions'][$q['question']]['intro'])) {
        $output['questions'][$q['question']]['intro'] = $q['intro'];
      }
      if(!isset($output['questions'][$q['question']]['answer']))
        $output['questions'][$q['question']]['answer'] = array();
      $output['questions'][$q['question']]['answer'][escape_answers($q['answer'])] = $q['n'];
    }

    // multiple choice (multi): get correlated answers per question
    $res = $db->query("
      SELECT  question, answers, COUNT(participant) AS n
      FROM    (        
                SELECT  p.participant, 
                        q.displayOrder AS question,
                        fn_get_answers_for_question(p.participant, q.question) AS answers
                FROM    vw_participants AS p
                        INNER JOIN questions AS q
                WHERE   p.isValid = 1 AND
                        ((q.questionFormat & 16) > 0) -- allowMultipleMulti
              ) AS a
      WHERE   answers IS NOT NULL
      GROUP   BY question, answers
      ORDER   BY question, n DESC");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      if(!isset($output['questions'][$q['question']]['answers']))
        $output['questions'][$q['question']]['answers'] = array();
      $output['questions'][$q['question']]['answers'][escape_answers($q['answers'])] = $q['n'];
    }

    // population pyramid
    $res = $db->query("
      SELECT  age, sex, COUNT(*) AS n
      FROM    vw_participants
      WHERE   isValid = 1
      GROUP   BY age, sex");
    check_for_db_error($res, $db);
    $output['population'] = array();
    while($q = $res->fetch_assoc()) {
      if(!array_key_exists($q['age'], $output['population']))
        $output['population'][$q['age']] = array();
      $output['population'][$q['age']][$q['sex']] = $q['n'];
    }

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }
?>