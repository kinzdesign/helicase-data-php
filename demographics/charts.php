<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_print_chart_panel.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_chart.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/demographics/_get_demographics.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/demographics/_print_demo_question_data.php');

  $script = '';
  Layout::EmitTop('Demographics', true);


  echo "
      <div class='panel-group' id='a-quiz' role='tablist' aria-multiselectable='true'>";

  print_chart_panel('Population Pyramid (Age and Sex)', '-p-y');
  print_chart_panel('1: How long ago were you born?', '-d-w-1');
  print_chart_panel('2: What is the highest level of education you have completed?', '-d-w-2');
  print_chart_panel('3: Which, if any, of the following courses have you taken?', '-d-c-3', '-d-p-3');
  print_chart_panel('4: Do you identify as being of Spanish/Hispanic/Latino origin? You can choose multiple options.', '-d-c-4', '-d-p-4');
  print_chart_panel('5: How do you identify? You can choose multiple options.', '-d-c-5', '-d-p-5');
  print_chart_panel('6: Which box did they check on your birth certificate?', '-d-w-6');
  print_chart_panel('7: With which (if any) gender(s) do you currently identify?', '-d-c-7', '-d-p-7');

  echo "
      </div>";

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}