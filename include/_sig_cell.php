<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');

function sig_cell($pValue, $baseClasses = '', $colSpan = 1, $sigThreshold = 0.05, 
  $classIfSignificant = 'sig', $classIfInsignificant = 'insig') 
{
  if($pValue <= 0) {
    return '<td' .
      ($colSpan > 1 ? " colspan='$colSpan'" : '') .
      " class='$baseClasses'>--</td>";
  }
  $num = get_first_sig_fig($pValue);
  return '<td' .
    ($colSpan > 1 ? " colspan='$colSpan'" : '') .
    " class='$baseClasses " .
    ($pValue <= $sigThreshold ? $classIfSignificant : $classIfInsignificant) .
    "'>$num</td>";
}

function double_sig_cell($pValueEqual, $pValueUnequal, $baseClasses = '', $colSpan = 1, $sigThreshold = 0.05, 
  $classIfSignificant = 'sig', $classIfInsignificant = 'insig') 
{
  return '<td' .
    ($colSpan > 1 ? " colspan='$colSpan'" : '') .
    " class='$baseClasses ".
    (($pValueEqual < $sigThreshold || $pValueUnequal < $sigThreshold) ? $classIfSignificant : $classIfInsignificant)
    ."'>
      ".sig_div($pValueEqual, $sigThreshold, $classIfSignificant, $classIfInsignificant, '=: ', 'Equal Variance')."
      ".sig_div($pValueUnequal, $sigThreshold, $classIfSignificant, $classIfInsignificant, '&ne;: ', 'Unequal Variance')."
    </td>";
}

function sig_div($pValue, $sigThreshold = 0.05, $classIfSignificant = 'sig', $classIfInsignificant = 'insig', 
  $prefix = '', $title = '') {
  if($pValue <= 0) {
    return "<div>{$prefix}--</div>";
  }
  $num = get_first_sig_fig($pValue);
  return "<div title='$title' class='div-sig " .
    ($pValue <= $sigThreshold ? $classIfSignificant : $classIfInsignificant) .
    "'>{$prefix}$num</div>";
}

function get_first_sig_fig($val) {
  if($val <= 0)
    return '--';
  $digits = ($val >= 1)
    ? 0
    : abs(floor(log10($val)));
  if($digits < 5)
    return round($val,$digits);
  else
    return round($val * 10**$digits, 0) .'&times;10<sup>-'.$digits.'</sup>';

}