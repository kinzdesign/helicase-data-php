<?php
  function get_config() {
    // load config_vars file if present
    if(file_exists($_SERVER['DOCUMENT_ROOT'].'/include/_config_vars.php')) {
      require_once($_SERVER['DOCUMENT_ROOT'].'/include/_config_vars.php');
      return config_vars();
    }

    $config = array();

    $config['DEFAULT_ENVIRONMENT'] = 'env';

    $config['MYSQL'] = array();

    $config['MYSQL']['env'] = [
      'HOST'      => $_ENV['MYSQL_HOST'],
      'PORT'      => $_ENV['MYSQL_PORT'],
      'WEB_USER'  => $_ENV['MYSQL_USER'],
      'WEB_PASS'  => $_ENV['MYSQL_PASS'],
      'BUP_USER'  => $_ENV['MYSQL_USER'],
      'BUP_PASS'  => $_ENV['MYSQL_PASS'],
      'DATABASE'  => $_ENV['MYSQL_DATABASE'],
      'SSL_CERT'  => $_SERVER['DOCUMENT_ROOT'].'/'.$_ENV['MYSQL_SSL_CER']
    ];

    return $config;
  }
?>
