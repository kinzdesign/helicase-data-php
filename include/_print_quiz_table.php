<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_sig_cell.php');
  function print_quiz_table($data, $quiz) {
    $summary = $data['Summary'];
    $headers = array(
      'Likert'      => 'Likert<sup>a</sup>, mean (SD)',
      'LikertTime'  => 'Likert Time (sec.), mean (SD)',
      'Correct'     => 'Correct Responses, mean (SD)',
      'MultiTime'   => 'Multiple-Choice Time (sec.), mean (SD)',
      'Score'       => 'Score, mean (SD)',
      'TimeBonus'   => 'Time Bonus, mean (SD)',
      'TotalScore'  => 'Total Score, mean (SD)'
    );
    echo "
      <h2>$quiz Summary Data</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-top heavy-right heavy-bottom heavy-left'>Measure</th>
            <th class='heavy-top heavy-right heavy-bottom heavy-left'>Overall</th>
            <th class='heavy-top thin-right heavy-bottom heavy-left'>CH-G</th>
            <th class='heavy-top thin-right heavy-bottom thin-left'>CH-C</th>
            <th class='heavy-top heavy-right heavy-bottom thin-left'><em>P</em>-value*</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th class='thin-top thin-right thin-bottom heavy-left'>Likert Consistency (Cronbach's &alpha;)</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($summary['alpha']['Total']['alpha'])."
            </td>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($summary['alpha']['CH-G']['alpha'])."
            </td>
            <td class='thin-top thin-right thin-bottom this-left'>
              ".format_number($summary['alpha']['CH-C']['alpha'])."
            </td>
            ".sig_cell(-1, 'thin-top heavy-right thin-bottom this-left')."
          </tr>";
    foreach ($data['Axes'] as $key) {
      echo "
          <tr>
            <th class='thin-top thin-right thin-bottom heavy-left'>{$headers[$key]}</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($summary[$key]['full']['mean'],2).' ('.format_number($summary[$key]['full']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($summary[$key]['CH-G']['mean'],2).' ('.format_number($summary[$key]['CH-G']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom this-left'>
              ".format_number($summary[$key]['CH-C']['mean'],2).' ('.format_number($summary[$key]['CH-C']['stddev.s'],1).")
            </td>
            ".sig_cell($summary[$key]['t.equal']['p2'], 'thin-top heavy-right thin-bottom this-left')."
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td colspan='5' class='heavy-all'>
              <sup>a</sup>Likert rating scale from 1 (strongly disagree) to 5 (strongly agree).
              *<em>P</em> value obtained from two-sample, two-tailed t-tests.
            </td>
          </tr>
        </tfoot>
      </table></div>";



    echo "
      <h2>$quiz Results Among All Participants</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-all'>Question</th>
            <th class='heavy-top thin-right heavy-bottom heavy-left'>Likert,<sup>a</sup> mean (SD)</th>
            <th class='heavy-top heavy-right heavy-bottom thin-left'>Likert Time (sec.), mean (SD)</th>
            <th class='heavy-top thin-right heavy-bottom heavy-left'>Correct Responses, <em>n</em> (%)</th>
            <th class='heavy-top heavy-right heavy-bottom thin-left'>Multiple-Choice Time (sec.), mean (SD)</th>
            <th class='heavy-top heavy-thin heavy-bottom heavy-left'>Score, mean (SD)</th>
            <th class='heavy-top heavy-thin heavy-bottom thin-left'>Time Bonus, mean (SD)</th>
            <th class='heavy-top heavy-right heavy-bottom thin-left'>Total Score, mean (SD)</th>
          </tr>
        </thead>
        <tbody>";
    foreach ($data['Questions'] as $question => $stats) {
      echo "
          <tr>
            <th class='thin-top heavy-right thin-bottom heavy-left'>{$question}: {$stats['Header']}</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($stats['Likert']['full']['mean'],2).' ('.format_number($stats['Likert']['full']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['LikertTime']['full']['mean'],2).' ('.format_number($stats['LikertTime']['full']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($stats['Correct']['full']['mean'] * $stats['Correct']['full']['n'],0).' ('.format_number($stats['Correct']['full']['mean']*100.0,0)."%)
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['MultiTime']['full']['mean'],2).' ('.format_number($stats['MultiTime']['full']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($stats['Score']['full']['mean'],2).' ('.format_number($stats['Score']['full']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['TimeBonus']['full']['mean'],2).' ('.format_number($stats['TimeBonus']['full']['stddev.s'],1).")
            </td>
            <td class='thin-top heavy-right thin-bottom thin-left'>
              ".format_number($stats['TotalScore']['full']['mean'],2).' ('.format_number($stats['TotalScore']['full']['stddev.s'],1).")
            </td>
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td colspan='100' class='heavy-all'>
              <sup>a</sup>Likert rating scale from 1 (strongly disagree) to 5 (strongly agree).
            </td>
          </tr>
        </tfoot>
      </table></div>";

    echo "
      <h2>$quiz Results by Cohort</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-all' rowspan='2'>Question</th>
            <th class='heavy-top mid-right thin-bottom heavy-left' colspan='3'>Likert,<sup>a</sup> mean (SD)</th>
            <th class='heavy-top heavy-right thin-bottom mid-left' colspan='3'>Likert Time (sec.), mean (SD)</th>
            <th class='heavy-top mid-right thin-bottom heavy-left' colspan='3'>Correct Responses, <em>n</em> (%)</th>
            <th class='heavy-top heavy-right thin-bottom mid-left' colspan='3'>Multiple-Choice Time (sec.), mean (SD)</th>
            <th class='heavy-top heavy-mid thin-bottom heavy-left' colspan='3'>Score, mean (SD)</th>
            <th class='heavy-top heavy-mid thin-bottom mid-left' colspan='3'>Time Bonus, mean (SD)</th>
            <th class='heavy-top heavy-right thin-bottom mid-left' colspan='3'>Total Score, mean (SD)</th>
          </tr>
          <tr>
            <th class='thin-top thin-right heavy-bottom thin-left'>Gamified</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='thin-top mid-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Gamified</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Gamified</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='thin-top mid-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Gamified</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Gamified</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='thin-top mid-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Gamified</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='thin-top mid-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Gamified</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'><em>P</em> value*</th>
          </tr>
        </thead>
        <tbody>";
    foreach ($data['Questions'] as $question => $stats) {
      echo "
          <tr>
            <th class='thin-top heavy-right thin-bottom heavy-left'>{$question}: {$stats['Header']}</th>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['Likert']['CH-G']['mean'],2).' ('.format_number($stats['Likert']['CH-G']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['Likert']['CH-C']['mean'],2).' ('.format_number($stats['Likert']['CH-C']['stddev.s'],1).")
            </td>
            ".double_sig_cell($stats['Likert']['t.equal']['p2'], $stats['Likert']['t.unequal']['p2'], 'thin-top mid-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['LikertTime']['CH-G']['mean'],2).' ('.format_number($stats['LikertTime']['CH-G']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['LikertTime']['CH-C']['mean'],2).' ('.format_number($stats['LikertTime']['CH-C']['stddev.s'],1).")
            </td>
            ".double_sig_cell($stats['LikertTime']['t.equal']['p2'], $stats['LikertTime']['t.unequal']['p2'], 'thin-top heavy-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['Correct']['CH-G']['mean'] * $stats['Correct']['CH-G']['n'],0).' ('.format_number($stats['Correct']['CH-G']['mean']*100.0,0)."%)
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['Correct']['CH-C']['mean'] * $stats['Correct']['CH-C']['n'],0).' ('.format_number($stats['Correct']['CH-C']['mean']*100.0,0)."%)
            </td>
            ".double_sig_cell($stats['Correct']['t.equal']['p2'], $stats['Correct']['t.unequal']['p2'], 'thin-top mid-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['MultiTime']['CH-G']['mean'],2).' ('.format_number($stats['MultiTime']['CH-G']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['MultiTime']['CH-C']['mean'],2).' ('.format_number($stats['MultiTime']['CH-C']['stddev.s'],1).")
            </td>
            ".double_sig_cell($stats['MultiTime']['t.equal']['p2'], $stats['MultiTime']['t.unequal']['p2'], 'thin-top heavy-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['Score']['CH-G']['mean'],2).' ('.format_number($stats['Score']['CH-G']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['Score']['CH-C']['mean'],2).' ('.format_number($stats['Score']['CH-C']['stddev.s'],1).")
            </td>
            ".double_sig_cell($stats['Score']['t.equal']['p2'], $stats['Score']['t.unequal']['p2'], 'thin-top mid-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['TimeBonus']['CH-G']['mean'],2).' ('.format_number($stats['TimeBonus']['CH-G']['stddev.s'],1).")
              "."
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['TimeBonus']['CH-C']['mean'],2).' ('.format_number($stats['TimeBonus']['CH-C']['stddev.s'],1).")
              "."
            </td>
            ".double_sig_cell($stats['TimeBonus']['t.equal']['p2'], $stats['TimeBonus']['t.unequal']['p2'], 'thin-top mid-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['TotalScore']['CH-G']['mean'],2).' ('.format_number($stats['TotalScore']['CH-G']['stddev.s'],1).")
              "."
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['TotalScore']['CH-C']['mean'],2).' ('.format_number($stats['TotalScore']['CH-C']['stddev.s'],1).")
              "."
            </td>
            ".double_sig_cell($stats['TotalScore']['t.equal']['p2'], $stats['TotalScore']['t.unequal']['p2'], 'thin-top heavy-right thin-bottom thin-left')."
            </td>
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td colspan='100' class='heavy-all'>
              <sup>a</sup>Likert rating scale from 1 (strongly disagree) to 5 (strongly agree).
              *<em>P</em> value obtained from two-sample, two-tailed t-tests.
            </td>
          </tr>
        </tfoot>
      </table></div>";
  }
?>