<?php
  function render_chart($id) {
    return "<div class='chart' id='$id'><i class='fa fa-4x fa-spin fa-spinner fa-fw' title='Loading chart&hellip;'></i><br/>Loading chart&hellip;</div>";
  }
?>