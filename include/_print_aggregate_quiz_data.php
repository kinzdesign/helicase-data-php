<?php
  function print_aggregate_quiz_data($quiz_code, $quiz, $script = '', $expanded = true) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_print_aggregate_question_data.php');
    echo "
        <div class='panel panel-danger'>
          <div class='panel-heading' role='tab' id='h-z-$quiz_code'>
            <h3 class='panel-title'>
              <a role='button' data-toggle='collapse' data-parent='#a-quiz' href='#c-z-$quiz_code' aria-controls='c-z-$quiz_code'" . 
              ($expanded ? " aria-expanded='true'" : '') .
              ">
                {$quiz_code}: {$quiz['title']}
              </a>
            </h3>
          </div>
          <div id='c-z-$quiz_code' class='panel-collapse collapse" . 
              ($expanded ? ' in' : '') .
              "' role='tabpanel' aria-labelledby='h-z-$quiz_code'>
            <div class='panel-body'>";

    echo '
              <div class="panel-group" id="a-z-$quiz_code" role="tablist" aria-multiselectable="true">';
    // $is_first_question = true;

    foreach($quiz['questions'] as $question_id => $question) {
      $script = print_aggregate_question_data($quiz_code, $question_id, $question, $script);//, $is_first_question);
      // $is_first_question = false;
    }
    echo '
              </div>
            </div>
          </div>
        </div>';
    $expanded = false;    
    return $script;
  }
?>