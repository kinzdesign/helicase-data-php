<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_sig_cell.php');
  function print_quiz_likert_table($data, $quiz) {
    $summary = $data['Summary'];
    $headers = array(
      'Likert'      => 'Likert<sup>a</sup>, mean (SD)',
      'LikertTime'  => 'Likert Time (sec.), mean (SD)'
    );
    echo "
      <h2>$quiz Summary Data</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-top heavy-right heavy-bottom heavy-left'>Measure</th>
            <th class='heavy-top heavy-right heavy-bottom heavy-left'>Overall</th>
            <th class='heavy-top thin-right heavy-bottom heavy-left'>CH-G</th>
            <th class='heavy-top thin-right heavy-bottom thin-left'>CH-C</th>
            <th class='heavy-top heavy-right heavy-bottom thin-left'><em>P</em>-value*</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th class='thin-top thin-right thin-bottom heavy-left'>Likert Consistency (Cronbach's &alpha;)</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($summary['alpha']['Total']['alpha'])."
            </td>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($summary['alpha']['CH-G']['alpha'])."
            </td>
            <td class='thin-top thin-right thin-bottom this-left'>
              ".format_number($summary['alpha']['CH-C']['alpha'])."
            </td>
            ".sig_cell(-1, 'thin-top heavy-right thin-bottom this-left')."
          </tr>";
    foreach ($data['Axes'] as $key) {
      echo "
          <tr>
            <th class='thin-top thin-right thin-bottom heavy-left'>{$headers[$key]}</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($summary[$key]['full']['mean'],2).' ('.format_number($summary[$key]['full']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              ".format_number($summary[$key]['CH-G']['mean'],2).' ('.format_number($summary[$key]['CH-G']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom this-left'>
              ".format_number($summary[$key]['CH-C']['mean'],2).' ('.format_number($summary[$key]['CH-C']['stddev.s'],1).")
            </td>
            ".sig_cell($summary[$key]['t.equal']['p2'], 'thin-top heavy-right thin-bottom this-left')."
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td colspan='5' class='heavy-all'>
              <sup>a</sup>Likert rating scale from 1 (strongly disagree) to 5 (strongly agree).
              *<em>P</em> value obtained from two-sample, two-tailed t-tests.
            </td>
          </tr>
        </tfoot>
      </table></div>";

    echo "
      <h2>$quiz Results</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-all' rowspan='2'>Question</th>
            <th class='heavy-all' rowspan='2'><em>N</em></th>
            <th class='heavy-top heavy-right mid-bottom heavy-left' colspan='4'>Likert,<sup>a</sup> mean (SD)</th>
            <th class='heavy-top heavy-right mid-bottom heavy-left' colspan='4'>Likert Time (sec.), mean (SD)</th>
          </tr>
          <tr>
            <th class='mid-top mid-right heavy-bottom heavy-left'>All</th>
            <th class='mid-top thin-right heavy-bottom mid-left'>Gamified</th>
            <th class='mid-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='mid-top heavy-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='mid-top mid-right heavy-bottom heavy-left'>All</th>
            <th class='mid-top thin-right heavy-bottom mid-left'>Gamified</th>
            <th class='mid-top thin-right heavy-bottom thin-left'>Control</th>
            <th class='mid-top heavy-right heavy-bottom thin-left'><em>P</em> value*</th>
          </tr>
        </thead>
        <tbody>";
    foreach ($data['Questions'] as $question => $stats) {
      echo "
          <tr>
            <th class='thin-top heavy-right thin-bottom heavy-left'>{$question}: {$stats['Header']}</th>
            <td class='thin-top heavy-right thin-bottom heavy-left'>
              {$stats['Likert']['full']['n']}
            </td>
            <td class='thin-top mid-right thin-bottom heavy-left'>
              ".format_number($stats['Likert']['full']['mean'],2).' ('.format_number($stats['Likert']['full']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom mid-left'>
              ".format_number($stats['Likert']['CH-G']['mean'],2).' ('.format_number($stats['Likert']['CH-G']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['Likert']['CH-C']['mean'],2).' ('.format_number($stats['Likert']['CH-C']['stddev.s'],1).")
            </td>
            ".sig_cell($stats['Likert']['t.equal']['p2'], 'thin-top heavy-right thin-bottom thin-left')."
            <td class='thin-top mid-right thin-bottom heavy-left'>
              ".format_number($stats['LikertTime']['full']['mean'],2).' ('.format_number($stats['LikertTime']['full']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom mid-left'>
              ".format_number($stats['LikertTime']['CH-G']['mean'],2).' ('.format_number($stats['LikertTime']['CH-G']['stddev.s'],1).")
            </td>
            <td class='thin-top thin-right thin-bottom thin-left'>
              ".format_number($stats['LikertTime']['CH-C']['mean'],2).' ('.format_number($stats['LikertTime']['CH-C']['stddev.s'],1).")
            </td>
            ".sig_cell($stats['LikertTime']['t.equal']['p2'], 'thin-top heavy-right thin-bottom thin-left')."
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td colspan='100' class='heavy-all'>
              <sup>a</sup>Likert rating scale from 1 (strongly disagree) to 5 (strongly agree).
              *<em>P</em> value obtained from two-sample, two-tailed t-tests.
            </td>
          </tr>
        </tfoot>
      </table></div>";
  }
?>