<?php
function box_plot($series, $id = 'box_plot') {
  if(!is_array($series) || count($series) < 1) 
    return;

  echo "
      <div id='$id' class='chart'></div>
      <script>
        google.charts.setOnLoadCallback(drawBoxPlot);
        function drawBoxPlot() {
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Group');
          data.addColumn('number', 'Min');
          data.addColumn('number', '1st Quartile');
          data.addColumn('number', 'Mean');
          data.addColumn('number', 'Median');
          data.addColumn('number', '3rd Quartile');
          data.addColumn('number', 'Max');
          data.addColumn({id:'max', type:'number', role:'interval'});
          data.addColumn({id:'min', type:'number', role:'interval'});
          data.addColumn({id:'firstQuartile', type:'number', role:'interval'});
          data.addColumn({id:'median', type:'number', role:'interval'});
          data.addColumn({id:'thirdQuartile', type:'number', role:'interval'});
          data.addRows([";
  foreach ($series as $label => $data) {
    $min      = $data['quartiles']['0%'];
    $firstQ   = $data['quartiles']['Q1'] ?? $min;
    $mean     = $data['mean'];
    $median   = $data['quartiles']['Q2'];
    $thirdQ   = $data['quartiles']['Q3'] ?? $median;
    $max      = $data['quartiles']['100%'];
    echo "
            [ '". str_replace("'", '\\\'', $label). "', $min, $firstQ, $mean, $median, $thirdQ, $max, 
              $max, $min, $firstQ, $median, $thirdQ ],";
  }
  echo "
          ]);
          window.chartData = window.chartData || {};
          window.chartData['$id'] = data;

          window.chartOptions = window.chartOptions || {};
          window.chartOptions['$id'] = {
            height: 500,
            legend: {position: 'none'},
            hAxis: {
              gridlines: {color: '#fff'}
            },
            vAxis: {
              viewWindowMode: 'maximized'
            },
            lineWidth: 0,
            pointSize: 0,
            series: {
              2: { 
                pointSize: 10,
                pointShape: 'diamond' ,
                color: '#e43725'
              }
            },
            intervals: {
              barWidth: 1,
              boxWidth: 1,
              lineWidth: 2,
              style: 'boxes',
              color: '#3498db'
            },
            chartArea: {
              width: '80%', 
              height: '90%'
            },
            interval: {
              max: {
                style: 'bars',
                fillOpacity: 1,
                color: '#777'
              },
              min: {
                style: 'bars',
                fillOpacity: 1,
                color: '#777'
              }
            }
          };
        }
      </script>";


}