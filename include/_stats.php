<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/math-php.php');
  use MathPHP\Probability\Distribution\Continuous\StudentT;
  use MathPHP\Probability\Distribution\Continuous\F;
  use MathPHP\Statistics\ANOVA;
  use MathPHP\Statistics\Average;
  use MathPHP\Statistics\Correlation;
  use MathPHP\Statistics\Descriptive;
  use MathPHP\Statistics\Significance;

  function descriptive_stats($data) {
    $output              = array();
    $output['n']         = count($data);
    $output['mean']      = Average::mean($data);
    $output['median']    = Average::median($data);
    $output['var.s']     = Descriptive::sampleVariance($data);
    $output['stddev.s']  = Descriptive::standardDeviation($data, Descriptive::SAMPLE);
    $output['quartiles'] = Descriptive::quartiles($data);

    return $output;
  }

  function t_test_two_sample($data1, $data2, $label1, $label2) {
    $output = array(
      $label1 => descriptive_stats($data1),
      $label2 => descriptive_stats($data2),
      'full' => descriptive_stats(array_merge($data1, $data2)),
      'labels' => array($label1, $label2)
    );
    $output['full']['varpool.s'] = Descriptive::sampleVariancePooled($data1, $data2);
    $output['full']['stddevpool.s'] = sqrt($output['full']['varpool.s']);
    // only run t-test if means are uneual
    if($output[$label1]['mean'] != $output[$label2]['mean']) {
      $output['t.equal']    = Significance::tTestTwoSample(
          $output[$label1]['mean'],
          $output[$label2]['mean'],
          $output[$label1]['n'],
          $output[$label2]['n'],
          $output['full']['stddevpool.s'],
          $output['full']['stddevpool.s']
      );
      $output['t.unequal']  = Significance::tTestTwoSample(
          $output[$label1]['mean'],
          $output[$label2]['mean'],
          $output[$label1]['n'],
          $output[$label2]['n'],
          $output[$label1]['stddev.s'],
          $output[$label2]['stddev.s']
      );
    // otherwise return -1 as failure value
    } else {
      $output['t.equal'] = array( 't' => -1, 'p1' => -1, 'p2' => -1 );
      $output['t.unequal'] = array( 'p1' => -1, 'p2' => -1 );
    }
    return $output;
  }

  function t_test_two_sample_from_data($data, $labelCol, $valueCol) {
    $transpose = array();
    $label1 = false;
    $label2 = false;
    foreach ($data['rows'] as $row) {
      if(!array_key_exists($row[$labelCol], $transpose)) {
        $transpose[$row[$labelCol]] = array();
        if(!$label1)
          $label1 = $row[$labelCol];
        elseif(!$label2)
          $label2 = $row[$labelCol];
        else
          throw new Exception('T-Tests can only have two distinct labels');
      }
      $transpose[$row[$labelCol]][] = sanitize_data($row[$valueCol]);
    }

    return t_test_two_sample($transpose[$label1], $transpose[$label2], $label1, $label2);
  }

  function correlation($data1, $data2, $label1 = 'X', $label2 = 'Y') {
    $n = count($data1);
    if($n != count($data2))
      throw new Exception("Data sets for correlation analysis must have same number of elements" .
                          " (passed $n and ".count($data2).")");
    $output = array(
      $label1       => descriptive_stats($data1),
      $label2       => descriptive_stats($data2),
      'full'        => descriptive_stats(array_merge($data1, $data2)),
      'labels'      => array($label1, $label2),
      'correlation' => Correlation::describe($data1, $data2)
    );
    // Calculate p value
    // adapted from; http://janda.org/c10/Lectures/topic06/L24-significanceR.htm
    $df                           = $n - 2;
    $r                            = $output['correlation']['r'];
    $t                            = correlation_t($r, $df);
    $output['correlation']['df']  = $df;
    $output['correlation']['t']   = $t;
    $output['correlation']['p1']  = StudentT::above(abs($t), $df);
    $output['correlation']['p2']  = StudentT::outside(-abs($t), abs($t), $df);

    return $output;
  }

  function correlation_t($r, $df) {
    return $r * sqrt($df / (1 - $r**2));
  }

  function correlation_from_data($data, $labelCol, $valueCol, $suffix) {
    $transpose = array(
      'values' => array(
        $labelCol => array(),
        $valueCol => array()
      ),
      'ranks'  => array(
        $labelCol => array(),
        $valueCol => array()
      )
    );
// echo"<pre>labelCol: $labelCol, valueCol: $valueCol, suffix: $suffix</pre>";
// Layout::print_r($data);
    foreach ($data['rows'] as $row) {
      $transpose['values'][$labelCol][] = sanitize_data($row[$labelCol]);
      $transpose['values'][$valueCol][] = sanitize_data($row[$valueCol]);
      $transpose['ranks'][$labelCol][]  = sanitize_data($row[$labelCol.$suffix]);
      $transpose['ranks'][$valueCol][]  = sanitize_data($row[$valueCol.$suffix]);
    }

    return array(
      'values' => correlation($transpose['values'][$labelCol], $transpose['values'][$valueCol], $labelCol, $valueCol),
      'ranks'  => correlation($transpose['ranks'][$labelCol], $transpose['ranks'][$valueCol],  $labelCol.$suffix, $valueCol.$suffix)
    );
  }

  // adapted from: http://psc.dss.ucdavis.edu/sommerb/sommerdemo/stat_inf/tutorials/tcorrhand.htm
  function t_test_paired($data1, $data2, $label1, $label2) {
    if(count($data1) != count($data2))
      throw new Exception("Data sets for t_test_paired must have the same number of elements");

    $output = array(
      $label1   => descriptive_stats($data1),
      $label2   => descriptive_stats($data2),
      'full'    => descriptive_stats(array_merge($data1, $data2)),
      'labels'  => array($label1, $label2)
    );

    $varpool = Descriptive::sampleVariancePooled($data1, $data2, 
          $output[$label1]['var.s'], $output[$label2]['var.s']);
    $output['full']['varpool.s'] = $varpool;
    $output['full']['stddevpool.s'] = sqrt($varpool);
    try {
      $output['full']['r'] = Correlation::r($data1, $data2);
    } catch (Exception $e) {
      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
    $output['t.paired'] = Significance::tTestPaired($data1, $data2);

    return $output;
  }

  function t_test_paired_from_data($data, $identCol, $valueCol, $suffix1 = 'Pre', $suffix2 = 'Post') {
    $label1 = $valueCol . $suffix1;
    $label2 = $valueCol . $suffix2;
    $data1  = array();
    $data2  = array();

    foreach ($data['rows'] as $row) {
      $data1[] = sanitize_data($row[$label1]);
      $data2[] = sanitize_data($row[$label2]);
    }

    return t_test_paired($data1, $data2, $label1, $label2);
  }

  function anova_one_way($samples) {
    $labels           = array_keys($samples);
    $output           = call_user_func_array('MathPHP\Statistics\ANOVA::oneWay', array_values($samples));
    $output['labels'] = $labels;
    $allSamples       = array();
    // label descriptive statistics by label, not index
    for ($i = 0; $i < count($samples); $i++) { 
      $output['data_summary'][$labels[$i]] = $output['data_summary'][$i];
      $output['data_summary'][$labels[$i]]['quartiles'] =
        Descriptive::quartiles($samples[$labels[$i]]);
      unset($output['data_summary'][$i]);
      $allSamples = array_merge($allSamples, $samples[$labels[$i]]);
    }
    $output['total_summary']['quartiles'] =
      Descriptive::quartiles($allSamples);

    return $output;
  }

  function anova_one_way_from_data($data, $labelCol, $valueCol) {
    $samples = array();

    foreach ($data['rows'] as $row) {
      if(!isset($samples[$row[$labelCol]]))
        $samples[$row[$labelCol]] = array();
      $samples[$row[$labelCol]][] = sanitize_data($row[$valueCol]);
    }

    return anova_one_way($samples);
  }

  function mcnemar_exact($b, $c) {
    // a and d parameters aren't used in mcnemar, so just pass 0
    // build R command
    $cmd = "R -e \"mcnemar.test(matrix(c(0,$b,$c,0), ncol=2, byrow=T))\"";
    // allocate results array
    $res = array();
    // call R from command line
    exec($cmd, $res);
    // iterate result lines
    foreach ($res as $line) {
      // find line with stats
      if(strpos($line, "McNemar's chi-squared") === 0) {
        // extract stats to build output
        $output = array();
        $start = 0;
        // get chi-squared
        $start = strpos($line, ' = ', $start) + 3;
        $end = strpos($line, ',', $start);
        $output['chi-squared'] = substr($line, $start, $end - $start);
        // get degrees of freedom
        $start = strpos($line, ' = ', $start) + 3;
        $end = strpos($line, ',', $start);
        $output['df'] = substr($line, $start, $end - $start);
        // get p-value
        $start = strpos($line, ' = ', $start) + 3;
        $output['p'] = substr($line, $start);
        // return stats
        return $output;
      }
    }
    return false;
  }

  function t_cronbach_alpha($data) {
    $n = count($data);
    $k = count($data[0]);

    // build R command
    $cmd = "R -e \"library(psy);cronbach(matrix(c(";
    $isFirst = true;
    // build matrix
    for($i = 0; $i < $n; $i++) {
      if(count($data[$i]) != $k)
        return array (
          'alpha'   => -1,
          'error' => "All sets passed to cronbach_alpha must have same number of items"
        );
      for($j = 0; $j < $k; $j++) {
        if(!$isFirst) $cmd .= ',';
        $isFirst = false;
        $cmd .= $data[$i][$j];
      }
    }
    // end R command
    $cmd .= "), ncol=" . count($data[0]) . ", byrow=T))\"";

    // call R from command line
    $res = shell_exec($cmd);//, $res);
    $start = strpos($res, "\$alpha");
    $end = strpos($res, "\n", $start);
    // return results
    return array (
      'alpha'   => substr($res, $start + 11, $end - $start + 3),
      'samples' => $n,
      'items'   => $k
    );
  }

  function sanitize_data($data) {
    if($data == 'True')
      return 1.0;
    elseif($data == 'False')
      return 0.0;
    return $data;
  }