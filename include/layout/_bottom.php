
    <?php if(Layout::$EmitContainer) { ?></div><?php } ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- include custom JavaScript last -->
    <script type="text/javascript" src="/assets/page.js"></script>
<?php
    if(Layout::$IncludeGoogleChart)
      echo '
    <script type="text/javascript" src="/assets/charts.js"></script>
';
  ?>
  </body>
</html><?php exit();