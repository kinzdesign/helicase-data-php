<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_load_question.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_data_table.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_box_plot.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_scatter_plot.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_correlation_tables.php');

  function render_correlation($data, $labelCol, $valueCol, $suffix = 'Rank') {
    try
    {
      $labelTitle = ucwords($labelCol);
      $valueTitle = ucwords($valueCol);
      $title = "$valueTitle by $labelTitle";
      $results = correlation_from_data($data, $labelCol, $valueCol, $suffix);
      Layout::EmitTop($title, true);
      if($suffix) {
        echo "
          <div class='row'>
            <div class='col-sm-6'>";
        scatter_plot($data['rows'], $labelCol, $valueCol, 'scatter_values');
        correlation_tables($results['values'], $labelCol, $valueCol, $suffix);
        echo "
            </div>
            <div class='col-sm-6'>";
        scatter_plot($data['rows'], $labelCol.$suffix, $valueCol.$suffix, 'scatter_rank');
        correlation_tables($results['ranks'], $labelCol, $valueCol, $suffix);
        echo "
            </div>
          </div>";
        data_table($data);
      } else {
        echo "
          <div class='row'>
            <div class='col-sm-6'>";
        correlation_tables($results['values'], $labelCol, $valueCol, $suffix);
        echo "
            </div>
            <div class='col-sm-6'>";
        scatter_plot($data['rows'], $labelCol, $valueCol, 'scatter_values');
        data_table($data);
        echo "
            </div>
          </div>";
      }
      Layout::EmitBottom();
    } catch (Exception $e) {
      Layout::RenderException($e);
    }
  }