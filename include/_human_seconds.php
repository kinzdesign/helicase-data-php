<?php
// adapted from http://stackoverflow.com/a/13878394
function human_seconds($sec)
{
    if(!is_numeric($sec))
        return $sec;
    $sign = $sec < 0 ? "-" : "";
    $sec = abs($sec);
    $min = floor($sec / 60);
    $sec = $sec % 60;
    $hr = floor($min / 60);
    $min = $min % 60;
    $day = floor($hr / 60);
    $hr = $hr % 60;
    if($hr < 10) $hr = "0$hr";
    if($min < 10) $min = "0$min";
    if($sec < 10) $sec = "0$sec";
    return "$sign{$hr}:{$min}:{$sec}";
}
?>