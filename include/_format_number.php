<?php
  function format_number($number, $places = 5) {
    if($number == 0)
      return 0;

    if($places == 0) {
      return round_force($number, 0);
    }
    if($places == 1) {
      return round_force($number, $number > 99 ? 0 : 1);
    }
    if($places == 2) {
      return round_force($number, $number > 99 ? ($number > 999 ? 0 : 1) : 2);
    }
    if(abs($number) / 1000  > 1)
      return number_format($number, 0);
    if(abs($number) / 100   > 1)
      return round_force($number, 1);
    if(abs($number) / 10    > 1)
      return round_force($number, 2);
    if(abs($number) * 10    > 1)
      return round_force($number, 3);
    if(abs($number) * 100   > 1)
      return round_force($number, 4);
    $power = floor(log($number, 10));
    return round_force(($number / pow(10, $power)), 2) . 'e' . $power;
  }

  function round_force($number, $digits) {
    return sprintf("%.{$digits}f", round($number,$digits));
  }
?>