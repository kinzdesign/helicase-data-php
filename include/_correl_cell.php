<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');

function correl_cell($rValue, $baseClasses = '', $colSpan = 1, $strongThreshold = 0.5, 
  $classIfStrong = 'sig', $classIfNotStrong = 'insig') 
{
	$num = format_number($rValue);
  return '<td' .
    ($colSpan > 1 ? " colspan='$colSpan'" : '') .
    " class='$baseClasses " .
    ($rValue >= $strongThreshold ? $classIfStrong : $classIfNotStrong) .
    "'>$num</td>";
}

