<?php
  function print_aggregate_likert_data($quiz_code, $question_id, $question, $script = '') {
    echo "
                    <div class='panel panel-warning'>
                      <div class='panel-heading'>
                        <h5 class='panel-title'>{$question['likertIntroHtml']}</h5>
                      </div>
                      <div class='panel-body'>
                        <div class='row'>
                          <div class='col-sm-6'>
                            <div id='c-l-a-$question_id' class='chart chart-likert'></div>
                          </div>
                          <div class='col-sm-6'>
                            <div id='c-l-b-$question_id' class='chart chart-likert'></div>
                          </div>
                        </div>
                      </div>
                    </div>";
    $script .= "
      tmpData = new google.visualization.DataTable();
      tmpData.addColumn('string', 'Cohort');
      tmpData.addColumn('number', 'Average Likert Value');
      tmpData.addRows([
        ['CH-C', " . get_value($question['likert'], 'likertAvg', 'CH-C') . "],
        ['CH-G', " . get_value($question['likert'], 'likertAvg', 'CH-G') . "]
      ]);
      window.chartData['c-l-a-$question_id'] = tmpData;
      window.chartData['c-l-b-$question_id'] = google.visualization.arrayToDataTable([
        ['Cohort', '0', '1' ,'2', '3', '4', '5', { role: 'annotation' } ],
        ['CH-C', " . 
          get_value($question['likert'], 'likertN0', 'CH-C') . ", " . 
          get_value($question['likert'], 'likertN1', 'CH-C') . ", " . 
          get_value($question['likert'], 'likertN2', 'CH-C') . ", " . 
          get_value($question['likert'], 'likertN3', 'CH-C') . ", " . 
          get_value($question['likert'], 'likertN4', 'CH-C') . ", " . 
          get_value($question['likert'], 'likertN5', 'CH-C') . ", ''],
        ['CH-G', " . 
          get_value($question['likert'], 'likertN0', 'CH-G') . ", " . 
          get_value($question['likert'], 'likertN1', 'CH-G') . ", " . 
          get_value($question['likert'], 'likertN2', 'CH-G') . ", " . 
          get_value($question['likert'], 'likertN3', 'CH-G') . ", " . 
          get_value($question['likert'], 'likertN4', 'CH-G') . ", " . 
          get_value($question['likert'], 'likertN5', 'CH-G') . ", '']
      ]);";
    return $script;
  }

  function get_value($array, $key1, $key2, $default = 0) {
    if(isset($array[$key1]) && isset($array[$key1][$key2]))
      return $array[$key1][$key2];
    return $default;
  }
?>