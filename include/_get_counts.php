<?php
  function get_counts() {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
    $db = get_database_backup();

    $output = array();

    // get all participants
    $res = $db->query("
      SELECT  COUNT(*) AS consented,
              COUNT(finishedStamp) AS finished,
              SUM(CASE WHEN finishedStamp IS NOT NULL AND basePaidAt IS NULL THEN 1 ELSE 0 END) AS unpaid
      FROM    vw_participants
      WHERE   isValid = 1
    ");
    check_for_db_error($res, $db);
    $output = $res->fetch_assoc();

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }

?>