<?php
function bubble_plot($data, $xLabel, $yLabel, $id = 'c-c-b') {
  if(!is_array($data) || count($data) < 1) 
    return;
  // compute counts
  $counts = array();
  foreach ($data as $row) {
    $x = $row[$xLabel];
    $y = $row[$yLabel];
    if(!isset($counts[$x]))
      $counts[$x] = array();
    if(!isset($counts[$x][$y]))
      $counts[$x][$y] = 1;
    else
      $counts[$x][$y]++;
  }

  echo "
      <div id='$id' class='chart'></div>
      <script>
        google.charts.setOnLoadCallback(drawBubblePlot);
        function drawBubblePlot() {
          var data = google.visualization.arrayToDataTable([
            [ 'ID', '$xLabel', '$yLabel', '', 'N' ]";
  foreach ($counts as $x => $ys) {
    foreach ($ys as $y => $n) {
      echo ",
            [ '$x, $y = $n', $x, $y, '', $n ]";
    }
  }
  echo "
          ]);
          window.chartData = window.chartData || {};
          window.chartData['$id'] = data;
          showChart(\$('#$id'));
        }
      </script>";
}