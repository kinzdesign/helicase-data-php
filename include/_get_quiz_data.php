<?php
  function get_quiz_data($quiz, $minDisplayOrder = 0, $maxDisplayOrder = 254) {
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_stats.php');
    $db = get_database();

    $axes = [ 'Likert', 'LikertTime', 'Score', 'TimeBonus', 'TotalScore', 'Correct', 'MultiTime' ];
    $output = array(
      'Summary'   => array(),
      'Questions' => array()
    );
    // get summary data from DB
    $res = $db->query("
      SELECT  p.participant AS Participant,
              p.cohort AS Cohort,
              SUM(COALESCE(r.likert, 0)) AS Likert,
              SUM(COALESCE(r.score, 0)) AS Score,
              SUM(COALESCE(r.timeBonus, 0)) AS TimeBonus,
              SUM(COALESCE(r.score, 0) + COALESCE(r.timeBonus, 0)) AS TotalScore,
              SUM(CASE WHEN r.score > 0 THEN 1 ELSE 0 END) AS Correct, 
              SUM(UNIX_TIMESTAMP(r.submittedMulti) - UNIX_TIMESTAMP(r.sentMulti)) AS MultiTime,
              SUM(UNIX_TIMESTAMP(r.submittedLikert) - UNIX_TIMESTAMP(r.sentLikert)) AS LikertTime,
              GROUP_CONCAT(r.likert SEPARATOR ',') AS Likerts
      FROM    vw_participants AS p
              INNER JOIN responses AS r ON p.participant = r.participant
              INNER JOIN questions AS q ON r.question = q.question
      WHERE   p.isValid = 1
              AND LEFT(q.quiz, 5) = '$quiz'
              AND q.displayOrder >= $minDisplayOrder
              AND q.displayOrder <= $maxDisplayOrder
      GROUP   BY p.cohort DESC, p.participant");
    check_for_db_error($res, $db);
    $tmp = array(
      'Total'   => init_bucket($axes),
      'CH-G'    => init_bucket($axes),
      'CH-C'    => init_bucket($axes)
    );
    $summary = array();
    while($q = $res->fetch_assoc()) {
      $cohort = $q['Cohort'];
      foreach ($axes as $key) {
        $tmp[$cohort][$key][]  = $q[$key];
        $tmp['Total'][$key][]  = $q[$key];
      }
      $likerts = explode(',', $q['Likerts']);
      $tmp[$cohort]['Likerts'][]  = $likerts;
      $tmp['Total']['Likerts'][]  = $likerts;
    }

    // compute statistics
    foreach ($axes as $key) {
      $output['Summary'][$key] =
        t_test_two_sample(
          $tmp['CH-G'][$key],
          $tmp['CH-C'][$key],
          'CH-G', 'CH-C'
        );
    }
    foreach ($tmp as $cohort => $vals) {
      $output['Summary']['alpha'][$cohort] = t_cronbach_alpha($vals['Likerts']);
    }

    // get question-level data from DB
    $res = $db->query("
      SELECT  p.participant AS Participant,
              p.cohort AS Cohort,
              q.displayOrder AS Question, 
              COALESCE(q.header, q.multiIntroHtml, q.likertIntroHtml) AS Header,
              -- r.likert AS Likert,
              r.likert AS Likert,
              -- COALESCE(r.likert, 0) AS Likert,
              COALESCE(r.score, 0) AS Score,
              COALESCE(r.timeBonus, 0) AS TimeBonus,
              COALESCE(r.score, 0) + COALESCE(r.timeBonus, 0) AS TotalScore,
              CASE WHEN r.score > 0 THEN 1 ELSE 0 END AS Correct, 
              UNIX_TIMESTAMP(r.submittedMulti) - UNIX_TIMESTAMP(r.sentMulti) AS MultiTime,
              UNIX_TIMESTAMP(r.submittedLikert) - UNIX_TIMESTAMP(r.sentLikert) AS LikertTime
      FROM    vw_participants AS p
              INNER JOIN responses AS r ON p.participant = r.participant
              INNER JOIN questions AS q ON r.question = q.question
      WHERE   p.isValid = 1
              AND LEFT(q.quiz, 5) = '$quiz'
              AND q.displayOrder >= $minDisplayOrder
              AND q.displayOrder <= $maxDisplayOrder
              AND likert > 0
      ORDER   BY q.displayOrder, p.cohort DESC, p.participant");
    check_for_db_error($res, $db);
    $tmp = array();
    while($q = $res->fetch_assoc()) {
      $question = $q['Question'];
      // ensure buckets exist
      if(!isset($tmp[$question])) {
        $tmp[$question] = array(
          'Total'   => init_bucket($axes),
          'CH-G'    => init_bucket($axes),
          'CH-C'    => init_bucket($axes)
        );
        $output['Questions'][$question] = array(
          'Header'  => $q['Header']
        );
      }
      // update buckets
      $cohort = $q['Cohort'];
      foreach ($axes as $key) {
        $tmp[$question][$cohort][$key][]  = $q[$key];
        $tmp[$question]['Total'][$key][]  = $q[$key];
      }
    }

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    // compute statistics
    foreach ($tmp as $question => $qData) {
      foreach ($axes as $key) {
        $t = t_test_two_sample(
            $tmp[$question]['CH-G'][$key],
            $tmp[$question]['CH-C'][$key],
            'CH-G', 'CH-C'
          );
        $output['Questions'][$question][$key] =
          $t;
      }
    }
    $output['Axes'] = $axes;

    return $output;
  }

  function init_bucket($axes) {
    $output = array();
    foreach ($axes as $key) {
      $output[$key] = array();
    }
    return $output;
  }
?>