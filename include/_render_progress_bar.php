<?php
  function render_progress_bar($finished = 0, $incomplete = 0) {
    echo "
        <div class='progress'>
          <div
            class='progress-bar progress-bar-striped progress-bar-success' 
            role='progressbar' 
            aria-valuenow='$finished' 
            aria-valuemin='0' 
            aria-valuemax='100' 
            style='width: $finished%;'
            title='$finished finished'
          >
            $finished / 100
          </div>
          <div 
            class='progress-bar progress-bar-striped progress-bar-warning' 
            role='progressbar' 
            aria-valuenow='$incomplete' 
            aria-valuemin='0' 
            aria-valuemax='100' 
            style='width: $incomplete%;'
            title='$incomplete incomplete'
          >
            $incomplete incomplete
          </div>
        </div>";
  }
?>