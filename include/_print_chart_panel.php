<?php
  function print_chart_panel($header, $idSuffix1, $idSuffix2 = false, $expanded = false) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_chart.php');
    echo "
        <div class='panel panel-danger'>
          <div class='panel-heading' role='tab' id='h-z$idSuffix1'>
            <h3 class='panel-title'>
              <a role='button' data-toggle='collapse' data-parent='#a-quiz' href='#c-z$idSuffix1' aria-controls='c-z$idSuffix1'" . 
              ($expanded ? " aria-expanded='true'" : '') .
              ">
                $header
              </a>
            </h3>
          </div>
          <div id='c-z$idSuffix1' class='panel-collapse collapse" . 
              ($expanded ? ' in' : '') .
              "' role='tabpanel' aria-labelledby='h-z$idSuffix1'>
            <div class='panel-body'>
              <div class='panel-group' id='a-z$idSuffix1' role='tablist' aria-multiselectable='true'>";
    if($idSuffix1 && $idSuffix2) {
      echo "
                <div class='row'>
                  <div class='col-sm-6'>
                    " . render_chart("c$idSuffix1") . "
                  </div>
                  <div class='col-sm-6'>
                    " . render_chart("c$idSuffix2") . "
                  </div>
                </div>";
    } else {
      echo "
                " . render_chart("c$idSuffix1");
    }
    echo '
              </div>
            </div>
          </div>
        </div>';
  }
?>