<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_stats.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_sig_cell.php');
function anova_one_way_tables($results, $labelCol, $valueCol) {
  $samplesN   = count($results['labels']);
  $samplesNp1 = $samplesN + 1;
  $topColSpan = max(1, floor($samplesN / 3));

  echo "
          <div class='table-responsive'><table class='table table-striped table-hover table-stats table-condensed'>
            <tbody>";
  echo "
              <tr class='info'>
                <th>ANOVA (One-Way)</th>
                <td colspan='$topColSpan'>Between Groups</td>
                <td colspan='$topColSpan'>Within Groups</td>
                <td colspan='$topColSpan'>Total</td>
              </tr>
              <tr>
                <th>Sum of Squares</th>
                <td colspan='$topColSpan'>".format_number($results['ANOVA']['treatment']['SS'])."</td>
                <td colspan='$topColSpan'>".format_number($results['ANOVA']['error']['SS'])."</td>
                <td colspan='$topColSpan'>".format_number($results['ANOVA']['total']['SS'])."</td>
              </tr>
              <tr>
                <th>Degrees of Freedom</th>
                <td colspan='$topColSpan'>{$results['ANOVA']['treatment']['df']}</td>
                <td colspan='$topColSpan'>{$results['ANOVA']['error']['df']}</td>
                <td colspan='$topColSpan'>{$results['ANOVA']['total']['df']}</td>
              </tr>
              <tr>
                <th>Mean of Squares</th>
                <td colspan='$topColSpan'>".format_number($results['ANOVA']['treatment']['MS'])."</td>
                <td colspan='$topColSpan'>".format_number($results['ANOVA']['error']['MS'])."</td>
                <td colspan='$topColSpan'></td>
              </tr>
              <tr>
                <th>F</th>
                <td colspan='$topColSpan'>".format_number($results['ANOVA']['treatment']['F'])."</td>
                <td colspan='$topColSpan'></td>
                <td colspan='$topColSpan'></td>
              </tr>
              <tr>
                <th>P-value</th>
                " . sig_cell($results['ANOVA']['treatment']['P'], '', $topColSpan) . "
                <td colspan='$topColSpan'></td>
                <td colspan='$topColSpan'></td>
              </tr>";
  echo "
              <tr class='info'>
                <th colspan='{$samplesNp1}'>Overall</th>
              </tr>
              <tr>
                <th>Observations</th>
                <td colspan='$samplesN'>{$results['total_summary']['n']}</td>
              </tr>
              <tr>
                <th>Mean $valueCol</th>
                <td colspan='$samplesN'>".format_number($results['total_summary']['mean'],2)."</td>
              </tr>
              <tr>
                <th>Variance</th>
                <td colspan='$samplesN'>".format_number($results['total_summary']['variance'])."</td>
              </tr>
              <tr>
                <th>Standard Deviation</th>
                <td colspan='$samplesN'>".format_number($results['total_summary']['sd'])."</td>
              </tr>";

  echo "
              <tr class='info'>
                <th>By $labelCol</th>";
  foreach ($results['labels'] as $label) {
    echo "
                <td>$label</td>";
  }
  echo "
              </tr>
              <tr>
                <th>Median</th>";
  foreach ($results['labels'] as $label) {
    echo "
                <td>{$results['data_summary'][$label]['n']}</td>";
  }
  echo "
              </tr>
              <tr>
                <th>Mean $valueCol</th>";
  foreach ($results['labels'] as $label) {
    echo "
                <td>".format_number($results['data_summary'][$label]['mean'],2)."</td>";
  }
  echo "
              </tr>
              <tr>
                <th>Variance</th>";
  foreach ($results['labels'] as $label) {
    echo "
                <td>".format_number($results['data_summary'][$label]['variance'])."</td>";
  }
  echo "
              </tr>
              <tr>
                <th>Standard Deviation</th>";
  foreach ($results['labels'] as $label) {
    echo "
                <td>".format_number($results['data_summary'][$label]['sd'])."</td>";
  }
  echo "
              </tr>
            </tbody>
          </table></div>";
}