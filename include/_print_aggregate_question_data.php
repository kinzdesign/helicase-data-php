<?php
  function print_aggregate_question_data($quiz_code, $question_id, $question, $script = '', $expanded = true) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_print_aggregate_likert_data.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_print_aggregate_multi_data.php');
    echo "
              <div class='panel panel-info'>
                <div class='panel-heading' role='tab' id='h-q-$question_id'>
                  <h4 class='panel-title'>
                    <a role='button' data-toggle='collapse' data-parent='#a-z-$quiz_code' href='#c-q-$question_id' aria-controls='c-q-$question_id'" . 
                    ($expanded ? " aria-expanded='true'" : '') .
                    ">
                      {$question['questionDisplayOrder']}: {$question['title']}
                    </a>
                  </h4>
                </div>
                <div id='c-q-$question_id' class='panel-collapse collapse" . 
                    ($expanded ? ' in' : '') .
                    "' role='tabpanel' aria-labelledby='h-q-$question_id'>
                  <div class='panel-body'>";
    if(isset($question['likert'])) {
      $script = print_aggregate_likert_data($quiz_code, $question_id, $question, $script);
    }
    if(isset($question['multi'])) {
      $script = print_aggregate_multi_data($quiz_code, $question_id, $question, $script);
    }
    echo '
                  </div>
                </div>
              </div>';
    return $script;
  }
?>