<?php

function render_quiz_charts($quizzes) {
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
  try
  {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_aggregates.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_print_aggregate_quiz_data.php');

    $aggregates = get_aggregates($quizzes);
    $script = '';
    
    Layout::EmitTop('Question Aggregates', true);
    echo '
        <div class="panel-group" id="a-quiz" role="tablist" aria-multiselectable="true">';
    foreach($aggregates as $quiz_code => $quiz) {
      $script = print_aggregate_quiz_data($quiz_code, $quiz, $script, !is_array($quizzes));
    }
    echo "
        </div>
        <script>
          window.chartData = window.chartData || {};
          google.charts.setOnLoadCallback(initChartData);
          function initChartData() {
            $script
          }

        </script>";

    Layout::EmitBottom();
  } catch (Exception $e) {
    Layout::RenderException($e);
  }
}
