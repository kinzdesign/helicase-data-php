<?php
function data_table($data) {
  if(!is_array($data) || count($data) < 1) 
    return;
  echo "
        <div class='panel panel-info'>
            <div class='panel-heading' role='tab' id='dataHeading'>
              <h4 class='panel-title'>
                <a title='Expand/Collapse' role='button' data-toggle='collapse' href='#dataCollapse' aria-expanded='true' aria-controls='dataCollapse' class='collapsed'>
                  <i class='fa fa-chevron'></i>
                  Data Table
                </a>
              </h4>
            </div>
            <div id='dataCollapse' class='panel-collapse collapse' role='tabpanel' aria-labelledby='dataHeading'>
              <div class='table-responsive'><table class='table table-striped table-condensed table-hover table-data'>
                <thead>
                  <tr>";
  foreach ($data['rows'][0] as $key => $value){
      echo "
                      <th>$key</th>";
  }
  echo "
                  </tr>
                </thead>
                <tbody>";
  foreach ($data['rows'] as $row) {
    echo "
                  <tr>";
    foreach ($row as $cell) {
      echo "
                    <td>$cell</td>";
    }
    echo "
                </tr>";
  }
  echo "
                </tbody>
              </table></div>
            </div>
          </div>";
}