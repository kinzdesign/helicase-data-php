<?php
  function print_aggregate_multi_data($quiz_code, $question_id, $question, $script = '') {
    if(isset($question['multi'])) {
      echo "
                    <div class='panel panel-warning'>
                      <div class='panel-heading'>
                        <h5 class='panel-title'>{$question['multiIntroHtml']}</h5>
                      </div>
                      <div class='panel-body'>
                        <div id='c-m-a-$question_id' class='chart chart-likert'></div>";
      echo "
                      </div>
                    </div>";
      $script .= "
        window.chartData['c-m-a-$question_id'] = google.visualization.arrayToDataTable([
          ['Cohort', ";
      foreach($question['multi']['CH-C'] as $a) {
        $title = str_replace("'", '&apos;', $a['title']);
        $script .= "'$title', ";
      }

      $script .= " { role: 'annotation' } ]";
      foreach($question['multi'] as $cohort => $answers) {
        $script .= ",
            ['$cohort', ";
        foreach($answers as $a) {
          $script .= "{$a['answerN']}, ";
        }
        $script .= "'']";
      }
      $script .= "
        ]);";
    }

    return $script;
  }
?>