<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_stats.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_sig_cell.php');
function t_test_two_sample_tables($results, $labelCol, $valueCol) {
  if(!is_array($results) || count($results) < 1) 
    return;

  $label1     = $results['labels'][0];
  $label2     = $results['labels'][1];
  echo "
          <div class='table-responsive'><table class='table table-striped table-hover table-stats table-condensed'>
            <tbody>
              <tr class='info'>
                <th colspan='3'>T-Test (Equal Variances)</th>
              </tr>
              <tr>
                <th>t Stat</th>
                <td colspan='2'>".format_number($results['t.equal']['t'])."</td>
              </tr>
              <tr>
                <th>P(T &leq; t) one-tail</th>
                " . sig_cell($results['t.unequal']['p1'], '', 2) . "
              </tr>
              <tr>
                <th>P(T &leq; t) two-tail</th>
                " . sig_cell($results['t.unequal']['p2'], '', 2) . "
              </tr>

              <tr class='info'>
                <th colspan='3'>T-Test (Unequal Variances)</th>
              </tr>
              <tr>
                <th>t Stat</th>
                <td colspan='2'>".format_number($results['t.unequal']['t'])."</td>
              </tr>
              <tr>
                <th>P(T &leq; t) one-tail</th>
                " . sig_cell($results['t.unequal']['p1'], '', 2) . "
              </tr>
              <tr>
                <th>P(T &leq; t) two-tail</th>
                " . sig_cell($results['t.unequal']['p2'], '', 2) . "
              </tr>

              <tr class='info'>
                <th colspan='3'>Overall</th>
              </tr>
              <tr>
                <th>Observations</th>
                <td colspan='2'>{$results['full']['n']}</td>
              </tr>
              <tr>
                <th>Mean $valueCol</th>
                <td colspan='2'>".format_number($results['full']['mean'],2)."</td>
              </tr>
              <tr>
                <th>Median $valueCol</th>
                <td colspan='2'>{$results['full']['median']}</td>
              </tr>
              <tr>
                <th>Variance</th>
                <td colspan='2'>".format_number($results['full']['var.s'],2)."</td>
              </tr>
              <tr>
                <th>Standard Deviation</th>
                <td colspan='2'>".format_number($results['full']['stddev.s'],1)."</td>
              </tr>
              <tr>
                <th>Pooled Variance</th>
                <td colspan='2'>".format_number($results['full']['varpool.s'],2)."</td>
              </tr>
              <tr>
                <th>Pooled Standard Deviation</th>
                <td colspan='2'>".format_number($results['full']['stddevpool.s'],2)."</td>
              </tr>
              <tr class='info'>
                <th>By $labelCol</th>
                <td>$label1</td>
                <td>$label2</td>
              </tr>
              <tr>
                <th>Observations</th>
                <td>{$results[$label1]['n']}</td>
                <td>{$results[$label2]['n']}</td>
              </tr>
              <tr>
                <th>Mean $valueCol</th>
                <td>".format_number($results[$label1]['mean'],2)."</td>
                <td>".format_number($results[$label2]['mean'],2)."</td>
              </tr>
              <tr>
                <th>Median $valueCol</th>
                <td>{$results[$label1]['median']}</td>
                <td>{$results[$label2]['median']}</td>
              </tr>
              <tr>
                <th>Variance</th>
                <td>".format_number($results[$label1]['var.s'],2)."</td>
                <td>".format_number($results[$label2]['var.s'],2)."</td>
              </tr>
              <tr>
                <th>Standard Deviation</th>
                <td>".format_number($results[$label1]['stddev.s'],1)."</td>
                <td>".format_number($results[$label2]['stddev.s'],1)."</td>
              </tr>
            </tbody>
          </table></div>";
}