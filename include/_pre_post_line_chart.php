<?php
function pre_post_line_chart($t, $labelCol, $valueCol, $suffix1, $suffix2, $id = 'line_chart') {
  if(!is_array($t) || count($t) < 1) 
    return;

  $n    = $t[$valueCol.$suffix1]['n'];
  $val1 = $t[$valueCol.$suffix1]['mean'];
  $val2 = $t[$valueCol.$suffix2]['mean'];

  echo "
      <div id='$id' style='background-color:blue;'></div>
      <script>
        google.charts.setOnLoadCallback(drawScatterPlot);
        function drawScatterPlot() {
          var data = google.visualization.arrayToDataTable([
            [ 'Interval', '$labelCol' ],
            [ '$suffix1', $val1 ],
            [ '$suffix2', $val2 ]
          ]);
          var options = {
            height: 500,
            legend: {position: 'none'},
            colors: [ '#3498db' ]
          };
          var chart = new google.visualization.LineChart(document.getElementById('$id'));
          chart.draw(data, options);

        }
      </script>";


}