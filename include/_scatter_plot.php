<?php
function scatter_plot($data, $xLabel, $yLabel, $id = 'scatter_plot', $trendline = true) {
  if(!is_array($data) || count($data) < 1) 
    return;

  echo "
      <div id='$id' class='chart'></div>
      <script>
        google.charts.setOnLoadCallback(drawScatterPlot);
        function drawScatterPlot() {
          var data = google.visualization.arrayToDataTable([
            ['$xLabel', '$yLabel'],";
  foreach ($data as $row) {
    echo "
            [ {$row[$xLabel]}, {$row[$yLabel]} ],";
  }
  echo "
          ]);
          window.chartData = window.chartData || {};
          window.chartData['$id'] = data;
          var options = {
            height: 500,
            legend: {position: 'none'},
            hAxis: { title: '$xLabel' },
            vAxis: { title: '$yLabel' },
            colors: [ '#3498db' ]";
  if($trendline) {
    echo ",
            trendlines: {
              0: { type: 'linear', color: '#e74c3c' }
            }";
  }
  echo "
          };
          var chart = new google.visualization.ScatterChart(document.getElementById('$id'));
          chart.draw(data, options);
          window.charts = window.charts || {};
          window.charts['$id'] = chart;
        }
      </script>";
}