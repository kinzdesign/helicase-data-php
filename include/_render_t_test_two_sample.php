<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_load_question.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_data_table.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_box_plot.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_t_test_two_sample_tables.php');

  function render_t_test_two_sample($res, $question, $labelCol, $valueCol) {
    try
    {
      $data = array('rows' => array());
      while($q = $res->fetch_assoc()) {
        $data['rows'][] = $q;
      }
      $data = load_question($question, $data);
      $labelTitle = ucwords($labelCol);
      $valueTitle = ucwords($valueCol);
      $title = "$valueTitle by $labelTitle";
      if(isset($data['question']))
        $title = "{$data['question']['quiz']} #{$data['question']['displayOrder']}: $title";
      Layout::EmitTop($title, true);

      $results = t_test_two_sample_from_data($data, $labelCol, $valueCol);

      if(isset($data['question'])) {
        echo "
          <h2 class='question'>{$data['question']['header']}</h2>";
      }
      echo "
          <div class='row'>
            <div class='col-sm-7'>";

      t_test_two_sample_tables($results, $labelCol, $valueCol);

      echo "
            </div>
            <figure class='col-sm-5'>";

      $series = array();
      foreach ($results['labels'] as $label) {
        $series[$label] = $results[$label];
      }
      $series['Overall'] = $results['full'];
      box_plot($series);

      data_table($data);

      echo "
        </figure>
      </div>";
      Layout::EmitBottom();
    } catch (Exception $e) {
      Layout::RenderException($e);
    }
  }