<?php
  function render_list($list, $delimeter = '|') {
    $items = explode($delimeter, $list);
    if(sizeof($items) < 1 || strlen($items[0]) < 1)
      return;
    echo "
                  <ul>";
    foreach ($items as $item)
      echo "
                    <li>$item</li>";
    echo "
                  </ul>";
  }
?>