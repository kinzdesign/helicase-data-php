<?php
class Layout {
  private static $TopEmitted          = false;
  private static $BottomEmitted       = false;
  public  static $Title               = 'Data Analysis';
  public  static $IncludeGoogleChart  = false;
  public  static $EmitContainer       = true;

// Layout Regions

  public static function EmitTop($title = null, $include_google_chart = false, $status_code = 200) {
    if(self::$TopEmitted) return;
    http_response_code($status_code);
    self::$Title = $title;
    self::$IncludeGoogleChart = $include_google_chart;
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/layout/_top.php');
    self::$TopEmitted = true;
  }

  public static function EmitPlainText($mimeType = 'text/plain', $download = false, $filename = 'download.txt', $status_code = 200) {
    if(self::$TopEmitted) return;
    http_response_code($status_code);
    header("Content-Type: $mimeType");
    if($download)
      header("Content-Disposition: attachment; filename=\"$filename\"");
    self::$TopEmitted = true;
  }

  public static function EmitBottom() {
    if(self::$BottomEmitted) return;
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/layout/_bottom.php');
    self::$BottomEmitted = true;
  }

// Error Handling

  public static function RenderException($e) {
    self::EmitTop('Error!', false, 500);
    echo '
      <h1>An error occurred!</h1>
      <h4>'.$e->getMessage().'</h4>
      <p><strong>In file:</strong> '.$e->getFile().'</p>
      <p><strong>At line:</strong> '.$e->getLine().'</p>
      <pre>'.$e->getTraceAsString().'</pre>';
    self::EmitBottom();
  }

  public static function RenderError($message, $title = 'Error!', $status_code = 500) {
    self::EmitTop($title, false, $status_code);
    echo "
      <h1>An error occurred!</h1>
      <h4>$message</h4>";
    self::EmitBottom();
  }

  public static function RenderBadRequest($message, $title = 'Bad Request!') {
    self::EmitTop($title , false, 400);
    echo "
      <h1>There was an error with your request.</h1>
      <h4>$message</h4>";
    self::EmitBottom();
  }

  public static function RenderNotFound($message, $title = 'Not Found!') {
    self::EmitTop($title, false, 404);
    echo "
      <h1>{$title}</h1>
      <h4>$message</h4>";
    self::EmitBottom();
  }

// Query String Getters

  public static function GetQueryString($key, $errorOnMissing = true) {
    if(isset($_GET[$key]) && strlen($_GET[$key]) > 0)
      return $_GET[$key];
    if($errorOnMissing)
      self::RenderBadRequest("Query string parameter '$key' is required");
  }

  public static function GetQueryStringNumeric($key, $errorOnMissing = true) {
    $val = self::GetQueryString($key, $errorOnMissing);
    if(is_numeric($_GET[$key]))
      return $_GET[$key] + 0;
    if($errorOnMissing)
      self::RenderBadRequest("Query string parameter '$key' must be a number (not '{$val}')");
  }

  public static function GetQueryStringInt($key, $errorOnMissing = true) {
    $val = self::GetQueryStringNumeric($key, $errorOnMissing);
    if(is_integer($val))
      return $val;
    if($errorOnMissing)
      self::RenderBadRequest("Query string parameter '$key' must be an integer (not '{$val}')");
  }

// Helper Functions

  public static function print_r($expr, $expr2 = null) {
    if (is_null($expr2)) {
      echo '<pre>';
      print_r($expr);
      echo '</pre>';
    } else {
      echo '<div class="row"><div class="col-sm-6"><pre>';
      print_r($expr);
      echo '</pre></div><div class="col-sm-6"><pre>';
      print_r($expr2);
      echo '</pre></div></div>';
    }
  }
}