<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_stats.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_correl_cell.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_sig_cell.php');
function correlation_tables($results, $labelCol, $valueCol) {
  if(!is_array($results) || count($results) < 1) 
    return;

  $label1     = $results['labels'][0];
  $label2     = $results['labels'][1];

  echo "
          <div class='table-responsive'><table class='table table-striped table-hover table-stats table-condensed'>
            <tbody>
              <tr class='info'>
                <th>Correlation</th>
                <td colspan='2'>$label1 vs. $label2</td>
              </tr>
              <tr>
                <th>Covariance</th>
                <td colspan='2'>".format_number($results['correlation']['cov'])."</td>
              </tr>
              <tr>
                <th>Pearson&rsquo;s r (<em>r</em>)</th>
                ".correl_cell($results['correlation']['r'], '', 2)."
              </tr>
              <tr>
                <th>Coeff. Determination (<em>R</em>&sup2;)</th>
                <td colspan='2'>".format_number($results['correlation']['R2'])."</td>
              </tr>
              <tr>
                <th>Kendall&rsquo;s tau (&tau;)</th>
                <td colspan='2'>".format_number($results['correlation']['tau'])."</td>
              </tr>
              <tr>
                <th>Spearman&rsquo;s rho (&rho;)</th>
                <td colspan='2'>".format_number($results['correlation']['rho'])."</td>
              </tr>
              <tr>
                <th>Degrees of Freedom</th>
                <td colspan='2'>{$results['correlation']['df']}</td>
              </tr>
              <tr>
                <th>t Stat</th>
                <td colspan='2'>".format_number($results['correlation']['rho'])."</td>
              </tr>
              <tr>
                <th>P(T &leq; t) one-tail</th>
                " . sig_cell($results['correlation']['p1'], '', 2) . "
              </tr>
              <tr>
                <th>P(T &leq; t) two-tail</th>
                " . sig_cell($results['correlation']['p2'], '', 2) . "
              </tr>

              <tr class='info'>
                <th>Descriptive Statistics</th>
                <td>$label1</td>
                <td>$label2</td>
              </tr>
              <tr>
                <th>Observations</th>
                <td>{$results[$label1]['n']}</td>
                <td>{$results[$label2]['n']}</td>
              </tr>
              <tr>
                <th>Mean $valueCol</th>
                <td>".format_number($results[$label1]['mean'],2)."</td>
                <td>".format_number($results[$label2]['mean'],2)."</td>
              </tr>
              <tr>
                <th>Median $valueCol</th>
                <td>{$results[$label1]['median']}</td>
                <td>{$results[$label2]['median']}</td>
              </tr>
              <tr>
                <th>Variance</th>
                <td>".format_number($results[$label1]['var.s'],2)."</td>
                <td>".format_number($results[$label2]['var.s'],2)."</td>
              </tr>
              <tr>
                <th>Standard Deviation</th>
                <td>".format_number($results[$label1]['stddev.s'],1)."</td>
                <td>".format_number($results[$label2]['stddev.s'],1)."</td>
              </tr>
            </tbody>
          </table></div>";
}