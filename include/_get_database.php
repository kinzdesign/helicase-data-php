<?php
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_config.php');

	function get_database_backup($environment = false) {
		return get_database($environment, true);
	}

	function get_database($environment = false, $backup = false) {
		$config = get_config();
		if(!$environment) $environment = $config['DEFAULT_ENVIRONMENT'];
		$env = $config['MYSQL'][$environment];
		
		$db = mysqli_init();
		mysqli_options($db, MYSQLI_OPT_SSL_VERIFY_SERVER_CERT, true);
		$db->ssl_set(NULL,NULL,$env['SSL_CERT'],NULL,NULL);

		$db->real_connect(
			$env['HOST'],
			$backup ? $env['BUP_USER'] : $env['WEB_USER'],
			$backup ? $env['BUP_PASS'] : $env['WEB_PASS'],
			$env['DATABASE'],
			$env['PORT'],
			NULL, MYSQLI_CLIENT_SSL);

		return $db;
	}

	function check_for_db_error($res, $db) {
	  if(!$res)
	    throw new Exception("DB Error {$db->errno} {$db->error}");
	}

	function get_rows_as_array($res, $db) {
		check_for_db_error($res, $db);
    $rows = array();
    while($q = $res->fetch_assoc()) {
      $rows[] = $q;
    }
    return $rows;
	}
?>