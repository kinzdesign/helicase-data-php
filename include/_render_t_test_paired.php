<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_load_question.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_box_plot.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_pre_post_line_chart.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_data_table.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_t_test_paired_tables.php');

function render_t_test_paired($data, $question, $identCol, $labelCol, $valueCol1, $valueCol2,
  $suffix1 = 'Pre', $suffix2 = 'Post') {
  try
  {
    $data = load_question($question, $data);
    $labelTitle  = ucwords($labelCol);
    $valueTitle1 = ucwords($valueCol1);
    if($valueCol2)
      $valueTitle2 = ucwords($valueCol2);

    $t1 = t_test_paired_from_data($data, $identCol, $valueCol1, $suffix1, $suffix2);
    $series = array();
    foreach ($t1['labels'] as $label) {
      $series[$label] = $t1[$label];
    }
    $series['Overall'] = $t1['full'];
    $t2 = false;
    if($valueCol2) {
      $t2 = t_test_paired_from_data($data, $identCol, $valueCol2, $suffix1, $suffix2);
    }

    $title = "{$suffix1}-{$suffix2} $valueCol1" .
      ($t2 ? " and $valueCol2" : '');
    if(isset($data['question']))
      $title = "{$data['question']['quiz']} #{$data['question']['displayOrder']}: $title";
    Layout::EmitTop($title, true);
    if(isset($data['question'])) 
      echo "
              <h2 class='question'>{$data['question']['header']}</h2>";
    if($t2) {
      echo "
          <div class='row'>
            <div class='col-sm-6'>";
      box_plot($series);
      t_test_paired_tables($t1, $valueCol1, $suffix1, $suffix2);
      echo "
            </div>
            <div class='col-sm-6'>";
      pre_post_line_chart($t2, $labelCol, $valueCol2, $suffix1, $suffix2);
      t_test_paired_tables($t2, $valueCol2, $suffix1, $suffix2);
      echo "
            </div>
          </div>";
    } else {
      echo "
          <div class='row'>
            <div class='col-sm-7'>";
      t_test_paired_tables($t1, $valueCol1, $suffix1, $suffix2);
      echo "
            </div>
            <div class='col-sm-5'>";
      box_plot($series);
      echo "
            </div>
          </div>";
    }
    data_table($data);

    Layout::EmitBottom();
  } catch (Exception $e) {
    Layout::RenderException($e);
  }
}