<?php
  function s3_upload($s3client, $bucket, $filename, $filepath) {
    $s3result = $s3client->putObject(array(
        'Bucket'                => $bucket,
        'Key'                   => $filename,
        'SourceFile'            => $filepath,
        'ServerSideEncryption'  => 'AES256'
    ));
    if($s3result['@metadata']['statusCode'] == 200) {
      $s3client->waitUntil('ObjectExists', array(
          'Bucket' => $bucket,
          'Key'    => $filename
      ));
      return $s3result['ObjectURL'];
    }
    return false;
  }
?>