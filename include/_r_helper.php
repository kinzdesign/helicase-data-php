<?php

  function cronbach_alpha($data) {

    $n = count($data);
    $k = count($data[0]);

    // build R command
    $cmd = "cronbach(matrix(c(";
    $isFirst = true;
    // build matrix
    for($i = 0; $i < $n; $i++) {
      if(count($data[$i]) != $k)
        throw new Exception("All sets passed to cronbach_alpha must have same number of items");
      for($j = 0; $j < $k; $j++) {
        if(!$isFirst) $cmd .= ',';
        $isFirst = false;
        $cmd .= $data[$i][$j];
      }
    }
    // end R command
    $cmd .= "), ncol=" . count($data[0]) . ", byrow=T))";

    return $cmd;
  }
