<?php
  function render_alerts($errors = false, $success = false) {
    if(is_array($success) && count($success)) {
      echo '
        <div class="alert alert-success">
          <ul>';
      foreach ($success as $suc) {
        echo "
            <li>$suc</li>";
      }
      echo '
          </ul>
        </div>';
      
    }

    if(is_array($errors) && count($errors)) {
      echo '
        <div class="alert alert-danger">
          <ul>';
      foreach ($errors as $err) {
        echo "
            <li>$err</li>";
      }
      echo '
          </ul>
        </div>';
    }
    
  }

?>