<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_box_plot.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_load_question.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_data_table.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_anova_one_way_tables.php');

function render_anova_one_way($data, $question, $identCol, $labelCol, $valueCol) {
  try
  {
    $data = load_question($question, $data);

    $labelTitle = ucwords($labelCol);
    $valueTitle = ucwords($valueCol);

    $title = "$valueTitle by $labelTitle";
    if(isset($data['question']))
      $title = "{$data['question']['quiz']} #{$data['question']['displayOrder']}: $title";
    Layout::EmitTop($title, true);
    if(isset($data['question'])) 
      echo "
            <h2 class='question'>{$data['question']['header']}</h2>";
    
    $results = anova_one_way_from_data($data, $labelCol, $valueCol);
    $series = array();
    foreach ($results['labels'] as $label) {
      $series[$label] = $results['data_summary'][$label];
    }
    $series['Overall'] = $results['total_summary'];

    if(count($results['labels']) > 3) {
      box_plot($series);
      anova_one_way_tables($results, $labelCol, $valueCol);
    } else {
        echo "
            <div class='row'>
              <div class='col-sm-7'>";
    
        anova_one_way_tables($results, $labelCol, $valueCol);
        echo "
              </div>
              <figure class='col-sm-5'>";
        box_plot($series);
        echo "
              </figure>
            </div>";
    }
    data_table($data);
    Layout::EmitBottom();
  } catch (Exception $e) {
    Layout::RenderException($e);
  }
}