<?php
function load_question($question, $data = false) {
  if(!$question) return $data;
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  $db = get_database();
  
  // get data
  $res = $db->query("
    SELECT  COALESCE(q.header, q.multiIntroHtml, q.likertIntroHtml) AS header, 
            q.displayOrder,
            q.multiIntroHtml, q.likertIntroHtml, 
            q.likertMinLabel, q.likertMaxLabel,
            z.title AS quiz
    FROM    questions AS q
            INNER JOIN quizzes AS z ON q.quiz = z.quiz
    WHERE   q.question = $question");
  check_for_db_error($res, $db);
  if($q = $res->fetch_assoc()) {
    $data = $data ?? array();
    $data['question'] = $q;
  }
  return $data;
}