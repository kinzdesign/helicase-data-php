<?php
  function get_aggregates($quizzes = false) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
    $db = get_database();

    $output = array();
    $where = "NOT IN ('CC','MI','CF','V-CF-X','V-CF-Y','V-CF-2')";
    if($quizzes) {
      $where = "IN ('" . 
        (is_array($quizzes) ? join($quizzes, "','") : $quizzes) .
        "')";
    }
    // get quiz data from DB
    $res = $db->query("
      SELECT  quiz, displayOrder AS quizDisplayOrder,
              CASE WHEN note IS NULL THEN title ELSE CONCAT(title, ' - ', note) END AS title
      FROM    quizzes
      WHERE   quiz $where
      ORDER   BY displayOrder");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      $output[$q['quiz']] = $q;
      $output[$q['quiz']]['questions'] = array();
    }

    // get question data from DB
    $res = $db->query("
      SELECT  quiz, question, displayOrder AS questionDisplayOrder,
              multiIntroHtml, likertIntroHtml,
              COALESCE(header, multiIntroHtml, likertIntroHtml) AS title
      FROM    questions
      WHERE   quiz $where
      ORDER   BY  quiz, displayOrder");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      $output[$q['quiz']]['questions'][$q['question']] = $q;
    }

    // get likert data from DB
    $res = $db->query("
      SELECT  q.quiz, q.question,
              CONCAT(h.pathway, '-', x.experience) AS cohort,
              ROUND(AVG(r.likert), 1) AS likertAvg,
              -- COUNT(likert) AS likertN,
              SUM(CASE WHEN r.likert = 0 THEN 1 ELSE 0 END) AS likertN0,
              SUM(CASE WHEN r.likert = 1 THEN 1 ELSE 0 END) AS likertN1,
              SUM(CASE WHEN r.likert = 2 THEN 1 ELSE 0 END) AS likertN2,
              SUM(CASE WHEN r.likert = 3 THEN 1 ELSE 0 END) AS likertN3,
              SUM(CASE WHEN r.likert = 4 THEN 1 ELSE 0 END) AS likertN4,
              SUM(CASE WHEN r.likert = 5 THEN 1 ELSE 0 END) AS likertN5-- ,
              -- ROUND(MIN(r.submittedLikert - r.sentLikert), 1) AS timeMin,
              -- ROUND(MAX(r.submittedLikert - r.sentLikert), 1) AS timeMax,
              -- ROUND(AVG(r.submittedLikert - r.sentLikert), 1) AS timeAvg,
              -- ROUND(STD(r.submittedLikert - r.sentLikert), 1) AS timeStd
      FROM    pathways AS h
              CROSS JOIN experiences AS x
              CROSS JOIN questions AS q
              CROSS JOIN vw_participants AS p ON 
                      p.isValid = 1
                      AND p.pathway = h.pathway
                      AND p.experience = x.experience
              LEFT  JOIN responses AS r ON q.question = r.question AND p.participant = r.participant
      WHERE   ((q.questionFormat & 1) > 0) AND 
              q.quiz $where
      GROUP   BY q.quiz, q.question, h.pathway, x.experience");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      foreach ($q as $k => $v) {
        if(!isset($output[$q['quiz']]['questions'][$q['question']]['likert']))
            $output[$q['quiz']]['questions'][$q['question']]['likert'] = array();
        if($k != 'quiz' && $k != 'question' && $k != 'cohort') {
          if(!isset($output[$q['quiz']]['questions'][$q['question']]['likert'][$k]))
            $output[$q['quiz']]['questions'][$q['question']]['likert'][$k] = array();
          $output[$q['quiz']]['questions'][$q['question']]['likert'][$k][$q['cohort']] = $v;
        }
      }
    }

    // get multi data from DB
    $res = $db->query("
      SELECT  q.quiz, q.question, a.answer, a.answerText,
              CONCAT(h.pathway, '-', x.experience) AS cohort,
              COUNT(r.answer) AS answerN, a.score,
              CONCAT(a.answerText, ' (', a.score, ')') AS title
              -- ,
              -- ROUND(MIN(r.score), 1) AS scoreMin,
              -- ROUND(MAX(r.score), 1) AS scoreMax,
              -- ROUND(AVG(r.score), 1) AS scoreAvg,
              -- ROUND(STD(r.score), 1) AS scoreStd,
              -- SUM(CASE WHEN r.timeBonus = 15 THEN 1 ELSE 0 END) AS timeBonusN15,
              -- SUM(CASE WHEN r.timeBonus = 10 THEN 1 ELSE 0 END) AS timeBonusN10,
              -- SUM(CASE WHEN r.timeBonus =  5 THEN 1 ELSE 0 END) AS timeBonusN5,
              -- ROUND(MIN(r.elapsed), 1) AS timeMin,
              -- ROUND(MAX(r.elapsed), 1) AS timeMax,
              -- ROUND(AVG(r.elapsed), 1) AS timeAvg,
              -- ROUND(STD(r.elapsed), 1) AS timeStd
      FROM    pathways AS h
              CROSS JOIN experiences AS x
              CROSS JOIN vw_participants AS p
              CROSS JOIN answers AS a
              INNER JOIN questions AS q ON a.question = q.question
              LEFT  JOIN (
                SELECT  participant, question, answer, score, timeBonus,
                        submittedMulti - sentMulti AS elapsed
                FROM    responses
                WHERE   answer IS NOT NULL
                UNION
                SELECT  participant, question, answer, NULL, NULL, NULL
                FROM    responses_multi
              ) AS r ON r.answer = a.answer AND r.participant = p.participant
      WHERE   ((q.questionFormat & 2) > 0) AND 
              h.pathway = 'CH' AND 
              p.pathway = h.pathway AND 
              p.experience = x.experience AND
              p.isValid = 1 AND
              q.quiz $where
      GROUP   BY q.quiz, q.question, h.pathway, x.experience, a.score DESC, a.displayOrder, a.answer");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      if(!isset($output[$q['quiz']]['questions'][$q['question']]['multi']))
        $output[$q['quiz']]['questions'][$q['question']]['multi'] = array();
      foreach ($q as $k => $v) {
        // copy cohort-specific aggregates
        if($k != 'quiz' && $k != 'question' && $k != 'answer' && $k != 'cohort') {
          if(!isset($output[$q['quiz']]['questions'][$q['question']]['multi'][$q['cohort']][$q['answer']]))
            $output[$q['quiz']]['questions'][$q['question']]['multi'][$q['cohort']][$q['answer']] = array();
          $output[$q['quiz']]['questions'][$q['question']]['multi'][$q['cohort']][$q['answer']][$k] = $v;
        }
      }
    }

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }
?>