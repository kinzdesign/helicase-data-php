<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('GLAC Data', true);
?>
      <h2>Summary Tables and Charts</h2>
      <ul>
        <li><a href="charts.php">View Question Charts</a></li>
        <li><a href="tables.php">View Tables</a></li>
        <li><a href="regression.php">View GLAC Score Multivariate Regression</a></li>
      </ul>

      <!-- <h2>Detailed Information</h2> -->
<?php
  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}