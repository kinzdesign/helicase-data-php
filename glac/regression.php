<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('GLAC Regression', true);
  $script = basename(__FILE__, '.php');

  // get data
  require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
  $db = get_database();
  $res = $db->query("
        SELECT  participant, cohort,
                fcgtCorrect,
                age, sex, pgenEducation AS education, pgenRace AS race, 
                CASE WHEN pgenHispanic = 1 THEN 'Y' ELSE 'N' END AS hispanic
        FROM    vw_participants
        WHERE   isValid = 1");
  check_for_db_error($res, $db);

  // write CSV
  $csv = fopen("$script.r.csv", "w");
  // write headers
  fwrite($csv, "participant,cohort,fcgtCorrect,age,sex,education,race,hispanic\n");
  // iterate rows, write data to file
  while($q = $res->fetch_assoc()) {
    fwrite($csv, 
          $q['participant'] . ',' .
          $q['cohort'] . ',' .
          $q['fcgtCorrect'] . ',' .
          $q['age'] . ',' .
          $q['sex'] . ',' .
          $q['education'] . ',' .
          $q['race'] . ',' .
          $q['hispanic'] . "\n");
  }
  // close file
  fclose($csv);

  // free up database
  if(isset($res) && is_object($res))
    $res->free();
  $db->close();

  // write R script
  file_put_contents($script . '.r', 
"### Computes Multivariate linear regression for GLAC data

# load CSV data
fcgt <- read.table(\"$script.r.csv\", header=T, sep=\",\")
#display basic stats
summary(fcgt)
# compute regression for pre knowledge
fcgt.lm <- lm(fcgtCorrect~cohort + age + sex + education + race + hispanic, data= fcgt)
summary(fcgt.lm)");

  // run script
  exec("Rscript $script.r > $script.r.txt");

  // download links
  echo "
    <ul>
      <li><a href='$script.r'>Download R script</a></li>
      <li><a href='$script.r.txt'>Download R output</a></li>
      <li><a href='$script.r.csv'>Download CSV data</a></li>
    </ul>";

  // emit data
  echo '<pre>';
  readfile("$script.r.txt");
  echo '</pre>';

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}