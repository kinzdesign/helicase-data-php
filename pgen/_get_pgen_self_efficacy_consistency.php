<?php
  function get_pgen_self_efficacy_consistency() {
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_r_helper.php');
    $db = get_database();

    $preQuestions  = [ 20, 21, 22, 23, 24 ];
    $postQuestions = [ 34, 35, 36, 37, 38 ];
    $output = array();
    $output['pre'] = get_self_efficacy($db, $preQuestions);
    $output['post'] = get_self_efficacy($db, $preQuestions);

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }

  function get_self_efficacy($db, $questions) {
    $output = array(
      'Total' => array(),
      'CH-G'  => array(),
      'CH-C'  => array()
    );

    // get quiz data from DB
    $res = $db->query("
          SELECT  p.participant AS Participant,
                  p.cohort      AS Cohort,
                  r1.likert     AS Likert1,
                  r2.likert     AS Likert2,
                  r3.likert     AS Likert3,
                  r4.likert     AS Likert4,
                  r5.likert     AS Likert5
          FROM    vw_participants AS p
                  LEFT JOIN responses AS r1 ON p.participant = r1.participant AND r1.question = {$questions[0]}
                  LEFT JOIN responses AS r2 ON p.participant = r2.participant AND r2.question = {$questions[1]}
                  LEFT JOIN responses AS r3 ON p.participant = r3.participant AND r3.question = {$questions[2]}
                  LEFT JOIN responses AS r4 ON p.participant = r4.participant AND r4.question = {$questions[3]}
                  LEFT JOIN responses AS r5 ON p.participant = r5.participant AND r5.question = {$questions[4]}
          WHERE   p.isValid = 1");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      $set = array(
        $q['Likert1'],
        $q['Likert2'],
        $q['Likert3'],
        $q['Likert4'],
        $q['Likert5']
      );
      $output[$q['Cohort']][] = $set;
      $output['Total'][] = $set;
    }

    return $output;
  }
?>