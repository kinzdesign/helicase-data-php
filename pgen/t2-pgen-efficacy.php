<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_two_sample.php');

  $db = get_database();
  
  $quiz         = Layout::GetQueryStringInt('quiz');
  $suffix       = $quiz == 1 ? 'Pre' : 'Post';
  $identCol     = 'Participant';
  $labelCol     = 'Cohort';
  $valueCol     = "SelfEfficacy$suffix";

  // get data
  $res = $db->query("
        SELECT  participant AS $identCol, 
                cohort AS $labelCol, 
                SUM(likert) AS $valueCol
        FROM    (
            SELECT  p.participant, 
                    p.cohort,
                    r.likert
            FROM    vw_participants AS p
                    INNER JOIN responses AS r ON p.participant = r.participant
                    INNER JOIN questions AS q ON r.question = q.question
            WHERE   p.isValid = 1
                    AND q.quiz = 'PGEN$quiz'
                    AND q.displayOrder > 9
            ORDER   BY cohort, p.participant
        ) AS a
        GROUP BY participant, cohort");
  check_for_db_error($res, $db);

  render_t_test_two_sample($res, false, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}