<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/pgen/_get_pgen_table_self_efficacy.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/pgen/_render_pgen_table_self_efficacy.php');

  $data = get_pgen_table_self_efficacy();

  Layout::$EmitContainer = false;
  Layout::EmitTop('PGen Genetic Self-Efficacy Tables', true);

  render_pgen_table_self_efficacy($data);

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}