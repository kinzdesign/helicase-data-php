<?php
  function get_pgen_table_self_efficacy() {
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_stats.php');
    $db = get_database();

    $output = array();

    // get likert data from DB
    $res = $db->query("
          SELECT  CONCAT(CASE WHEN q1.displayOrder < 10 THEN ' ' ELSE '' END, q1.displayOrder, ': ', q1.likertIntroHtml) AS Question, 
                  p.participant AS participant, p.cohort AS Cohort,
                  r1.likert AS LikertPre,
                  r2.likert AS LikertPost
          FROM    vw_participants AS p
                  INNER JOIN responses AS r1 ON p.participant = r1.participant
                  INNER JOIN responses AS r2 ON p.participant = r2.participant
                  INNER JOIN questions AS q1 ON r1.question = q1.question AND q1.quiz = 'PGEN1'
                  INNER JOIN questions AS q2 ON r2.question = q2.question AND q2.quiz = 'PGEN2' AND q1.displayOrder = q2.displayOrder
          WHERE   p.isValid = 1
                  AND ((q1.questionFormat & 1) > 0) -- showLikert
                  AND r1.likert > 0 AND r2.likert > 0
          ORDER   BY q1.displayOrder, p.cohort DESC, p.participant");
    check_for_db_error($res, $db);
    
    $tmp = array();
    while($q = $res->fetch_assoc()) {
      // ensure buckets exist
      if(!isset($tmp[$q['Question']]))
        $tmp[$q['Question']] = array();
      $cohort = $q['Cohort'] ?? 'Total';
      if(!isset($tmp[$q['Question']][$cohort]))
        $tmp[$q['Question']][$cohort] = array(
        'N'    => 0,
        'Pre'  => array(),
        'Post' => array()
      );
      if(!isset($tmp[$q['Question']]['Total']))
        $tmp[$q['Question']]['Total'] = array(
        'N'    => 0,
        'Pre'  => array(),
        'Post' => array()
      );
      // update cohort bucket
      $tmp[$q['Question']][$cohort]['N']++;
      $tmp[$q['Question']][$cohort]['Pre'][]  = $q['LikertPre'];
      $tmp[$q['Question']][$cohort]['Post'][] = $q['LikertPost'];
      // update overall bucket
      $tmp[$q['Question']]['Total']['N']++;
      $tmp[$q['Question']]['Total']['Pre'][]  = $q['LikertPre'];
      $tmp[$q['Question']]['Total']['Post'][] = $q['LikertPost'];
    }
    // get aggregate data from DB
    $res = $db->query("
          SELECT  CONCAT(CASE WHEN q1.displayOrder < 10 THEN ' ' ELSE '' END, q1.displayOrder, ': ', q1.likertIntroHtml) AS Question, p.cohort AS Cohort,
                  AVG(r1.likert) AS LikertPreAvg,
                  STDDEV(r1.likert) AS LikertPreStdDev,
                  AVG(r2.likert) AS LikertPostAvg,
                  STDDEV(r2.likert) AS LikertPostStdDev,
                  SUM(CASE WHEN r1.likert > 3 THEN 1 ELSE 0 END) AS AgreePre,
                  SUM(CASE WHEN r2.likert > 3 THEN 1 ELSE 0 END) AS AgreePost,
                  SUM(CASE WHEN r1.likert > 3 AND r2.likert <= 3 THEN 1 ELSE 0 END) AS AgreeDisagree,
                  SUM(CASE WHEN r1.likert <= 3 AND r2.likert > 3 THEN 1 ELSE 0 END) AS DisagreeAgree,
                  SUM(CASE WHEN r1.likert > r2.likert THEN 1 ELSE 0 END) AS Increase,
                  SUM(CASE WHEN r1.likert < r2.likert THEN 1 ELSE 0 END) AS Decrease,
                  COUNT(*) AS N
          FROM    vw_participants AS p
                  INNER JOIN responses AS r1 ON p.participant = r1.participant
                  INNER JOIN responses AS r2 ON p.participant = r2.participant
                  INNER JOIN questions AS q1 ON r1.question = q1.question AND q1.quiz = 'PGEN1'
                  INNER JOIN questions AS q2 ON r2.question = q2.question AND q2.quiz = 'PGEN2' AND q1.displayOrder = q2.displayOrder
          WHERE   p.isValid = 1
                  AND ((q1.questionFormat & 1) > 0) -- showLikert
                  AND r1.likert > 0 AND r2.likert > 0
          GROUP   BY question, p.cohort WITH ROLLUP");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      if($q['Question']) {
        // ensure buckets exist
        if(!isset($output[$q['Question']]))
          $output[$q['Question']] = array();
        $cohort = $q['Cohort'] ?? 'Total';
        if(!isset($output[$q['Question']][$cohort]))
          $output[$q['Question']][$cohort] = array();
        $n = $q['N'];
        $output[$q['Question']][$cohort]['N'] = $n;
        $output[$q['Question']][$cohort]['LikertPreAvg'] = $q['LikertPreAvg'];
        $output[$q['Question']][$cohort]['LikertPreStdDev'] = $q['LikertPreStdDev'];
        $output[$q['Question']][$cohort]['LikertPostAvg'] = $q['LikertPostAvg'];
        $output[$q['Question']][$cohort]['LikertPostStdDev'] = $q['LikertPostStdDev'];
        $output[$q['Question']][$cohort]['AgreePre'] = $q['AgreePre'];
        $output[$q['Question']][$cohort]['AgreePrePct'] = $q['AgreePre'] / $n * 100;
        $output[$q['Question']][$cohort]['AgreePost'] = $q['AgreePost'];
        $output[$q['Question']][$cohort]['AgreePostPct'] = $q['AgreePost'] / $n * 100;
        $output[$q['Question']][$cohort]['Increase'] = $q['Increase'];
        $output[$q['Question']][$cohort]['IncreasePct'] = $q['Increase'] / $n * 100;
        $output[$q['Question']][$cohort]['Decrease'] = $q['Decrease'];
        $output[$q['Question']][$cohort]['DecreasePct'] = $q['Decrease'] / $n * 100;
        // calculate P value using McNemar's exact test
        $mcnemar = mcnemar_exact($q['AgreeDisagree'], $q['DisagreeAgree']);
        if($mcnemar)
          $output[$q['Question']][$cohort]['pAgree'] = $mcnemar['p'];
        // calculate paired t-test for pre/post Likert
        if(isset($tmp[$q['Question']][$cohort])) {
          $tTest = t_test_paired(
            $tmp[$q['Question']][$cohort]['Pre'],
            $tmp[$q['Question']][$cohort]['Post'],
            'Pre', 'Post');
          $output[$q['Question']][$cohort]['pRating'] = $tTest['t.paired']['p2'];
        }
      }
    }

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }
?>