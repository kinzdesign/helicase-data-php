<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('PGen Data', true);
?>
      <h2>Summary Tables and Charts</h2>
      <ul>
        <li><a href="charts.php">View Question Charts</a></li>
        <li><a href="knowledge.php">View Genetics Knowledge Tables</a></li>
        <li><a href="self-efficacy.php">View Genetics Self-Efficacy Tables</a></li>
        <li><a href="pgen1-knowledge.php">View Initial Genetics Knowledge Tables</a></li>
        <li><a href="pgen2-knowledge.php">View Follow-up Genetics Knowledge Tables</a></li>
        <li><a href="pgen1-efficacy.php">View Initial Genetics Self-Efficacy Tables</a></li>
        <li><a href="pgen2-efficacy.php">View Follow-up Genetics Self-Efficacy Tables</a></li>
        <li><a href="regressions.php">PGen Multivariate Linear Regression</a></li>
      </ul>

      <h2>Detailed Information</h2>
      <div class='table-responsive'><table class="table table-striped">
        <thead>
          <tr>
            <td>
            </td>
            <th class="text-center">Paired t-Test</th>
            <th class="text-center">Two-Sample t-Test</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <th>Overall: Genetics Knowledge (# Correct, Questions 1-9)</th>
            <td>
              <a href="tp-pgen-knowledge.php">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-knowledge.php?quiz=1">Pre by Cohort</a><br/>
              <a href="t2-pgen-knowledge.php?quiz=2">Post by Cohort</a><br/>
              <a href="t2-pgen-knowledge-delta.php">Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>Overall: Genetics Self-Efficacy (Likert, Questions 10-14)</th>
            <td>
              <a href="tp-pgen-efficacy.php">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-efficacy.php?quiz=1">Pre by Cohort</a><br/>
              <a href="t2-pgen-efficacy.php?quiz=2">Post by Cohort</a><br/>
              <a href="t2-pgen-efficacy-delta.php">Change by Cohort</a>
            </td>
          </tr>


          <tr>
            <th>1: Healthy parents can have a child with an inherited disease.</th>
            <td>
              <a href="tp-pgen.php?pre=11&amp;post=25">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=11&amp;post=25">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>2: If your close relatives have diabetes or heart disease, you are more likely to develop these conditions.</th>
            <td>
              <a href="tp-pgen.php?pre=12&amp;post=26">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=12&amp;post=26">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>3: A disease is only genetically determined if more than one family member is affected.</th>
            <td>
              <a href="tp-pgen.php?pre=13&amp;post=27">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=13&amp;post=27">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>4: Some genetic disorders occur more often within particular ethnic groups.</th>
            <td>
              <a href="tp-pgen.php?pre=14&amp;post=28">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=14&amp;post=28">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>5: The environment has little or no effect on how genes contribute to disease.</th>
            <td>
              <a href="tp-pgen.php?pre=15&amp;post=29">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=15&amp;post=29">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>6: Some genetic disorders occur later in adult life.</th>
            <td>
              <a href="tp-pgen.php?pre=16&amp;post=30">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=16&amp;post=30">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>7: Most genetic disorders are caused by only a single gene.</th>
            <td>
              <a href="tp-pgen.php?pre=17&amp;post=31">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=17&amp;post=31">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>8: Once a genetic marker for a disorder is identified in a person, the disorder can usually be prevented or cured.</th>
            <td>
              <a href="tp-pgen.php?pre=18&amp;post=32">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=18&amp;post=32">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>9: A healthy lifestyle can prevent or lessen the negative consequences of having genetic predispositions to some diseases.</th>
            <td>
              <a href="tp-pgen.php?pre=19&amp;post=33">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=19&amp;post=33">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>10: I am confident in my ability to understand information about genetics.</th>
            <td>
              <a href="tp-pgen.php?pre=20&amp;post=34">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=20&amp;post=34">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>11: I am able to understand information about how genes can affect my health.</th>
            <td>
              <a href="tp-pgen.php?pre=21&amp;post=35">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=21&amp;post=35">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>12: I have a good idea about how genetics may influence risk for disease generally.</th>
            <td>
              <a href="tp-pgen.php?pre=22&amp;post=36">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=22&amp;post=36">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>13: I have a good idea about how my own genetic makeup might affect my risk for disease.</th>
            <td>
              <a href="tp-pgen.php?pre=23&amp;post=37">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=23&amp;post=37">Likert Change by Cohort</a>
            </td>
          </tr>
          <tr>
            <th>14: I am able to explain to others how genes affect one&rsquo;s health.</th>
            <td>
              <a href="tp-pgen.php?pre=24&amp;post=38">Pre vs. Post</a>
            </td>
            <td>
              <a href="t2-pgen-change.php?pre=24&amp;post=38">Likert Change by Cohort</a>
            </td>
          </tr>
        </tbody>
      </table></div>
<?php
  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}