<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('PGen Regressions', true);
  $script = basename(__FILE__, '.php');

  // get data
  require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
  $db = get_database();
  $res = $db->query("
        SELECT  participant, cohort,
                pgen1Knowledge, pgen1SelfEfficacy,
                pgen2Knowledge, pgen2SelfEfficacy,
                age, sex, pgenEducation AS education, pgenRace AS race, 
                CASE WHEN pgenHispanic = 1 THEN 'Y' ELSE 'N' END AS hispanic,
                fcgtCorrect AS glacCorrect
        FROM    vw_participants
        WHERE   isValid = 1");
  check_for_db_error($res, $db);

  // write CSV
  $csv = fopen("$script.r.csv", "w");
  // write headers
  fwrite($csv, "participant,cohort,pgen1Knowledge,pgen1SelfEfficacy,pgen2Knowledge,pgen2SelfEfficacy,age,sex,education,race,hispanic,glacCorrect\n");
  // iterate rows, write data to file
  while($q = $res->fetch_assoc()) {
    fwrite($csv, 
          $q['participant'] . ',' .
          $q['cohort'] . ',' .
          $q['pgen1Knowledge'] . ',' .
          $q['pgen1SelfEfficacy'] . ',' .
          $q['pgen2Knowledge'] . ',' .
          $q['pgen2SelfEfficacy'] . ',' .
          $q['age'] . ',' .
          $q['sex'] . ',' .
          $q['education'] . ',' .
          $q['race'] . ',' .
          $q['hispanic'] . ',' .
          $q['glacCorrect'] . "\n");
  }
  // close file
  fclose($csv);

  // free up database
  if(isset($res) && is_object($res))
    $res->free();
  $db->close();

  // write R script
  file_put_contents($script . '.r', 
"### Computes Multivariate linear regression for PGen data

# download CSV data
pgen <- read.table(\"$script.r.csv\", header=T, sep=\",\")

## GENETICS KNOWLEDGE

# compute regression for pre knowledge
pgen.knowledge1lm <- lm(pgen1Knowledge~cohort + age + sex + education + race + hispanic + glacCorrect, data= pgen)
summary(pgen.knowledge1lm)

# compute regression for post knowledge
pgen.knowledge2lm <- lm(pgen2Knowledge~cohort + age + sex + education + race + hispanic + glacCorrect + pgen1Knowledge + pgen1SelfEfficacy, data= pgen)
summary(pgen.knowledge2lm)

## GENETIC SELF-EFFICACY

# compute regression for pre self-efficacy
pgen.efficacy1lm <- lm(pgen1SelfEfficacy~cohort + age + sex + education + race + hispanic + glacCorrect, data= pgen)
summary(pgen.efficacy1lm)

# compute regression for post self-efficacy
pgen.efficacy2lm <- lm(pgen2SelfEfficacy~cohort + age + sex + education + race + hispanic + glacCorrect + pgen1Knowledge + pgen1SelfEfficacy, data= pgen)
summary(pgen.efficacy2lm)");

  // run script
  exec("Rscript $script.r > $script.r.txt");

  // download links
  echo "
    <ul>
      <li><a href='$script.r'>Download R script</a></li>
      <li><a href='$script.r.txt'>Download R output</a></li>
      <li><a href='$script.r.csv'>Download CSV data</a></li>
    </ul>";

  // emit data
  echo '<pre>';
  readfile("$script.r.txt");
  echo '</pre>';

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}