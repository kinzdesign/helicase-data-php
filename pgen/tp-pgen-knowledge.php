<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_paired.php');

  $db = get_database();
  
  $identCol     = 'Participant';
  $labelCol     = 'Cohort';
  $valueCol1    = 'Knowledge';
  $suffix1      = 'Pre';
  $suffix2      = 'Post';

  // get data
  $res = $db->query("
    SELECT  participant AS $identCol, 
            cohort AS $labelCol, 
            SUM(correct1) AS {$valueCol1}{$suffix1},
            SUM(correct2) AS {$valueCol1}{$suffix2}
    FROM    (
        SELECT  p.participant, 
                p.cohort,
                r1.likert AS likert1,
                r2.likert AS likert2,
                CASE WHEN r1.score IS NULL THEN NULL WHEN r1.score > 0 THEN 1 ELSE 0 END AS correct1,
                CASE WHEN r2.score IS NULL THEN NULL WHEN r2.score > 0 THEN 1 ELSE 0 END AS correct2
        FROM    vw_participants AS p
                INNER JOIN responses AS r1 ON p.participant = r1.participant
                INNER JOIN questions AS q1 ON r1.question = q1.question
                INNER JOIN responses AS r2 ON p.participant = r2.participant
                INNER JOIN questions AS q2 ON r2.question = q2.question
        WHERE   p.isValid = 1
                AND q1.quiz = 'PGEN1'
                AND q2.quiz = 'PGEN2'
                AND q1.displayOrder = q2.displayOrder
                AND q1.displayOrder < 10
        ORDER   BY cohort, p.participant
    ) AS a
    GROUP BY participant, cohort");
  $data = array('rows' => get_rows_as_array($res, $db));

  render_t_test_paired($data, false, $identCol, $labelCol, $valueCol1, false, $suffix1, $suffix2);
} catch (Exception $e) {
  Layout::RenderException($e);
}