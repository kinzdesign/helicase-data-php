<?php
  function get_pgen_knowledge_consistency() {
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_r_helper.php');
    $db = get_database();

    $preQuestions  = [ 11, 12, 13, 14, 15, 16, 17, 18, 19 ];
    $postQuestions = [ 25, 26, 27, 28, 29, 30, 31, 32, 33 ];
    $output = array();
    $output['pre'] = get_knowledge($db, $preQuestions);
    $output['post'] = get_knowledge($db, $preQuestions);

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }

  function get_knowledge($db, $questions) {
    $output = array(
      'Total' => array(),
      'CH-G'  => array(),
      'CH-C'  => array()
    );

    // get quiz data from DB
    $res = $db->query("
          SELECT  p.participant AS Participant,
                  p.cohort      AS Cohort,
                  r1.likert     AS Likert1,
                  r2.likert     AS Likert2,
                  r3.likert     AS Likert3,
                  r4.likert     AS Likert4,
                  r4.likert     AS Likert5,
                  r4.likert     AS Likert6,
                  r4.likert     AS Likert7,
                  r4.likert     AS Likert8,
                  r5.likert     AS Likert9
          FROM    vw_participants AS p
                  LEFT JOIN responses AS r1 ON p.participant = r1.participant AND r1.question = {$questions[0]}
                  LEFT JOIN responses AS r2 ON p.participant = r2.participant AND r2.question = {$questions[1]}
                  LEFT JOIN responses AS r3 ON p.participant = r3.participant AND r3.question = {$questions[2]}
                  LEFT JOIN responses AS r4 ON p.participant = r4.participant AND r4.question = {$questions[3]}
                  LEFT JOIN responses AS r5 ON p.participant = r5.participant AND r5.question = {$questions[4]}
                  LEFT JOIN responses AS r6 ON p.participant = r6.participant AND r6.question = {$questions[5]}
                  LEFT JOIN responses AS r7 ON p.participant = r7.participant AND r7.question = {$questions[6]}
                  LEFT JOIN responses AS r8 ON p.participant = r8.participant AND r8.question = {$questions[7]}
                  LEFT JOIN responses AS r9 ON p.participant = r9.participant AND r9.question = {$questions[8]}
          WHERE   p.isValid = 1");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      $set = array(
        $q['Likert1'],
        $q['Likert2'],
        $q['Likert3'],
        $q['Likert4'],
        $q['Likert5']
      );
      $output[$q['Cohort']][] = $set;
      $output['Total'][] = $set;
    }

    return $output;
  }
?>