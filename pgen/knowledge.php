<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/pgen/_get_pgen_table_knowledge.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/pgen/_render_pgen_table_knowledge.php');

  $data = get_pgen_table_knowledge();

  Layout::$EmitContainer = false;
  Layout::EmitTop('PGen Genetics Knowledge Tables', true);

  render_pgen_table_knowledge($data);

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}