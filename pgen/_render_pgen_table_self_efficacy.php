<?php
  function render_pgen_table_self_efficacy($data) {
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_sig_cell.php');
    echo "
      <h2>Genetics Self-Efficacy Among All Participants</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-all' rowspan='2'>Question</th>
            <th class='heavy-top heavy-right thin-bottom heavy-left' colspan='3'>Rating,<sup>a</sup> mean (SD)</th>
            <th class='heavy-top heavy-right thin-bottom heavy-left' colspan='3'>Agree or strongly agree (%)</th>
            <th class='heavy-all' rowspan='2'>Changed response at post, <em>n</em>, (%) increase/decrease</th>
          </tr>
          <tr>
            <th class='thin-top thin-right heavy-bottom heavy-left'>Pre</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Post</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom heavy-left'>Pre</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Post</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'><em>P</em> value**</th>
          </tr>
        </thead>
        <tbody>";
    foreach ($data as $question => $stats) {
      echo "
          <tr>
            <th class='thin-top heavy-right thin-bottom heavy-left'>{$question}</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>{$stats['Total']['LikertPreAvg']} (".format_number($stats['Total']['LikertPreStdDev']).")</td>
            <td class='thin-top thin-right thin-bottom thin-left'>{$stats['Total']['LikertPostAvg']} (".format_number($stats['Total']['LikertPostStdDev']).")</td>
            ".sig_cell($stats['Total']['pRating'], 'thin-top heavy-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom heavy-left'>{$stats['Total']['AgreePre']} (".format_number($stats['Total']['AgreePrePct'])."%)</td>
            <td class='thin-top thin-right thin-bottom thin-left'>{$stats['Total']['AgreePost']} (".format_number($stats['Total']['AgreePostPct'])."%)</td>
            ".sig_cell($stats['Total']['pAgree'], 'thin-top heavy-right thin-bottom thin-left')."
            <td class='thin-top heavy-right thin-bottom heavy-left'>
              {$stats['Total']['Increase']} (".format_number($stats['Total']['IncreasePct'])."%) /
              {$stats['Total']['Decrease']} (".format_number($stats['Total']['DecreasePct'])."%)
            </td>
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td colspan='15' class='heavy-all'>
              <sup>a</sup>Likert rating scale from 1 (strongly disagree) to 5 (strongly agree).
              *<em>P</em> value obtained from paired <em>t</em>-tests.
              **<em>P</em> value obtained from McNemar exact tests.
            </td>
          </tr>
        </tfoot>
      </table></div>


      <h2>Genetics Self-Efficacy by Cohort</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-all' rowspan='3'>Question</th>
            <th class='heavy-top heavy-right mid-bottom heavy-left' colspan='6'>Rating,<sup>a</sup> mean (SD)</th>
            <th class='heavy-top heavy-right mid-bottom heavy-left' colspan='6'>Agree or strongly agree (%)</th>
            <th class='heavy-top heavy-right thin-bottom heavy-left' colspan='2' rowspan='2'>Changed response at post, <em>n</em>, (%) increase/decrease</th>
          </tr>
          <tr>
            <th class='mid-top mid-right thin-bottom heavy-left' colspan='3'>Gamified</th>
            <th class='mid-top heavy-right thin-bottom mid-left' colspan='3'>Control</th>
            <th class='mid-top mid-right thin-bottom heavy-left' colspan='3'>Gamified</th>
            <th class='mid-top heavy-right thin-bottom mid-left' colspan='3'>Control</th>
          </tr>
          <tr>
            <th class='thin-top thin-right heavy-bottom heavy-left'>Pre</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Post</th>
            <th class='thin-top mid-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom mid-left'>Pre</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Post</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Pre</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Post</th>
            <th class='thin-top mid-right heavy-bottom thin-left'><em>P</em> value**</th>
            <th class='thin-top thin-right heavy-bottom mid-left'>Pre</th>
            <th class='thin-top thin-right heavy-bottom thin-left'>Post</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'><em>P</em> value**</th>
            <th class='thin-top thin-right heavy-bottom heavy-left'>Gamified</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'>Control</th>
          </tr>
        </thead>
        <tbody>";
    foreach ($data as $question => $stats) {
      echo "
          <tr>
            <th class='thin-top heavy-right thin-bottom heavy-left'>{$question}</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>{$stats['CH-G']['LikertPreAvg']} (".format_number($stats['CH-G']['LikertPreStdDev']).")</td>
            <td class='thin-top thin-right thin-bottom thin-left'>{$stats['CH-G']['LikertPostAvg']} (".format_number($stats['CH-G']['LikertPostStdDev']).")</td>
            ".sig_cell($stats['CH-G']['pRating'], 'thin-top mid-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom mid-left'>{$stats['CH-C']['LikertPreAvg']} (".format_number($stats['CH-C']['LikertPreStdDev']).")</td>
            <td class='thin-top thin-right thin-bottom thin-left'>{$stats['CH-C']['LikertPostAvg']} (".format_number($stats['CH-C']['LikertPostStdDev']).")</td>
            ".sig_cell($stats['CH-C']['pRating'], 'thin-top heavy-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom heavy-left'>{$stats['CH-G']['AgreePre']} (".format_number($stats['CH-G']['AgreePrePct'])."%)</td>
            <td class='thin-top thin-right thin-bottom thin-left'>{$stats['CH-G']['AgreePost']} (".format_number($stats['CH-G']['AgreePostPct'])."%)</td>
            ".sig_cell($stats['CH-G']['pAgree'], 'thin-top mid-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom mid-left'>{$stats['CH-C']['AgreePre']} (".format_number($stats['CH-C']['AgreePrePct'])."%)</td>
            <td class='thin-top thin-right thin-bottom thin-left'>{$stats['CH-C']['AgreePost']} (".format_number($stats['CH-C']['AgreePostPct'])."%)</td>
            ".sig_cell($stats['CH-C']['pAgree'], 'thin-top heavy-right thin-bottom thin-left')."
            <td class='thin-top thin-right thin-bottom heavy-left'>
              {$stats['CH-G']['Increase']} (".format_number($stats['CH-G']['IncreasePct'])."%) /
              {$stats['CH-G']['Decrease']} (".format_number($stats['CH-G']['DecreasePct'])."%)
            </td>
            <td class='thin-top heavy-right thin-bottom thin-left'>
              {$stats['CH-C']['Increase']} (".format_number($stats['CH-C']['IncreasePct'])."%) /
              {$stats['CH-C']['Decrease']} (".format_number($stats['CH-C']['DecreasePct'])."%)
            </td>
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td colspan='15' class='heavy-all'>
              <sup>a</sup>Likert rating scale from 1 (strongly disagree) to 5 (strongly agree).
              *<em>P</em> value obtained from paired <em>t</em>-tests.
              **<em>P</em> value obtained from McNemar exact tests.
            </td>
          </tr>
        </tfoot>
      </table></div>";

  }
?>