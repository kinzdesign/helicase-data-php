<?php
  function print_pgen_question_data($question_id, $question_data, $script = '', $expanded = true) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/pgen/_print_pgen_scatter.php');
    echo "
              <div class='panel panel-danger'>
                <div class='panel-heading' role='tab' id='h-q-$question_id'>
                  <h4 class='panel-title'>
                    <a role='button' data-toggle='collapse' data-parent='#a-pgen' href='#c-q-$question_id' aria-controls='c-q-$question_id'" . 
                    ($expanded ? " aria-expanded='true'" : '') .
                    ">
                      {$question_data['intro']}
                    </a>
                  </h4>
                </div>
                <div id='c-q-$question_id' class='panel-collapse collapse" . 
                    ($expanded ? ' in' : '') .
                    "' role='tabpanel' aria-labelledby='h-q-$question_id'>
                  <div class='panel-body'>
                    <div class='row'>";
    foreach ($question_data['cohorts'] as $cohort => $scores) {
      $script = print_pgen_scatter($question_id, $cohort, $scores, $script);
    }
    echo '
                    </div>
                  </div>
                </div>
              </div>';
    return $script;
  }
?>