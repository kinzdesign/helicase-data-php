<?php
  function get_pgen() {
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
    $db = get_database();

    $output = array();

    // get quiz data from DB
    $res = $db->query("
      SELECT  cohort, questions, intro, likert1, likert2, COUNT(*) AS n
      FROM    (
                SELECT  p.participant, CONCAT(p.pathway,'-',p.experience) AS cohort,
                        CONCAT(q1.displayOrder, ': ', COALESCE(q1.multiIntroHtml, q1.likertIntroHtml)) AS intro, 
                        CONCAT(q1.question, '-', q2.question) AS questions,
                        CASE WHEN a1.score < 0 THEN -1 WHEN a1.score = 0 THEN 0 ELSE 1 END * CAST(r1.likert AS SIGNED) AS likert1, 
                        CASE WHEN a2.score < 0 THEN -1 WHEN a2.score = 0 THEN 0 ELSE 1 END * CAST(r2.likert AS SIGNED) AS likert2,
                        a1.answerText AS answerText1, a2.answerText AS answerText2,
                        a1.score AS answerScore1, a2.score AS answerScore2
                FROM    vw_participants AS p
                        INNER JOIN responses AS r1 ON p.participant = r1.participant
                        INNER JOIN responses AS r2 ON p.participant = r2.participant
                        INNER JOIN questions AS q1 ON r1.question = q1.question
                        INNER JOIN questions AS q2 ON r2.question = q2.question
                        LEFT  JOIN answers   AS a1 ON r1.answer = a1.answer
                        LEFT  JOIN answers   AS a2 ON r2.answer = a2.answer
                WHERE   p.pathway = 'CH' AND
                        p.isValid = 1 AND
                        q1.quiz = 'PGEN1' AND
                        q2.quiz = 'PGEN2' AND 
                        q1.displayOrder = q2.displayOrder
                ORDER   BY cohort, q1.displayOrder, p.participant
              ) AS foo
      GROUP   BY cohort, questions, intro, likert1, likert2");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      // ensure buckets exist
      if(!isset($output[$q['questions']]))
        $output[$q['questions']] = array();
      if(!isset($output[$q['questions']]['intro']))
        $output[$q['questions']]['intro'] = $q['intro'];
      if(!isset($output[$q['questions']]['cohorts']))
        $output[$q['questions']]['cohorts'] = array();
      if(!isset($output[$q['questions']]['cohorts'][$q['cohort']]['counts']))
        $output[$q['questions']]['cohorts'][$q['cohort']]['counts'] = array();
      if(!isset($output[$q['questions']]['cohorts'][$q['cohort']]['aggregates']))
        $output[$q['questions']]['cohorts'][$q['cohort']]['aggregates'] = array();
      // build data
      $l1 = $q['likert1'];
      $l2 = $q['likert2'];
      $delta = abs($l2) - abs($l1);
      if($delta < 0) {
        $aggDelta = '-';
      } elseif($delta > 0) {
        $aggDelta = '+';
      } else {
        $aggDelta = '0';
      }
      if($l1 < 0 && $l2 > 0) {
        $aggQuad = '-+';
      } elseif($l1 > 0 && $l2 > 0) {
        $aggQuad = '++';
      } elseif($l1 > 0 && $l2 < 0) {
        $aggQuad = '+-';
      } elseif($l1 < 0 && $l2 < 0) {
        $aggQuad = '--';
      } else {
        $aggQuad = '0*';
      }
      $key = "{$l1},{$l2}";
      $output[$q['questions']]['cohorts'][$q['cohort']]['counts'][$key]['likert1'] = $l1;
      $output[$q['questions']]['cohorts'][$q['cohort']]['counts'][$key]['likert2'] = $l2;
      $output[$q['questions']]['cohorts'][$q['cohort']]['counts'][$key]['delta'] = $delta;
      $output[$q['questions']]['cohorts'][$q['cohort']]['counts'][$key]['n'] = $q['n'];
      // update aggregates
      if(!isset($output[$q['questions']]['cohorts'][$q['cohort']]['aggregates'][$aggDelta]))
        $output[$q['questions']]['cohorts'][$q['cohort']]['aggregates'][$aggDelta] = 0;
      $output[$q['questions']]['cohorts'][$q['cohort']]['aggregates'][$aggDelta] += $q['n'];

      if(!isset($output[$q['questions']]['cohorts'][$q['cohort']]['aggregates'][$aggQuad]))
        $output[$q['questions']]['cohorts'][$q['cohort']]['aggregates'][$aggQuad] = 0;
      $output[$q['questions']]['cohorts'][$q['cohort']]['aggregates'][$aggQuad] += $q['n'];

      if(!isset($output[$q['questions']]['cohorts'][$q['cohort']]['aggregates']['n']))
        $output[$q['questions']]['cohorts'][$q['cohort']]['aggregates']['n'] = 0;
      $output[$q['questions']]['cohorts'][$q['cohort']]['aggregates']['n'] += $q['n'];
    }

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }
?>