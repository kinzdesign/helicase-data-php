<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/pgen/_get_pgen.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/pgen/_print_pgen_question_data.php');

  $pgen = get_pgen();
  $script = '';

  Layout::EmitTop('PGEN 1 vs. 2', true);
  echo '
      <div class="panel-group" id="a-pgen" role="tablist" aria-multiselectable="true">';
  foreach ($pgen as $question => $question_data) {
    $script = print_pgen_question_data($question, $question_data, $script, false);
  }
  echo "
      </div>
      <script>
        google.charts.load('current', {'packages':['corechart']});
        window.chartData = {};
        google.charts.setOnLoadCallback(initChartData);
        function initChartData() {
          $script
        }
      </script>";

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}