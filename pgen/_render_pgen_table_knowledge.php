<?php
  function render_pgen_table_knowledge($data) {
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_format_number.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_sig_cell.php');
    echo "
      <h2>Genetics Knowledge Among All Participants</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-bordered table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-all' rowspan='2'>Question</th>
            <th class='heavy-top heavy-right thin-bottom heavy-left' colspan='2'>Correct response, <em>n</em> (%)</th>
            <th class='heavy-top thin-right heavy-bottom heavy-left' rowspan='2'>Response changed to incorrect/correct on post, <em>n</em> (%)</th>
            <th class='heavy-top heavy-right heavy-bottom thin-left' rowspan='2'><em>P</em> value*</th>
          </tr>
          <tr>
            <th class='thin-top thin-right heavy-bottom heavy-left'>Pre</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'>Post</th>
          </tr>
        </thead>
        <tbody>";
    foreach ($data as $question => $stats) {
// Layout::print_r($stats);
      echo "
          <tr>
            <th class='thin-top heavy-right thin-bottom heavy-left'>{$question}</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>{$stats['Total']['CorrectPre']} (".format_number($stats['Total']['CorrectPrePct'])."%)</td>
            <td class='thin-top heavy-right thin-bottom thin-left'>{$stats['Total']['CorrectPost']} (".format_number($stats['Total']['CorrectPostPct'])."%)</td>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              {$stats['Total']['RightToWrong']} (".format_number($stats['Total']['RightToWrongPct'])."%) /
              {$stats['Total']['WrongToRight']} (".format_number($stats['Total']['WrongToRightPct'])."%)
            </td>
            ".sig_cell($stats['Total']['p'], 'thin-top heavy-right thin-bottom thin-left')."
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td class='heavy-all' colspan='6'>*<em>P</em> value obtained from McNemar exact tests</td>
          </tr>
        </tfoot>
      </table></div>


      <h2>Genetics Knowledge by Cohort</h2>
      <div class='table-responsive'><table class='table table-striped table-hover table-bordered table-center-headers'>
        <thead>
          <tr>
            <th class='heavy-all' rowspan='3'>Question</th>
            <th class='heavy-top heavy-right thin-bottom heavy-left' colspan='4'>Correct response, <em>n</em> (%)</th>
            <th class='heavy-top heavy-right thin-bottom heavy-left' colspan='4'>Response changed to incorrect/correct on post</th>
          </tr>
          <tr>
            <th class='mid-top mid-right thin-bottom heavy-left' colspan='2'>Gamified</th>
            <th class='mid-top heavy-right thin-bottom mid-left' colspan='2'>Control</th>
            <th class='mid-top mid-right thin-bottom heavy-left' colspan='2'>Gamified</th>
            <th class='mid-top heavy-right thin-bottom mid-left' colspan='2'>Control</th>
          </tr>
          <tr>
            <th class='thin-top thin-right heavy-bottom mid-left'>Pre</th>
            <th class='thin-top mid-right heavy-bottom thin-left'>Post</th>
            <th class='thin-top thin-right heavy-bottom mid-left'>Pre</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'>Post</th>
            <th class='thin-top thin-right heavy-bottom heavy-left'><em>n</em> (%)</th>
            <th class='thin-top mid-right heavy-bottom thin-left'><em>P</em> value*</th>
            <th class='thin-top thin-right heavy-bottom thin-left'><em>n</em> (%)</th>
            <th class='thin-top heavy-right heavy-bottom thin-left'><em>P</em> value*</th>
          </tr>
        </thead>
        <tbody>";
    foreach ($data as $question => $stats) {
      echo "
          <tr>
            <th class='thin-top heavy-right thin-bottom heavy-left'>{$question}</th>
            <td class='thin-top thin-right thin-bottom heavy-left'>{$stats['CH-G']['CorrectPre']} (".format_number($stats['CH-G']['CorrectPrePct'])."%)</td>
            <td class='thin-top mid-right thin-bottom thin-left'>{$stats['CH-G']['CorrectPost']} (".format_number($stats['CH-G']['CorrectPostPct'])."%)</td>
            <td class='thin-top thin-right thin-bottom mid-left'>{$stats['CH-C']['CorrectPre']} (".format_number($stats['CH-C']['CorrectPrePct'])."%)</td>
            <td class='thin-top heavy-right thin-bottom thin-left'>{$stats['CH-C']['CorrectPost']} (".format_number($stats['CH-C']['CorrectPostPct'])."%)</td>
            <td class='thin-top thin-right thin-bottom heavy-left'>
              {$stats['CH-G']['RightToWrong']} (".format_number($stats['CH-G']['RightToWrongPct'])."%) /
              {$stats['CH-G']['WrongToRight']} (".format_number($stats['CH-G']['WrongToRightPct'])."%)
            </td>
            ".sig_cell($stats['CH-G']['p'], 'thin-top mid-right thin-bottom thin-left')."
            <td class='thin-top mid-right thin-bottom mid-left'>
              {$stats['CH-C']['RightToWrong']} (".format_number($stats['CH-C']['RightToWrongPct'])."%) /
              {$stats['CH-C']['WrongToRight']} (".format_number($stats['CH-C']['WrongToRightPct'])."%)
            </td>
            ".sig_cell($stats['CH-C']['p'], 'thin-top heavy-right thin-bottom thin-left')."
          </tr>";
    }
    echo "
        </tbody>
        <tfoot>
          <tr>
            <td class='heavy-all' colspan='14'>*<em>P</em> value obtained from McNemar exact tests</td>
          </tr>
        </tfoot>
      </table></div>";

  }
?>