<?php
  function print_pgen_scatter($question_id, $cohort, $scores, $script = '') {
    $data = '';
    // compute percentages
    $counts = array();
    $n = $scores['aggregates']['n'];
    $counts['N-+'] = !isset($scores['aggregates']['-+']) ? 0 : $scores['aggregates']['-+'];
    $counts['%-+'] = round($counts['N-+'] / $n * 100);

    $counts['N--'] = !isset($scores['aggregates']['--']) ? 0 : $scores['aggregates']['--'];
    $counts['%--'] = round($counts['N--'] / $n * 100);

    $counts['N++'] = !isset($scores['aggregates']['++']) ? 0 : $scores['aggregates']['++'];
    $counts['%++'] = round($counts['N++'] / $n * 100);

    $counts['N+-'] = !isset($scores['aggregates']['+-']) ? 0 : $scores['aggregates']['+-'];
    $counts['%+-'] = round($counts['N+-'] / $n * 100);

    $counts['N0*'] = !isset($scores['aggregates']['0*']) ? 0 : $scores['aggregates']['0*'];
    $counts['%0*'] = round($counts['N0*'] / $n * 100);

    $counts['N+'] = !isset($scores['aggregates']['+']) ? 0 : $scores['aggregates']['+'];
    $counts['%+'] = round($counts['N+'] / $n * 100);

    $counts['N-'] = !isset($scores['aggregates']['-']) ? 0 : $scores['aggregates']['-'];
    $counts['%-'] = round($counts['N-'] / $n * 100);

    $counts['N0'] = !isset($scores['aggregates']['0']) ? 0 : $scores['aggregates']['0'];
    $counts['%0'] = round($counts['N0'] / $n * 100);

    // determine layout (Quad or Positive)
    $layout = $counts['%++'] + $counts['%0*'] < 100 ? 'q' : 'p';
    // build javascript
    $script .= "
      window.chartData['c-p-$layout-$question_id-$cohort'] = google.visualization.arrayToDataTable([
        ['Cohort', 'PGEN1', 'PGEN2', 'Likert Delta', 'Count']";
    // iterate rows to output data and compute aggregates
    foreach ($scores['counts'] as $row) {
      $script .= ",
        ['{$cohort}', {$row['likert1']}, {$row['likert2']}, {$row['delta']}, {$row['n']}]";
    }
    $script .= "
      ]);";
    // open column, emit header, emit chart, open table
    echo "
                      <div class='col-sm-6'>
                        <h3>$cohort</h3>
                        <div id='c-p-$layout-$question_id-$cohort' class='chart'></div>";
    // output data table
    if($layout == 'q') echo "
                        <div class='table-responsive'><table class='table table-center-headers table-center-cells'>
                          <tbody>
                            <tr>
                              <td colspan='2' rowspan='2' class='heavy-all'>
                                <h4>Multiple Choice</h4>
                                (<em>N</em> = $n)
                              </td>
                              <th colspan='2' class='heavy-top heavy-right mid-bottom heavy-left'>After</th>
                            </tr>
                            <tr>
                              <th class='mid-top mid-right heavy-bottom heavy-left'>Wrong</th>
                              <th class='mid-top heavy-right heavy-bottom mid-left'>Right</th>
                            </tr>
                            <tr>
                              <th rowspan='2' class='heavy-top mid-right heavy-bottom heavy-left'>Before</th>
                              <th class='heavy-top heavy-right mid-bottom mid-left'>Right</th>
                              <td class='heavy-top mid-right mid-bottom heavy-left' title='Wrong to Wrong'>
                                <i class='fa fa-thumbs-up'></i> <i class='fa fa-arrow-right'></i> <i class='fa fa-thumbs-down'></i>: 
                                {$counts['N+-']} ({$counts['%+-']}%)
                              </td>
                              <td class='heavy-top heavy-right mid-bottom mid-left' title='Wrong to Right'>
                                <i class='fa fa-thumbs-up'></i> <i class='fa fa-arrow-right'></i> <i class='fa fa-thumbs-up'></i>: 
                                {$counts['N++']} ({$counts['%++']}%)
                              </td>
                            </tr>
                            <tr>
                              <th class='mid-top heavy-right heavy-bottom mid-left'>Wrong</th>
                              <td class='mid-top mid-right heavy-bottom heavy-left' title='Right to Wrong'>
                                <i class='fa fa-thumbs-down'></i> <i class='fa fa-arrow-right'></i> <i class='fa fa-thumbs-down'></i>: 
                                {$counts['N--']} ({$counts['%--']}%)
                              </td>
                              <td class='mid-top heavy-right heavy-bottom mid-left' title='Right to Right'>
                                <i class='fa fa-thumbs-down'></i> <i class='fa fa-arrow-right'></i> <i class='fa fa-thumbs-up'></i>: 
                                {$counts['N-+']} ({$counts['%-+']}%)
                              </td>
                            </tr>
                          </tbody>
                        </table></div>";
    echo "
                        <div class='table-responsive'><table class='table table-center-headers table-center-cells'>
                          <tbody>
                            <tr>
                              <td rowspan='2' class='heavy-all'>
                                <h4>Likert Scale</h4>
                                (<em>N</em> = $n)
                              </td>
                              <th class='heavy-top mid-right heavy-bottom heavy-left'>Decreased</th>
                              <th class='heavy-top mid-right heavy-bottom mid-left'>Maintained</th>
                              <th class='heavy-top heavy-right heavy-bottom mid-left'>Increased</th>
                            </tr>
                            <tr>
                              <td class='heavy-top mid-right heavy-bottom heavy-left' title='Likert Decreased'>
                                <i class='fa fa-arrow-down' style='color:blue'></i>: 
                                {$counts['N-']} ({$counts['%-']}%)
                              </td>
                              <td class='heavy-top mid-right heavy-bottom mid-left' title='Likert Constant'>
                                <i class='fa fa-arrow-right' style='color:lime'></i>: 
                                {$counts['N0']} ({$counts['%0']}%)
                              </td>
                              <td class='heavy-top heavy-right heavy-bottom mid-left' title='Likert Increased'>
                                <i class='fa fa-arrow-up' style='color:red'></i>: 
                                {$counts['N+']} ({$counts['%+']}%)
                              </td>
                            </tr>
                          </tbody>
                        </table></div>
                      </div>";
    return $script;
  }
?>