<?php
  function get_pgen_table_knowledge() {
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
    require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_stats.php');
    $db = get_database();

    $output = array();

    // get quiz data from DB
    $res = $db->query("
          SELECT  CONCAT(q1.displayOrder, '. ', q1.multiIntroHtml) AS Question, p.cohort AS Cohort,
                  SUM(CASE WHEN a1.score IS NULL THEN NULL WHEN a1.score > 0 THEN 1 ELSE 0 END) AS CorrectPre,
                  SUM(CASE WHEN a2.score IS NULL THEN NULL WHEN a2.score > 0 THEN 1 ELSE 0 END) AS CorrectPost,
                  SUM(CASE WHEN a1.score > 0 AND a2.score < 0 THEN 1 ELSE 0 END) AS RightToWrong,
                  SUM(CASE WHEN a1.score < 0 AND a2.score > 0 THEN 1 ELSE 0 END) AS WrongToRight,
                  COUNT(*) AS N
          FROM    vw_participants AS p
                  INNER JOIN responses AS r1 ON p.participant = r1.participant
                  INNER JOIN responses AS r2 ON p.participant = r2.participant
                  INNER JOIN answers   AS a1 ON r1.answer = a1.answer
                  INNER JOIN answers   AS a2 ON r2.answer = a2.answer
                  INNER JOIN questions AS q1 ON r1.question = q1.question AND q1.quiz = 'PGEN1'
                  INNER JOIN questions AS q2 ON r2.question = q2.question AND q2.quiz = 'PGEN2' AND q1.displayOrder = q2.displayOrder
          WHERE   p.isValid = 1
                  AND ((q1.questionFormat & 2) > 0) -- showMulti
          GROUP   BY question, p.cohort WITH ROLLUP");
    check_for_db_error($res, $db);
    while($q = $res->fetch_assoc()) {
      if($q['Question']) {
        // ensure buckets exist
        if(!isset($output[$q['Question']]))
          $output[$q['Question']] = array();
        $cohort = $q['Cohort'] ?? 'Total';
        if(!isset($output[$q['Question']][$q['Cohort']]))
          $output[$q['Question']][$cohort] = array();
        $n = $q['N'];
        $output[$q['Question']][$cohort]['N'] = $n;
        $output[$q['Question']][$cohort]['CorrectPre'] = $q['CorrectPre'];
        $output[$q['Question']][$cohort]['CorrectPrePct'] = $q['CorrectPre'] / $n * 100;
        $output[$q['Question']][$cohort]['CorrectPost'] = $q['CorrectPost'];
        $output[$q['Question']][$cohort]['CorrectPostPct'] = $q['CorrectPost'] / $n * 100;
        $output[$q['Question']][$cohort]['RightToWrong'] = $q['RightToWrong'];
        $output[$q['Question']][$cohort]['RightToWrongPct'] = $q['RightToWrong'] / $n * 100;
        $output[$q['Question']][$cohort]['WrongToRight'] = $q['WrongToRight'];
        $output[$q['Question']][$cohort]['WrongToRightPct'] = $q['WrongToRight'] / $n * 100;
        // calculate P value using McNemar's exact test
        $mcnemar = mcnemar_exact($q['WrongToRight'], $q['RightToWrong']);
        if($mcnemar)
          $output[$q['Question']][$cohort]['p'] = $mcnemar['p'];
      }
    }

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }
?>