<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_two_sample.php');

  $db = get_database();
  
  $questionPre  = Layout::GetQueryStringInt('pre');
  $questionPost = Layout::GetQueryStringInt('post');
  $identCol     = 'Participant';
  $labelCol     = 'Cohort';
  $valueCol     = 'LikertChange';

  // get data
  $res = $db->query("
    SELECT  p.participant AS $identCol, 
            p.cohort AS $labelCol,
            CAST(r2.likert AS SIGNED) - CAST(r1.likert AS SIGNED) AS {$valueCol}
    FROM    vw_participants AS p
            INNER JOIN w_participants AS wp ON p.participant = wp.participant
            INNER JOIN responses AS r1 ON p.participant = r1.participant AND r1.question = $questionPre
            INNER JOIN responses AS r2 ON p.participant = r2.participant AND r2.question = $questionPost
    WHERE   p.isValid = 1
    ORDER   BY cohort, p.participant");
  check_for_db_error($res, $db);

  render_t_test_two_sample($res, $questionPre, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}