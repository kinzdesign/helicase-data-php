<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_two_sample.php');

  $db = get_database();
  
  $identCol     = 'Participant';
  $labelCol     = 'Cohort';
  $valueCol     = "SelfEfficacyChange";

  // get data
  $res = $db->query("
        SELECT  participant AS $identCol, 
                cohort AS $labelCol, 
                SUM(ABS(likertChange)) AS $valueCol
        FROM    (
            SELECT  p.participant, 
                    p.cohort,
                    CAST(r2.likert AS SIGNED) - CAST(r1.likert AS SIGNED) AS likertChange
            FROM    vw_participants AS p
                    INNER JOIN responses AS r1 ON p.participant = r1.participant
                    INNER JOIN questions AS q1 ON r1.question = q1.question
                    INNER JOIN responses AS r2 ON p.participant = r2.participant
                    INNER JOIN questions AS q2 ON r2.question = q2.question
            WHERE   p.isValid = 1
                    AND q1.quiz = 'PGEN1'
                    AND q2.quiz = 'PGEN2'
                    AND q1.displayOrder = q2.displayOrder
                    AND q1.displayOrder > 9
            ORDER   BY cohort, p.participant
        ) AS a
        GROUP BY participant, cohort");
  check_for_db_error($res, $db);

  render_t_test_two_sample($res, false, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}