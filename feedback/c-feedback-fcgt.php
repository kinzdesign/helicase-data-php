<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_correlation.php');

  $db = get_database();
  
  $question   = Layout::GetQueryStringInt('question');
  $labelAlias = 'FcgtScore';
  $valueAlias = 'Likert';

  // get data
  $res = $db->query("
    SELECT  p.participant AS Participant, 
            p.cohort      AS Cohort, 
            p.fcgtCorrect AS $labelAlias, 
            r.likert      AS $valueAlias
    FROM    vw_participants AS p
            INNER JOIN responses AS r ON p.participant = r.participant
    WHERE   p.isValid = 1 
            AND r.question = $question
            AND r.likert > 0
    ORDER   BY $labelAlias, $valueAlias, participant");
  $data = [ 'rows' => get_rows_as_array($res, $db) ];

  render_correlation($data, $labelAlias, $valueAlias, false);
} catch (Exception $e) {
  Layout::RenderException($e);
}