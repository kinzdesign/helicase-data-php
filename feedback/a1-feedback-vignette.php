<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_anova_one_way.php');

  $db = get_database();
  
  $question     = Layout::GetQueryStringInt('question');
  $identCol     = 'Participant';
  $treatCol     = 'Cohort';
  $labelCol     = 'Vignette';
  $valueCol     = 'Likert';

  // get data
  $res = $db->query("
    SELECT  p.participant AS $identCol,
            p.cohort AS $treatCol,
            p.vignette AS $labelCol, 
            r.likert AS $valueCol
    FROM    questions AS q
            INNER JOIN responses AS r ON q.question = r.question
            INNER JOIN vw_participants AS p ON r.participant = p.participant
    WHERE   p.isValid = 1 
            AND q.question = $question
    ORDER   BY q.quiz, p.experience DESC, r.likert DESC");
  $data = array('rows' => get_rows_as_array($res, $db));

  render_anova_one_way($data, $question, $identCol, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}