<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_quiz_likert_data.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_print_quiz_likert_table.php');

  $data = get_quiz_likert_data('FEED');

  Layout::$EmitContainer = false;
  Layout::EmitTop('Feedback Tables', true);

  print_quiz_likert_table($data, 'Feedback');

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}