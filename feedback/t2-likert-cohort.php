<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_two_sample.php');

  $db = get_database();
  
  $question = Layout::GetQueryStringInt('question');
  $labelCol = 'Cohort';
  $valueCol = 'Likert';

  // get data
  $res = $db->query("
    SELECT  p.participant AS Participant, p.cohort AS $labelCol, r.likert AS $valueCol
    FROM    responses AS r
            INNER JOIN vw_participants AS p ON r.participant = p.participant
    WHERE   r.question = $question
            AND r.likert IS NOT NULL
            AND p.isValid = 1
            AND r.likert > 0
    ORDER   BY p.cohort DESC, r.likert DESC");
  check_for_db_error($res, $db);

  render_t_test_two_sample($res, $question, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}