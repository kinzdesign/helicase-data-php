<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('Feedback');
?>
      <h2>Summary Tables and Charts</h2>
      <ul>
        <li><a href="charts.php">View Question Charts</a></li>
        <li><a href="tables.php">View Tables</a></li>
      </ul>

      <h2>Feedback Questions</h2>
      <div class='table-responsive'><table class="table table-striped">
        <thead>
          <tr>
            <td>
            </td>
            <th class="text-center">Correlation Analysis</th>
            <th class="text-center">Two-Sample t-Test</th>
            <th class="text-center">One-Way ANOVA</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>1: I feel like I know more about genetics now than I did before participating.</th>
            <td>
              <a href="c-feedback-correct.php?question=88">by Questions Correct</a><br/>
              <a href="c-feedback-fcgt.php?question=88">by GLAC Score</a><br/>
              <a href="c-feedback-pgen.php?question=88">by PGen1 Knowledge</a><br/>
            </td>
            <td><a href="t2-likert-cohort.php?question=88">by Cohort</a></td>
            <td><a href="a1-feedback-vignette.php?question=88">by Vignette</a></td>
          </tr>
          <tr>
            <th>2: The exercise was enjoyable.</th>
            <td>
              <a href="c-feedback-correct.php?question=89">by Questions Correct</a><br/>
              <a href="c-feedback-fcgt.php?question=89">by GLAC Score</a><br/>
              <a href="c-feedback-pgen.php?question=89">by PGen1 Knowledge</a><br/>
            </td>
            <td><a href="t2-likert-cohort.php?question=89">by Cohort</a></td>
            <td><a href="a1-feedback-vignette.php?question=89">by Vignette</a></td>
          </tr>
          <tr>
            <th>3: The exercise was frustrating.</th>
            <td>
              <a href="c-feedback-correct.php?question=90">by Questions Correct</a><br/>
              <a href="c-feedback-fcgt.php?question=90">by GLAC Score</a><br/>
              <a href="c-feedback-pgen.php?question=90">by PGen1 Knowledge</a><br/>
            </td>
            <td><a href="t2-likert-cohort.php?question=90">by Cohort</a></td>
            <td><a href="a1-feedback-vignette.php?question=90">by Vignette</a></td>
          </tr>
          <tr>
            <th>4: I felt stressed or pressured during the exercise.</th>
            <td>
              <a href="c-feedback-correct.php?question=91">by Questions Correct</a><br/>
              <a href="c-feedback-fcgt.php?question=91">by GLAC Score</a><br/>
              <a href="c-feedback-pgen.php?question=91">by PGen1 Knowledge</a><br/>
            </td>
            <td><a href="t2-likert-cohort.php?question=91">by Cohort</a></td>
            <td><a href="a1-feedback-vignette.php?question=91">by Vignette</a></td>
          </tr>
          <tr>
            <th>5: I would consider using a platform like this to learn more about science.</th>
            <td>
              <a href="c-feedback-correct.php?question=92">by Questions Correct</a><br/>
              <a href="c-feedback-fcgt.php?question=92">by GLAC Score</a><br/>
              <a href="c-feedback-pgen.php?question=92">by PGen1 Knowledge</a><br/>
            </td>
            <td><a href="t2-likert-cohort.php?question=92">by Cohort</a></td>
            <td><a href="a1-feedback-vignette.php?question=92">by Vignette</a></td>
          </tr>
          <tr>
            <th>6: The scenario related to genetic testing helped me emotionally connect with the material.</th>
            <td>
              <a href="c-feedback-correct.php?question=93">by Questions Correct</a><br/>
              <a href="c-feedback-fcgt.php?question=93">by GLAC Score</a><br/>
              <a href="c-feedback-pgen.php?question=93">by PGen1 Knowledge</a><br/>
            </td>
            <td><a href="t2-likert-cohort.php?question=93">by Cohort</a></td>
            <td><a href="a1-feedback-vignette.php?question=93">by Vignette</a></td>
          </tr>
          <tr>
            <th>7: I am more likely to consider undergoing personal genetic testing after completing this exercise.</th>
            <td>
              <a href="c-feedback-correct.php?question=94">by Questions Correct</a><br/>
              <a href="c-feedback-fcgt.php?question=94">by GLAC Score</a><br/>
              <a href="c-feedback-pgen.php?question=94">by PGen1 Knowledge</a><br/>
            </td>
            <td><a href="t2-likert-cohort.php?question=94">by Cohort</a></td>
            <td><a href="a1-feedback-vignette.php?question=94">by Vignette</a></td>
          </tr>
          <tr>
            <th>8: I feel like I gained something from this exercise.</th>
            <td>
              <a href="c-feedback-correct.php?question=95">by Questions Correct</a><br/>
              <a href="c-feedback-fcgt.php?question=95">by GLAC Score</a><br/>
              <a href="c-feedback-pgen.php?question=95">by PGen1 Knowledge</a><br/>
            </td>
            <td><a href="t2-likert-cohort.php?question=95">by Cohort</a></td>
            <td><a href="a1-feedback-vignette.php?question=95">by Vignette</a></td>
          </tr>
        </tbody>
      </table></div>
<?php 
  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}