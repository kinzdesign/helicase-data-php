<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_two_sample.php');

  $db = get_database();
  
  $displayOrder = Layout::GetQueryStringInt('question');
  $identCol     = 'Participant';
  $labelCol     = 'Cohort';
  $valueCol     = 'Likert';

  // get data
  $res = $db->query("
    SELECT  p.participant AS $identCol,
            p.cohort AS $labelCol, 
            r.likert AS $valueCol
    FROM    questions AS q
            INNER JOIN responses AS r ON q.question = r.question
            INNER JOIN vw_participants AS p ON r.participant = p.participant
    WHERE   q.quiz LIKE 'V-CH-%'
            AND displayOrder = $displayOrder
            AND p.isValid = 1
    ORDER   BY p.cohort DESC, p.participant");
  check_for_db_error($res, $db);

  render_t_test_two_sample($res, false, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}