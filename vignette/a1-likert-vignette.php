<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_anova_one_way.php');

  $db = get_database();
  
  $displayOrder = Layout::GetQueryStringInt('question');
  $identCol     = 'Participant';
  $treatCol     = 'Cohort';
  $labelCol     = 'Vignette';
  $valueCol     = 'Likert';

  // get data
  $res = $db->query("
    SELECT  p.participant AS $identCol,
            p.cohort AS $treatCol,
            q.quiz AS $labelCol, 
            r.likert AS $valueCol
    FROM    questions AS q
            INNER JOIN responses AS r ON q.question = r.question
            INNER JOIN vw_participants AS p ON r.participant = p.participant
    WHERE   q.quiz LIKE 'V-CH-%'
            AND displayOrder = $displayOrder
            AND p.isValid = 1
    ORDER   BY q.quiz, p.experience DESC, r.likert DESC");
  $data = array('rows' => get_rows_as_array($res, $db));

  render_anova_one_way($data, 118 + $displayOrder, $identCol, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}