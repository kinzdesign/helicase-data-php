<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_quiz_data.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_print_quiz_table.php');

  $data = get_quiz_data('V-CH-');

  Layout::$EmitContainer = false;
  Layout::EmitTop('Vignette Tables', true);

  print_quiz_table($data, 'Vignette');

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}