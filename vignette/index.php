<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('Vignette');
?>
      <h2>Summary Tables and Charts</h2>
      <ul>
        <li><a href="charts.php">View Question Charts</a></li>
        <li><a href="tables.php">View Tables</a></li>
      </ul>

      <h2>Vignette Likert Summaries</h2>
      <div class='table-responsive'><table class="table table-striped">
        <thead>
          <tr>
            <td>
            </td>
            <th class="text-center">Paired t-Test</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>2-4: At this point, how well do you feel like you understand Brian's genetic risk factors for Coronary Artery Disease and Hypertension?</th>
            <td>
              <a href="tp-vignette.php?pre=2&amp;post=4">Pre vs. Post</a>
            </td>
          </tr>
          <tr>
            <th>3-5: If you had to rank Brian's cardiac risk at this point, how would you rank it?</th>
            <td>
              <a href="tp-vignette.php?pre=3&amp;post=5">Pre vs. Post</a>
            </td>
          </tr>
        </tbody>
      </table></div>
      <div class='table-responsive'><table class="table table-striped">
        <thead>
          <tr>
            <td>
            </td>
            <th class="text-center">Two-Sample t-Test</th>
            <th class="text-center">One-Way ANOVA</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>1: If you were in Brian's place, how likely is it that you would have gone through with the genetic testing?</th>
            <td>
              <a href="t2-likert-vignette.php?question=1">Likert by Cohort</a>
            </td>
            <td>
              <a href="a1-likert-vignette.php?question=1">Likert by Vignette</a>
            </td>
          </tr>
          <tr>
            <th>2: PRE: At this point, how well do you feel like you understand Brian's genetic risk factors for Coronary Artery Disease and Hypertension?</th>
            <td>
              <a href="t2-likert-vignette.php?question=2">Likert by Cohort</a>
            </td>
            <td>
              <a href="a1-likert-vignette.php?question=2">Likert by Vignette</a>
            </td>
          </tr>
          <tr>
            <th>3: PRE: If you had to rank Brian's cardiac risk at this point, how would you rank it?</th>
            <td>
              <a href="t2-likert-vignette.php?question=3">Likert by Cohort</a>
            </td>
            <td>
              <a href="a1-likert-vignette.php?question=3">Likert by Vignette</a>
            </td>
          </tr>
          <tr>
            <th>4: POST: At this point, how well do you feel like you understand Brian's genetic risk factors for Coronary Artery Disease and Hypertension?</th>
            <td>
              <a href="t2-likert-vignette.php?question=4">Likert by Cohort</a>
            </td>
            <td>
              <a href="a1-likert-vignette.php?question=4">Likert by Vignette</a>
            </td>
          </tr>
          <tr>
            <th>5: POST: If you had to rank Brian's cardiac risk at this point, how would you rank it?</th>
            <td>
              <a href="t2-likert-vignette.php?question=5">Likert by Cohort</a>
            </td>
            <td>
              <a href="a1-likert-vignette.php?question=5">Likert by Vignette</a>
            </td>
          </tr>
          <tr>
            <th>6: If you were in Brian's place, how much control would you feel you had over your cardiac health?</th>
            <td>
              <a href="t2-likert-vignette.php?question=6">Likert by Cohort</a>
            </td>
            <td>
              <a href="a1-likert-vignette.php?question=6">Likert by Vignette</a>
            </td>
          </tr>
          <tr>
            <th>7: If you were in Brian's place, how likely would you be to change your diet and exercise habits after meeting with Jeff?</th>
            <td>
              <a href="t2-likert-vignette.php?question=7">Likert by Cohort</a>
            </td>
            <td>
              <a href="a1-likert-vignette.php?question=7">Likert by Vignette</a>
            </td>
          </tr>
        </tbody>
      </table></div>

<?php 
  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}