<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_paired.php');

  $db = get_database();
  
  $questionPre  = Layout::GetQueryStringInt('pre');
  $questionPost = Layout::GetQueryStringInt('post');
  $identCol     = 'Participant';
  $labelCol     = 'Cohort';
  $valueCol1    = 'Likert';
  $valueCol2    = false;
  $suffix1      = 'Pre';
  $suffix2      = 'Post';

  // get data
  $res = $db->query("
    SELECT  p.participant AS $identCol, 
            p.cohort AS $labelCol,
            r1.likert AS {$valueCol1}{$suffix1}, 
            r2.likert AS {$valueCol1}{$suffix2}
    FROM    vw_participants AS p
            INNER JOIN responses AS r1 ON p.participant = r1.participant
            INNER JOIN responses AS r2 ON p.participant = r2.participant
            INNER JOIN questions AS q1 ON r1.question = q1.question
            INNER JOIN questions AS q2 ON r2.question = q2.question
    WHERE   p.isValid = 1 AND
            q1.quiz LIKE 'V-CH-%' AND
            q1.quiz = q2.quiz AND
            q1.displayOrder = $questionPre AND
            q2.displayOrder = $questionPost
    ORDER   BY p.cohort, p.participant");
  $data = array('rows' => get_rows_as_array($res, $db));

  render_t_test_paired($data, 118 + $questionPre, $identCol, $labelCol, $valueCol1, $valueCol2, $suffix1, $suffix2);
} catch (Exception $e) {
  Layout::RenderException($e);
}