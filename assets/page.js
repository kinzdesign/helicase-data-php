$(document).ready(setActiveNav);
function setActiveNav(){
	$('.navbar-nav > li > a[href]').each(function(){
		if(window.location.pathname.startsWith($(this).attr('href')))
			$(this).parent().addClass('active');
		else
			$(this).parent().removeClass('active');
	});
}