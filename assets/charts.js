// Load the Visualization API and the corechart package.
// google.charts.load("current", {"packages":["corechart"]});

window.chartData = window.chartData || {};
window.charts = window.charts || {};

google.charts.setOnLoadCallback(initCharts);
function initCharts() {
  // allow show/hide binding
  // adapted from http://stackoverflow.com/a/8898710
  $.each(['show','hide', 'toggleClass', 'addClass', 'removeClass'], function(){
    var _oldFn = $.fn[this];
    $.fn[this] = function(){
      var hidden = this.find(':hidden').add(this.filter(':hidden'));
      var visible = this.find(':visible').add(this.filter(':visible'));
      var result = _oldFn.apply(this, arguments);
      hidden.filter(':visible').each(function(){
          $(this).triggerHandler('show');
      });
      visible.filter(':hidden').each(function(){
          $(this).triggerHandler('hide');
      });
      return result;
    }
  });

  var hidChartIds = new Array();
  var visChartIds = new Array();
  window.chartData = window.chartData || {};
  $('.chart').each(function() {
    var $this = $(this);
    var id = $this.attr('id');
    // see if data already defined
    if(!(id in window.chartData)) {
      if($this.is(':visible'))
        visChartIds.push(id);
      else
        hidChartIds.push(id);
    }
    addDownloadButton($this, id);
  });

  // download data for visible charts
  if(visChartIds.length > 0) {
    downloadChartData(visChartIds, function(ids){
      if(!Array.isArray(ids)) ids = [ ids ];
      // show visible charts
      for(var i = 0; i < ids.length; i++) {
        showChart($('#' + ids[i]));
      }
    });
    // download data for hidden charts
    if(hidChartIds.length > 0) {
      downloadChartData(hidChartIds, function(ids){});
    }
  } else {
    // download data for hidden charts
    if(hidChartIds.length > 0) {
      downloadChartData(hidChartIds, function(ids){});
    }
  }
  $('.chart').bind('show', function() {
    showChart($(this));
  });
  $('.chart:visible').each(function() {
    showChart($(this));
  });
}

function downloadChartData(ids, success) {
  if(Array.isArray(ids) && ids.length > 5) {
    downloadChartData(ids.slice(0, 5), function(){
      success();
      downloadChartData(ids.slice(6), success);
      return;
    });
  } else {
    if(Array.isArray(ids))
      ids = ids.join();
    $.getJSON( '/api/chart-data.php?ids=' + ids )
      .done(function (data) { 
        for(var id in data) {
          var prefix = id.substring(0, 5);
          switch(prefix) {
            default:
              // convert array to basic data table
              window.chartData[id] = 
                google.visualization.arrayToDataTable(data[id]);
              break;
          }
        }
        if(success) success(ids);
      })
      .fail(function (jqXHR, textStatus, errorThrown) { 
        console.log(textStatus, errorThrown, jqXHR.responseText); 
      });
  }
}

function showChart($this) {
  var id = $this.attr('id');
  if(!id) return;
  // don't re-show a chart
  if(window.charts[id]) return;
  // ensure data is downloaded
  getData(id, function(data){
    // draw chart
    var prefix = id.substring(0, 5);
    chart = createChart(prefix, document.getElementById(id));
    chart.draw(data, getOptions(id, prefix));
    window.charts[id] = chart;
    addDownloadButton($this, id);
  });
}

function createChart(prefix, el) {
  switch(prefix) {
    case 'box_p':
      return new google.visualization.LineChart(el);
    case 'c-b-p':
      return new google.visualization.PieChart(el);
    case 'c-d-b':
    case 'c-d-c':
      return new google.visualization.ColumnChart(el);
    case 'c-g-c':
      return new google.visualization.BubbleChart(el);
    case 'c-l-a':
    case 'c-l-b':
      return new google.visualization.ColumnChart(el);
    case 'c-m-a':
      return new google.visualization.BarChart(el);
    case 'c-p-p':
    case 'c-p-q':
      return new google.visualization.BubbleChart(el);
      break;
    case 'c-p-y':
      return new google.visualization.BarChart(el);
    default:
      return new google.visualization.PieChart(el);
  }
}

function getData(id, callback) {
  if(!window.chartData[id]) {
    downloadChartData(id, function(){
      if(window.chartData[id])
        callback(window.chartData[id]);
    });
  } else {
    callback(window.chartData[id]);
  }
}

function getOptions(id, prefix) {
  if(window.chartOptions && window.chartOptions[id])
    return window.chartOptions[id];
  var likertColorAxis = {
    minValue: -5,
    maxValue: 5,
    values: [-5, -1, 0, 1, 5],
    colors: ['blue', 'cyan', 'lime', 'yellow', 'red'],
    legend: {position: 'bottom'}
  };
  switch(prefix) {
    case 'c-b-p':
      return {
        height: 800,
        legend: {
          position: 'top',
          maxLines: 4,
          alignment: 'center'
        },
        tooltip: { showColorCode: true },
        pieSliceText: 'value'
      };
    case 'c-d-b':
    case 'c-d-c':
      return {
        legend: { position: 'none'} 
      };
    case 'c-d-p': 
      return {
        legend: { position: 'top', maxLines: 4 }
      };
    case 'c-d-w': 
      return {
        legend: { position: 'right' }
      }; 
    case 'c-g-c':
      return {
        height: 800,
        legend: { position: 'none' },
        chartArea: {
          width: '85%', 
          height: '85%'
        },
        hAxis: { 
          title: 'GLAC Questions Correct',
          viewWindowMode: 'explicit',
          viewWindow: {
            min: 4,
            max: 9
          }
        },
        vAxis: { 
          title: 'Total Questions Correct',
          viewWindowMode: 'explicit',
          viewWindow: {
            min: 20,
            max: 36
          }
        },
        bubble: {
          textStyle: {
            color: 'transparent',
            auraColor: 'none'
          }
        },
        colors: [ '#3498db' ],
        trendlines: {
          0: { type: 'linear', color: '#e74c3c' }
        }
      };
    case 'c-l-a': 
      return {
        'title':'Average Likert by Cohort',
        'legend': {position: 'none'},
        'vAxis':{
          viewWindowMode:'explicit',
          viewWindow: {
            max:5,
            min:0
          }
        }
      };
    case 'c-l-b':
      return {
        'title':'Likert by Cohort',
        legend: { position: 'right', maxLines: 3 },
        isStacked: true
      };
    case 'c-m-a': 
      return {
        'title':'Answers by Cohort',
        legend: { position: 'top', maxLines: 3 },
        isStacked: true,
        'hAxis':{
          viewWindowMode:'explicit',
          viewWindow: {
            max:50,
            min:0
          }
        }
      };
    case 'c-p-p': 
      return {
        legend: {position: 'none'},
        bubble: {
          textStyle: {color: 'none'}
        },
        colorAxis: likertColorAxis,
        hAxis: {
          title: 'PGEN1 Likert',
          viewWindowMode:'explicit',
          viewWindow: {
            min: -0.5,
            max: 7
          }
        },
        vAxis: {
          title: 'PGEN2 Likert',
          viewWindowMode:'explicit',
          viewWindow: {
            min: -0.5,
            max: 7
          }
        }
      };
    case 'c-p-q': 
      return {
        legend: {position: 'none'},
        bubble: {
          textStyle: {color: 'none'}
        },
        colorAxis: likertColorAxis,
        hAxis: {
          title: 'PGEN1 Likert',
          viewWindowMode:'explicit',
          viewWindow: {
            min: -7,
            max: 7
          }
        },
        vAxis: {
          title: 'PGEN2 Likert',
          viewWindowMode:'explicit',
          viewWindow: {
            min: -7,
            max: 7
          }
        }
      };
    case 'c-p-y': 
      return {
        isStacked: true,
        hAxis: { format: ';' },
        vAxis: { direction: -1 }
      };
    default:
      return { };
  }
}

function addDownloadButton($chart, id) {
  $('<div class="chart-dl-btn"><i class="fa fa-fw fa-2x fa-download" title="Download as PNG"></i></div>').click(function(){
    window.open(window.charts[id].getImageURI(), '_blank');
  }).appendTo($chart);
}