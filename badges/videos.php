<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');

  Layout::EmitTop('Badge Video Charts', true);
  $db = get_database();

  // get question-level data from DB
  $res = $db->query("
    SELECT  v.badge, 
            CONCAT(
              CASE WHEN t.inQuestion = 1 THEN 'In-Question: ' ELSE 'Pre-Quiz: ' END,
              v.user, ', \"', v.title, '\"' 
            ) AS video,
            ROUND(t.viewSecs / 60, 0) AS viewMins
    FROM    videos AS v
            INNER JOIN
            (
              SELECT  i.video, 
                      CASE WHEN i.question IS NULL THEN 0 ELSE 1 END AS inQuestion,
                      SUM(i.elapsedSecs) AS viewSecs
              FROM    w_interactions AS i
                      INNER JOIN vw_participants AS p ON i.participant = p.participant
              WHERE   p.isValid = 1
                      AND track = 'youtube'
              GROUP   BY i.video, inQuestion
            ) AS t ON v.video = t.video
    ORDER   BY v.badge, t.inQuestion, t.viewSecs DESC");
  check_for_db_error($res, $db);
  $data = array();
  while($q = $res->fetch_assoc()) {
    $badge = $q['badge'];
    if(!isset($data[$badge]))
      $data[$badge] = array();
    $data[$badge][] = $q;
  }
  // free up resources
  if(isset($res) && is_object($res))
    $res->free();
  $db->close();


  foreach ($data as $badge => $badgeData) {
    echo "
      <h3>{$badge}</h3>
      <div id='c-b-p-$badge' class='chart'></div>";
  }

  echo "
      <script>
        google.charts.setOnLoadCallback(initPieChartData);
        function initPieChartData() {
          window.chartData = window.chartData || {};";
  foreach ($data as $badge => $badgeData) {
    echo "
          window.chartData['c-b-p-$badge'] = google.visualization.arrayToDataTable([
            [ 'Video', 'Time Watched (min.)' ]";
    foreach ($badgeData as $row) {
      echo ",
            [ '{$row['video']}', {$row['viewMins']} ]";
    }
    echo "
          ]);";
  }
//           window.chartData['$id'] = data;
//           var options = {
//             height: 500,
//             legend: {position: 'none'},
//             hAxis: { title: '$xLabel' },
//             vAxis: { title: '$yLabel' },
//             colors: [ '#3498db' ]
//           };
//           var chart = new google.visualization.ScatterChart(document.getElementById('$id'));
//           chart.draw(data, options);
//           window.charts = window.charts || {};
//           window.charts['$id'] = chart;
  echo "
        }
      </script>";


  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}