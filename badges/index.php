<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('Badge Quiz Data', true);
?>
      <h2>Summary Tables and Charts</h2>
      <ul>
        <li><a href="charts.php">View Question Charts</a></li>
        <li><a href="cad.php">Coronary Artery Disease Badge Quiz Summary</a></li>
        <li><a href="pi.php">Polygenic Inheritance Badge Quiz Summary</a></li>
        <li><a href="snp.php">Single Nucleotide Polymorphisms Badge Quiz Summary</a></li>
        <li><a href="videos.php">View Video Charts</a></li>
      </ul>
<?php
  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}