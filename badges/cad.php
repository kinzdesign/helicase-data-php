<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_quiz_data.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_print_quiz_table.php');

  $data = get_quiz_data('CAD');

  Layout::$EmitContainer = false;
  Layout::EmitTop('Coronary Artery Disease Tables', true);

  print_quiz_table($data, 'Coronary Artery Disease');

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}