<?php
  function get_participant_quiz_data($participant) {
    if(!is_numeric($participant))
      throw new Exception("Invalid participant ID", 1);
      
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
    $db = get_database();

    $output = array();

    // get participant data
    $res = $db->query("
      SELECT  p.participant, 
              s.cohort,
              s.score
      FROM    participants AS p
              LEFT JOIN w_participants AS s ON p.participant = s.participant
      WHERE   p.participant = $participant
    ");
    check_for_db_error($res, $db);
    if($q = $res->fetch_assoc())
      $output['participant'] = $q;
    else
      return false;

    // get all quizzes
    $output['quizzes'] = array();
    $res = $db->query("
      SELECT  q.quiz, q.title AS quizTitle,
              s.completed, s.multiScore, s.timeBonuses, s.totalScore, s.isScored
      FROM    q_participant_quiz AS qq
              INNER JOIN quizzes AS q ON q.quiz = qq.quiz
              LEFT  JOIN vw_quiz_score AS s ON qq.quiz = s.quiz AND qq.participant = s.participant
      WHERE   qq.participant = $participant
              AND q.quiz <> 'DEMO'
      ORDER   BY qq.q_id;
    ");
    while($q = $res->fetch_assoc()){
      $output['quizzes'][$q['quiz']] = $q;
      $output['quizzes'][$q['quiz']]['questions'] = array();
    }

    // get all questions
    $res = $db->query("
      SELECT  q.quiz, q.question, 
              COALESCE(q.multiIntroHtml, q.likertIntroHtml) AS questionText,
              r.answer, a.answerText AS answeredText, c.answerText AS correctText, r.likert, 
              COALESCE(r.score, 0) AS score,
              COALESCE(r.timeBonus, 0) AS timeBonus,
              TIMESTAMPDIFF(SECOND, r.sentMulti, r.submittedMulti) AS multiSecondsServer,
              TIMESTAMPDIFF(SECOND, r.displayedMulti, r.answeredMulti) AS multiSecondsClient,
              TIMESTAMPDIFF(SECOND, r.sentLikert, r.submittedLikert) AS likertSecondsServer,
              TIMESTAMPDIFF(SECOND, r.displayedLikert, r.answeredLikert) AS likertSecondsClient
      FROM    responses AS r
              INNER JOIN questions AS q ON r.question = q.question
              LEFT JOIN answers AS a ON r.answer = a.answer
              LEFT  JOIN answers AS c ON c.answer = (SELECT answer FROM answers WHERE question = r.question AND score > 0)
      WHERE   r.participant = $participant
              AND q.quiz <> 'DEMO'
      ORDER   BY responseId;
    ");
    while($q = $res->fetch_assoc()){
      $output['quizzes'][$q['quiz']]['questions'][] = $q;
    }

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }

?>