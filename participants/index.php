<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('Performance Data', true);
?>
      <h2>Summary Tables and Charts</h2>
      <ul>
        <li><a href="participants.php">View Participant List</a></li>
        <li><a href="regressions.php">View Performance Multivariate Regressions</a></li>
      </ul>

      <h2>Result t-Tests</h2>
      <div class='table-responsive'><table class="table table-striped">
        <thead>
          <tr>
            <td>
            </td>
            <th class="text-center">Score</th>
            <th class="text-center">Questions Correct</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>0: By Cohort</th>
            <td>
              <a href="t2-cohort-score.php">Two-Sample t-Test</a>
            </td>
            <td>
              <a href="t2-cohort-correct.php">Two-Sample t-Test</a>
            </td>
          </tr>
        </tbody>
      </table></div>

      <h2>Result Correlations</h2>
      <div class='table-responsive'><table class="table table-striped">
        <thead>
          <tr>
            <td>
            </td>
            <th class="text-center">Score</th>
            <th class="text-center">Questions Correct</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>By GLAC Score</th>
            <td>
              <a href="c-glac-score.php">Total Score by GLAC Score</a>
            </td>
            <td>
              <a href="c-glac-correct.php">Total Questions Correct by GLAC Questions Correct</a>
            </td>
          </tr>
          <tr>
            <th>By Focus Time</th>
            <td>
              <a href="c-focus-score.php">Score by Focus Time</a>
            </td>
            <td>
              <a href="c-focus-correct.php">Questions Correct by Focus Time</a>
            </td>
          </tr>
          <tr>
            <th>By Video Time</th>
            <td>
              <a href="c-video-score.php">Score by Video Time</a>
            </td>
            <td>
              <a href="c-video-correct.php">Questions Correct by Video Time</a>
            </td>
          </tr>
          <tr>
            <th>By Elapsed Time</th>
            <td>
              <a href="c-elapsed-score.php">Score by Elapsed Time</a>
            </td>
            <td>
              <a href="c-elapsed-correct.php">Questions Correct by Elapsed Time</a>
            </td>
          </tr>
          <tr>
            <th>By Courses Taken</th>
            <td>
              <a href="c-courses-score.php">Score by Courses Taken</a>
            </td>
            <td>
              <a href="c-courses-correct.php">Questions Correct by Courses Taken</a>
            </td>
          </tr>
        </tbody>
      </table></div>
<?php
  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}