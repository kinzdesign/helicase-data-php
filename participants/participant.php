<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  $id = Layout::GetQueryStringInt('id');
  // load participant data
  require_once($_SERVER['DOCUMENT_ROOT'].'/participants/_get_participant_data.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_list.php');
  $participant_data = get_participant_data($_GET['id'], isset($_GET['refresh']));
  if(!$participant_data) 
    Layout::RenderNotFound("The participant ID specified ({$id}) does not exist.");

  Layout::EmitTop("Participant {$id}");
  // output participant details
  echo " 
      <div class='row'>
        <div class='col-md-6'>
          <div class='table-responsive'><table class='table table-striped table-hover'>
            <tbody>
              <tr>
                <th>Experience</th>
                <td>{$participant_data['participant']['experience']}</td>
              </tr>
              <tr>
                <th>Pathway</th>
                <td>{$participant_data['participant']['pathway']}</td>
              </tr>
              <tr>
                <th>Vignette</th>
                <td>{$participant_data['participant']['vignette']}</td>
              </tr>
              <tr>
                <th>Score</th>
                <td>{$participant_data['participant']['score']}</td>
              </tr>
              <tr>
                <th>Elapsed Time</th>
                <td>{$participant_data['participant']['elapsedProse']}</td>
              </tr>
              <tr>
                <th>Focus Time</th>
                <td>{$participant_data['participant']['focusProse']}</td>
              </tr>
              <tr>
                <th>Video Time</th>
                <td>{$participant_data['participant']['videoProse']}</td>
              </tr>
              <tr>
                <th>Consented</th>
                <td>
                  {$participant_data['participant']['consented']}
                </td>
              </tr>
              <tr>
                <th>Finished</th>
                <td>{$participant_data['participant']['finishedStamp']}</td>
              </tr>
              <tr>
                <th>\$6 Base Paid At</th>
                <td>{$participant_data['participant']['basePaidAt']}</td>
              </tr>" .
              ($participant_data['participant']['bonusPaidAt'] ? "
              <tr>
                <th>\${$participant_data['participant']['bonusAmount']} Bonus Paid At</th>
                <td>{$participant_data['participant']['bonusPaidAt']}</td>
              </tr>" : '') . "
            </tbody>
          </table></div>
        </div>
        <div class='col-md-6'>
          <div class='table-responsive'><table class='table table-striped table-hover'>
            <tbody>
              <tr>
                <th>Age</th>
                <td>{$participant_data['participant']['age']}</td>
              </tr>
              <tr>
                <th>Education</th>
                <td>
                  {$participant_data['participant']['education']}";
  render_list($participant_data['participant']['courses']);
  echo " 
                </td>
              </tr>
              <tr>
                <th>Hispanic</th>
                <td>";
  render_list($participant_data['participant']['hispanic']);
  echo " 
                </td>
              </tr>
              <tr>
                <th>Ethnicity</th>
                <td>";
  render_list($participant_data['participant']['ethnicity']);
  echo " 
                </td>
              </tr>
              <tr>
                <th>Birth Assignment</th>
                <td>{$participant_data['participant']['sex']}</td>
              </tr>
              <tr>
                <th>Gender Identity</th>
                <td>";
  render_list($participant_data['participant']['genders']);
  echo "
                </td>
              </tr>
            </tbody>
          </table></div>
        </div>
      </div>";



  echo "
      <div class='panel panel-danger'>
        <div class='panel-heading'>
          <h3 class='panel-title'>
            Quizzes
          </h3>
        </div>
        <div class='panel-body'>
          <div class='row'>
            <div class='table-responsive'><table class='table table-striped table-hover text-center'>
              <thead>
                <tr>
                  <td>Quiz</td>
                  <td>Completed</td>
                  <td>Multi</td>
                  <td>Likert</td>
                  <td>Correct</td>
                  <td>Score</td>
                </tr>
              </thead>
              <tbody>";
  foreach($participant_data['quizzes'] as $quiz => $quiz_data) {
      echo "
                <tr>
                  <th>
                    <a href='/participants/quizzes.php?id={$_GET['id']}#{$quiz_data['quiz']}' target='_blank'>
                      {$quiz_data['quiz']}:
                      {$quiz_data['quizTitle']}
                    </a>
                  </th>
                  <td>";
      if($quiz_data['completed'] == 1)
        echo "<i class='fa fa-check' title='Quiz Completed'></i>";
      else
        echo "<i class='fa fa-times' title='Quiz Incomplete'></i>";
      echo "
                  </td>
                  <td>" . 
                    ($quiz_data['nMulti'] > 0 ? "{$quiz_data['cMulti']} / {$quiz_data['nMulti']}" : '&mdash;') . 
                  "</td>
                  <td>" . 
                    ($quiz_data['nLikert'] > 0 ? "{$quiz_data['cLikert']} / {$quiz_data['nLikert']}" : '&mdash;') . 
                  "</td>
                  <td>" . 
                    ($quiz_data['nMulti'] > 0 ? "{$quiz_data['nCorrect']} / {$quiz_data['nMulti']}" : '&mdash;') . 
                  "</td>
                  <td>";
      if(!$quiz_data['isScored'] || $quiz_data['isScored'] == 0)
        echo "[unscored]";
      elseif($quiz_data['timeBonuses'] > 0)
        echo "{$quiz_data['multiScore']} + {$quiz_data['timeBonuses']} = {$quiz_data['totalScore']}";
      else
        echo $quiz_data['totalScore'];
      echo "
                  </td>
                  <td>";
      echo "
                  </td>
                </tr>";
  }

  echo "
              </tbody>
            </table></div>
          </div>
        </div>
      </div>";

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}