<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  $id = Layout::GetQueryStringInt('id');
  // load participant data
  require_once($_SERVER['DOCUMENT_ROOT'].'/participants/_get_participant_quiz_data.php');
  $participant_data = get_participant_quiz_data($id);
  if(!$participant_data)
    Layout::RenderNotFound("The participant ID specified ({$id}) does not exist.");

  Layout::EmitTop("Quizzes for Participant {$id}");

  foreach($participant_data['quizzes'] as $quiz => $quiz_data) {
    // open panel and table
    echo "
      <div class='panel panel-danger'>
        <div class='panel-heading'>
          <h3 class='panel-title'>
            <a name='{$quiz_data['quiz']}'></a>
            {$quiz_data['quiz']}:
            {$quiz_data['quizTitle']}
          </h3>
        </div>
        <div class='panel-body'>
          <div class='row'>
            <div class='table-responsive'><table class='table table-striped table-hover'>
              <thead>
                <tr>
                  <td>Multi Answered</td>
                  <td>Multi Correct</td>
                  <td>Likert</td>
                  <td>Score</td>
                </tr>
              </thead>
              <tbody>";
    for($i = 0; $i < sizeof($quiz_data['questions']); $i++) {
      $question = $quiz_data['questions'][$i];
      echo "
                <tr>
                  <td colspan='4'>
                    <strong>";
      echo $i + 1;
      echo ": {$question['questionText']}</strong>
                  </td>
                </tr>
                <tr>
                  <td>
                    {$question['answeredText']}<br/>
                    Server: {$question['multiSecondsServer']} sec.<br/>
                    Client {$question['multiSecondsClient']} sec.
                  </td>
                  <td>{$question['correctText']}</td>
                  <td>
                    {$question['likert']}<br/>
                    Server: {$question['likertSecondsServer']} sec.<br/>
                    Client {$question['likertSecondsClient']} sec.
                  </td>
                  <td>
                    {$question['score']}";
      if($question['timeBonus'] > 0)
        echo " + {$question['timeBonus']}";
      echo "
                  </td>
                </tr>";
    }
    // close table and panel
    echo "
              </tbody>
            </table></div>
          </div>
        </div>
      </div>";
  }

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}