<?php
  function get_participant_data($participant, $refreshInteractions = false) {
    if(!is_numeric($participant))
      throw new Exception("Invalid participant ID", 1);
      
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_human_seconds.php');
    $db = get_database();

    $output = array();

    // get participant data
    $res = $db->query("
      SELECT  p.participant, pw.pathwayName AS pathway,
              ex.experienceName AS experience,
              p.cohort,
              p.vignette, p.score, 
              p.elapsedProse, p.focusProse, p.videoProse,
              p.consented, p.finishedStamp, p.basePaidAt, p.bonusPaidAt,
              p.age, p.education, p.courses, p.hispanic, 
              p.ethnicity, p.sex, p.genders, 
              IF(p.bonusAmount > 0, p.bonusAmount, '') AS bonusAmount
      FROM    vw_participants AS p
              LEFT JOIN pathways AS pw ON p.pathway = pw.pathway
              LEFT JOIN experiences AS ex ON p.experience = ex.experience
      WHERE   p.participant = $participant
    ");
    check_for_db_error($res, $db);
    if($q = $res->fetch_assoc()) {
      $output['participant'] = $q;
    } else {
      return false;
    }

    // get all quizzes
    $output['quizzes'] = array();
    $res = $db->query("
      SELECT  q.quiz, q.title AS quizTitle,
              s.cMulti, s.nMulti, s.cLikert, s.nLikert, s.nCorrect,
              s.completed, s.multiScore, s.timeBonuses, s.totalScore, s.isScored
      FROM    q_participant_quiz AS qq
              INNER JOIN quizzes AS q ON q.quiz = qq.quiz
              LEFT  JOIN vw_quiz_score AS s ON qq.quiz = s.quiz AND qq.participant = s.participant
      WHERE   qq.participant = $participant
              AND q.quiz <> 'DEMO'
      ORDER   BY qq.q_id;
    ");
    while($q = $res->fetch_assoc()){
      $output['quizzes'][$q['quiz']] = $q;
      $output['quizzes'][$q['quiz']]['questions'] = array();
    }


    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }

?>