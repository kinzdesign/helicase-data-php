<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_bubble_plot.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_correlation.php');

  $db = get_database();
  
  $labelCol = 'FcgtCorrect';
  $valueCol = 'TotalCorrect';
  $suffix   = false;

  // get data
  $res = $db->query("
    SELECT  participant AS Participant, 
            cohort      AS Cohort, 
            fcgtCorrect AS $labelCol, 
            questionsRight AS $valueCol
    FROM    vw_participants
    WHERE   isValid = 1 
    ORDER   BY participant");
  $data = [ 'rows' => get_rows_as_array($res, $db) ];


  $labelTitle = ucwords($labelCol);
  $valueTitle = ucwords($valueCol);
  $title = "$valueTitle by $labelTitle";
  $results = correlation_from_data($data, $labelCol, $valueCol, $suffix);
  Layout::EmitTop($title, true);
  echo "
    <div class='row'>
      <div class='col-sm-4'>";
  correlation_tables($results['values'], $labelCol, $valueCol, $suffix);
  echo "
      </div>
      <div class='col-sm-8'>";

  $counts = array();
  foreach ($data['rows'] as $row) {
    $x = $row[$labelCol];
    $y = $row[$valueCol];
    if(!isset($counts[$x]))
      $counts[$x] = array();
    if(!isset($counts[$x][$y]))
      $counts[$x][$y] = 1;
    else
      $counts[$x][$y]++;
  }
  $id = 'c-g-c';
  echo "
      <div id='$id' class='chart'></div>
      <script>
        google.charts.setOnLoadCallback(drawBubblePlot);
        function drawBubblePlot() {
          var data = google.visualization.arrayToDataTable([
            [ 'ID', '$labelCol', '$valueCol', '', 'N' ]";
  foreach ($counts as $x => $ys) {
    foreach ($ys as $y => $n) {
      echo ",
            [ '$x, $y = $n', $x, $y, '', $n ]";
    }
  }
  echo "
          ]);
          window.chartData = window.chartData || {};
          window.chartData['$id'] = data;
          showChart(\$('#$id'));
        }
      </script>";


  echo "
      </div>
    </div>";
  data_table($data);

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}