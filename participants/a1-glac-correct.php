<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_anova_one_way.php');

  $db = get_database();
  
  $labelCol     = 'GLAC';
  $identCol     = 'Participant';
  $treatCol     = 'Cohort';
  $valueCol     = 'NumCorrect';

  // get data
  $res = $db->query("
    SELECT  participant AS $identCol,
            cohort AS $treatCol,
            fcgtCorrect AS $labelCol, 
            questionsRight AS $valueCol
    FROM    vw_participants
    WHERE   isValid = 1
    ORDER   BY fcgtCorrect, participant");
  $data = array('rows' => get_rows_as_array($res, $db));

  render_anova_one_way($data, false, $identCol, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}