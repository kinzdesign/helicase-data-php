<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_t_test_two_sample.php');

  $db = get_database();
  
  $labelCol = 'Cohort';
  $valueCol = 'Score';

  // get data
  $res = $db->query("
    SELECT  participant AS Participant, 
            cohort AS $labelCol,
            score AS $valueCol
    FROM    vw_participants
    WHERE   isValid = 1
    ORDER   BY cohort DESC, score DESC");
  check_for_db_error($res, $db);

  render_t_test_two_sample($res, false, $labelCol, $valueCol);
} catch (Exception $e) {
  Layout::RenderException($e);
}