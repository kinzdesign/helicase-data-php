<?php
function print_participant_table($participant_data) {
  echo "
                  <div class='table-responsive'><table class='table table-striped table-hover text-center'>
                    <thead>
                      <tr>
                        <td>ID</td>
                        <td>Score</td>
                        <td>Elapsed</td>
                        <td>Focus</td>
                        <td>Video</td>
                      </tr>
                    </thead>
                    <tbody>";
  foreach($participant_data as $id => $participant) {
    $begin_link = "<a href='/participants/participant.php?id={$participant['participant']}'' target='_blank'>";
    $incomplete_cell = "
                        <i class='fa fa-times'></i>";
    echo "
                      <tr>
                        <th>$begin_link{$participant['participant']}</a></th>
                        <td>$begin_link{$participant['score']}</a></td>
                        <td>$begin_link{$participant['elapsedSortable']}</a></td>
                        <td>$begin_link{$participant['focusSortable']}</a></td>
                        <td>$begin_link{$participant['videoSortable']}</a></td>
                      </tr>";
  }
  echo "
                    </tbody>
                  </table></div>";
  }
?>