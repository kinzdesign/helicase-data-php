<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/participants/_get_participants_list.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/participants/_print_participant_table.php');

  // get data
  $participants_list = get_participants_list();
  
  Layout::EmitTop('Participants');
  // output header and progress bar
  $totalParticipants = $participants_list['counts']['total'];
  $finished = $participants_list['counts']['finished'];
  $incomplete = $totalParticipants - $finished;

  // handle pathways
  foreach($participants_list['data'] as $pathway => $pathway_data) {
    // open outer panel
    echo "
      <div class='panel panel-danger'>
        <div class='panel-heading'>
          <h3 class='panel-title'>{$pathway} (<em>n</em> = ";
    echo $participants_list['counts'][$pathway];
    echo ")</h3>
        </div>
        <div class='panel-body'>
          <div class='row'>";

    // handle experiences
    foreach($pathway_data as $experience => $experience_data) {
      // open inner panel
      echo "
            <div class='col-md-6'>
              <div class='panel panel-info'>
                <div class='panel-heading'>
                  <h3 class='panel-title'>{$pathway}-{$experience} (<em>n</em> = ";
      echo $participants_list['counts']["$pathway-{$experience}"];
      echo ")</h3>
                </div>
                <div class='panel-body'>";
      print_participant_table($experience_data);
      // close inner panel
      echo "
                </div>
              </div>
            </div>";
    }

    // close outer panel
    echo "
          </div>
        </div>
      </div>";
  }

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}