<?php
  function get_participants_list() {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_human_seconds.php');
    $db = get_database();

    $output = array();
    $output['data'] = array();
    $output['counts'] = array();

    // get all participants
    $res = $db->query("
      SELECT  p.participant, p.pathway, p.experience, p.cohort, 
              p.elapsedSortable, p.focusSortable, p.videoSortable,
              p.vignette, p.score,
              p.consented, p.finishedStamp, p.basePaidAt, p.bonusPaidAt
      FROM    vw_participants AS p
      WHERE   p.isValid = 1
      ORDER   BY p.cohort DESC, p.score DESC
    ");
    check_for_db_error($res, $db);
    // copy participants to output, binned by pathway and experience
    while($q = $res->fetch_assoc()) {
      // ensure buckets exist
      if(!array_key_exists($q['pathway'], $output['data']))
        $output['data'][$q['pathway']] = array();
      if(!array_key_exists($q['experience'], $output['data'][$q['pathway']]))
        $output['data'][$q['pathway']][$q['experience']] = array();
      if(!array_key_exists('total', $output['counts']))
        $output['counts']['total'] = 0;
      if(!array_key_exists('finished', $output['counts']))
        $output['counts']['finished'] = 0;
      if(!array_key_exists($q['pathway'], $output['counts']))
        $output['counts'][$q['pathway']] = 0;
      if(!array_key_exists($q['cohort'], $output['counts']))
        $output['counts'][$q['cohort']] = 0;

      // add data to output
      $output['data'][$q['pathway']][$q['experience']][] = $q;

      // increment counters
      $output['counts']['total']++;
      if($q['finishedStamp'])
        $output['counts']['finished']++;
      $output['counts'][$q['pathway']]++;
      $output['counts'][$q['cohort']]++;
    }


    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }

?>