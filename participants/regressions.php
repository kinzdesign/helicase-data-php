<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('Score Regression', true);
  $script = basename(__FILE__, '.php');

  // get data
  require_once($_SERVER["DOCUMENT_ROOT"] . '/include/_get_database.php');
  $db = get_database();
  $res = $db->query("
        SELECT  participant, cohort, vignette, score, 
                elapsedSecs, focusSecs, videoSecs,
                age, sex, education, courseUnits, questionsRight, fcgtCorrect, 
                pgenRace AS race, CASE WHEN pgenHispanic = 1 THEN 'Y' ELSE 'N' END AS hispanic, 
                pgen1SelfEfficacy, pgen1Knowledge
        FROM    vw_participants 
        WHERE   isValid = 1");
  check_for_db_error($res, $db);

  // write CSV
  $csv = fopen("$script.r.csv", "w");
  // write headers
  fwrite($csv, "participant,cohort,vignette,score,elapsedSecs,focusSecs,videoSecs,age,sex,education,courseUnits,questionsRight,fcgtCorrect,race,hispanic,pgen1SelfEfficacy,pgen1Knowledge\n");
  // iterate rows, write data to file
  while($q = $res->fetch_assoc()) {
    fwrite($csv, 
          $q['participant'] . ',' .
          $q['cohort'] . ',' .
          $q['vignette'] . ',' .
          $q['score'] . ',' .
          $q['elapsedSecs'] . ',' .
          $q['focusSecs'] . ',' .
          $q['videoSecs'] . ',' .
          $q['age'] . ',' .
          $q['sex'] . ',"' .
          $q['education'] . '",' .
          $q['courseUnits'] . ',' .
          $q['questionsRight'] . ',' .
          $q['fcgtCorrect'] . ',' .
          $q['race'] . ',' .
          $q['hispanic'] . ',' .
          $q['pgen1SelfEfficacy'] . ',' .
          $q['pgen1Knowledge'] . "\n");
  }
  // close file
  fclose($csv);

  // free up database
  if(isset($res) && is_object($res))
    $res->free();
  $db->close();

  // write R script
  file_put_contents($script . '.r', 
"### Computes Multivariate linear regression for score data

# load CSV data
scores <- read.table(\"$script.r.csv\", header=T, sep=\",\")

# questions right regression
scores.correctlm <- lm(questionsRight~cohort + vignette + age + sex + education + race + hispanic + focusSecs + videoSecs + courseUnits + fcgtCorrect + pgen1SelfEfficacy + pgen1Knowledge, data= scores)
summary(scores.correctlm)

# score regression
scores.scorelm <- lm(score~cohort + vignette + age + sex + education + race + hispanic + focusSecs + videoSecs + courseUnits + fcgtCorrect + pgen1SelfEfficacy + pgen1Knowledge, data= scores)
summary(scores.scorelm)");

  // run script
  exec("Rscript $script.r > $script.r.txt");

  echo "
    <ul>
      <li><a href='$script.r'>Download R script</a></li>
      <li><a href='$script.r.txt'>Download R output</a></li>
      <li><a href='$script.r.csv'>Download CSV data</a></li>
    </ul>";

  // emit data
  echo '<pre>';
  readfile("$script.r.txt");
  echo '</pre>';

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}