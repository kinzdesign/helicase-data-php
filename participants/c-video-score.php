<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/include/_render_correlation.php');

  $db = get_database();
  
  $labelAlias = 'Video';
  $valueAlias = 'Score';
  $suffix     = 'Rank';

  // get data
  $res = $db->query("
    SELECT  participant AS Participant, 
            cohort      AS Cohort, 
            videoSecs   AS $labelAlias, 
            score       AS $valueAlias,
            videoRank   AS $labelAlias$suffix, 
            rankOverall AS $valueAlias$suffix
    FROM    vw_participants 
    WHERE   isValid = 1 
    ORDER   BY participant");
  $data = [ 'rows' => get_rows_as_array($res, $db) ];

  render_correlation($data, $labelAlias, $valueAlias, $suffix);
} catch (Exception $e) {
  Layout::RenderException($e);
}