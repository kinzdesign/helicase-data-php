<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  Layout::EmitTop('Research Data', true);
?>
      <p>Please use the navigation bar at the top of the page to navigate the statistics.</p>
<?php
  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}