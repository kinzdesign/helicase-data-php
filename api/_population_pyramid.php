<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');

function population_pyramid() {
  $db = get_database();
  $res = $db->query("
    SELECT  age, sex, COUNT(*) AS n
    FROM    vw_participants
    WHERE   isValid = 1
    GROUP   BY age, sex");
  check_for_db_error($res, $db);
  $data = array();
  while($q = $res->fetch_assoc()) {
    if(!array_key_exists($q['age'], $data))
      $data[$q['age']] = array();
    $data[$q['age']][$q['sex']] = $q['n'];
  }
  echo "
    [ \"Age\", \"Male\", \"Female\" ]";
  foreach ($data as $age => $values) {
    $male = $values['Male'] ?? 0;
    $female = -1 * ($values['Female'] ?? 0);
    echo ",
    [ \"$age\", $male, $female ]";
  }

}
