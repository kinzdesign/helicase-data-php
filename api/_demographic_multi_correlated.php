<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');

function demographic_multi_correlated($id) {
  $chunks   = explode('-', $id);
  $format   = $chunks[2];
  $question = $chunks[3];
  if(!is_numeric($question))
    throw new Exception("Invalid question number, must be numeric: '$question'");
  $db       = get_database();
  $res      = $db->query("
    SELECT  answers, COUNT(participant) AS n
    FROM    (        
              SELECT  p.participant, 
                      q.displayOrder AS question,
                      fn_get_answers_for_question(p.participant, q.question) AS answers
              FROM    vw_participants AS p
                      INNER JOIN questions AS q
              WHERE   p.isValid = 1 AND
                      q.displayOrder = $question
            ) AS a
    WHERE   answers IS NOT NULL
    GROUP   BY answers");
  check_for_db_error($res, $db);

  // output header row
  echo "
    [ \"Answer\", \"Count\" ]";

  // iterate rows to output data
  while($q = $res->fetch_assoc()) {
    echo ",
    [ \"{$q['answers']}\", {$q['n']} ]";
  }
}
