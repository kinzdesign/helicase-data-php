<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');

function demographic_multi($id) {
  $chunks   = explode('-', $id);
  $format   = $chunks[2];
  $question = $chunks[3];
  if(!is_numeric($question))
    throw new Exception("Invalid question number, must be numeric: '$question'");
  $db       = get_database();
  $res      = $db->query("
    SELECT  a.answerText AS answer,
            COUNT(p.participant) AS n
    FROM    responses_multi AS r
            INNER JOIN questions AS q ON r.question = q.question
            INNER JOIN answers AS a ON r.answer = a.answer
            INNER JOIN vw_participants AS p ON r.participant = p.participant
    WHERE   p.isValid = 1
            AND q.quiz = 'DEMO'
            AND q.displayOrder = $question
    GROUP   BY a.answerText
    ORDER   BY a.displayOrder");
  check_for_db_error($res, $db);

  // output header row
  echo "
    [ \"Answer\", \"Count\" ]";

  // iterate rows to output data
  while($q = $res->fetch_assoc()) {
    echo ",
    [ \"{$q['answer']}\", {$q['n']} ]";
  }
}
