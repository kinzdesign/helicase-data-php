<?php
// set MIME-type
header('Content-Type: text/json');
// open JSON object
echo '{';
$isFirst = true;
// iterate over charts requested
foreach (explode(',', $_GET['ids']) as $id) {
  // emit comma if needed and open array
  if(!$isFirst) echo ",";
  $isFirst = false;
  echo "\n  \"$id\": [ ";

  // get and output data based on chart type prefix
  switch (substr($id, 0, 5)) {
    case 'c-d-b': 
      require_once($_SERVER['DOCUMENT_ROOT'].'/api/_demographic_single.php');
      demographic_single($id);
      break;
    case 'c-d-c': 
      require_once($_SERVER['DOCUMENT_ROOT'].'/api/_demographic_multi_correlated.php');
      demographic_multi_correlated($id);
      break;
    case 'c-d-p': 
      require_once($_SERVER['DOCUMENT_ROOT'].'/api/_demographic_multi.php');
      demographic_multi($id);
      break;
    case 'c-d-w': 
      require_once($_SERVER['DOCUMENT_ROOT'].'/api/_demographic_single.php');
      demographic_single($id);
      break;
    case 'c-p-y': 
      require_once($_SERVER['DOCUMENT_ROOT'].'/api/_population_pyramid.php');
      population_pyramid();
      break;
    default:
      break;
  }

  // close array
  echo "\n  ]";
}

// close JSON object
echo "\n}";