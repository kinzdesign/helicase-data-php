<?php
  function get_quiz_data() {
    require_once($_SERVER['DOCUMENT_ROOT'].'/include/_get_database.php');
    $db = get_database();

    $output = array();

    // get all quizzes
    $res = $db->query('
      SELECT *
      FROM quizzes
      ORDER BY quiz
    ');
    // copy quizzes to output
    while($q = $res->fetch_assoc()) {
      $q['questions'] = array();
      $output[$q['quiz']] = $q;
    }

    // get all questions
    $res = $db->query('
      SELECT  *
      FROM    questions
      ORDER   BY quiz, displayOrder
    ');
    // copy questions to output
    while($q = $res->fetch_assoc()) {
      $q['answers'] = array();
      $output[$q['quiz']]['questions'][$q['question']] = $q;
    }

    // get all answers
    $res = $db->query('
      SELECT  *
      FROM    answers AS a
              INNER JOIN questions AS q ON a.question = q.question
      ORDER   BY q.quiz, q.displayOrder, a.displayOrder
    ');
    // copy answers to output
    while($a = $res->fetch_assoc()) {
      $output[$a['quiz']]['questions'][$a['question']]['answers'][] = $a;
    }

    // free up resources
    if(isset($res) && is_object($res))
      $res->free();
    $db->close();

    return $output;
  }

?>