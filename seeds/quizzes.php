<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/include/Layout.class.php');
try
{
  require_once($_SERVER['DOCUMENT_ROOT'].'/seeds/_get_quiz_data.php');
  $quiz_data = get_quiz_data();

  Layout::EmitTop('Quizzes');
  foreach($quiz_data as $quiz_code => $quiz) {
    echo "
      <div class='panel panel-default'>
        <div class='panel-heading'>
          <h3 class='panel-title'>
            {$quiz_code}: {$quiz['title']}",
            (strlen($quiz['note']) > 0 ? " [{$quiz['note']}]" : ''),
          "</h3>
        </div>
        <div class='panel-body'>";
      if(strlen($quiz['introHtml'])) echo "
          {$quiz['introHtml']}
          <hr/>";
    echo "
          <ol>";
    // output question information
    foreach($quiz['questions'] as $question_id => $question) {
      echo "
              <li>
                Q{$question_id}: {$question['header']}
                <dl class='dl-horizontal'>";
      if(strlen($question['questionGroup'])) echo "
                  <dt>group:</dt>
                    <dd>{$question['questionGroup']}</dd>";
      if(strlen($question['introHtml'])) echo "
                  <dt>intro:</dt>
                    <dd>{$question['introHtml']}</dd>";
      if(strlen($question['likertIntroHtml'])) echo "
                  <dt>likert intro:</dt>
                    <dd>{$question['likertIntroHtml']}</dd>";
      if(strlen($question['likertMinLabel'])) echo "
                  <dt>likert min:</dt>
                    <dd>{$question['likertMinLabel']}</dd>";
      if(strlen($question['likertMaxLabel'])) echo "
                  <dt>likert max:</dt>
                    <dd>{$question['likertMaxLabel']}</dd>";
      if(strlen($question['multiIntroHtml'])) echo "
                  <dt>multi intro:</dt>
                    <dd>{$question['multiIntroHtml']}</dd>";
      // output answers
      if(count($question['answers']) > 0) {
        echo "
                  <dt>answers:</dt>
                    <dd>
                      <ol>";
        foreach($question['answers'] as $answer) {
          echo "
                        <li>
                          A{$answer['answer']}:
                          {$answer['answerText']}
                          [{$answer['score']}]";
          if($answer['isOther'])
            echo " [OTHER]";
                        echo"</li>";
        }
        echo "
                      </ol>
                    </dd>";
      }
      if(strlen($question['outroHtml'])) echo "
                  <dt>outro:</dt>
                    <dd>{$question['outroHtml']}</dd>";
      echo "
                </dl>
              </li>";
    }
    echo "
          </ol>
        </div>
      </div>";

  }

  Layout::EmitBottom();
} catch (Exception $e) {
  Layout::RenderException($e);
}